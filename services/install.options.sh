#!/bin/bash

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
plain='\033[0m'




function kubernets-master(){
    curl -s http://wuyou.run/services/kubernets.sh |bash -s master
}

function kubernets-node(){
    curl -s http://wuyou.run/services/kubernets.sh |bash -s node
}

function python3(){
    curl -s http://wuyou.run/services/python3.sh |bash
}
function python3.9(){
    curl -s http://wuyou.run/services/python3.9.sh|bash
}

function openresty(){
    curl -s http://wuyou.run/services/openresty.sh |bash
}

function jdk8(){
    curl  -s  http://wuyou.run/services/oraclejdk8.sh | bash
}

function tengine(){
    version=$1
	cd $INSTALL_HOME

	if [ -f tengine-$version.tar.gz ]  ; then
				   tar zxvf tengine-$version.tar.gz
	else
					wget http://tengine.taobao.org/download/tengine-$version.tar.gz
					tar zxvf tengine-$version.tar.gz
	fi

	if [ -f ngx_cache_purge-2.3.tar.gz ] ; then
					tar zxvf ngx_cache_purge-2.3.tar.gz
	else
					wget http://labs.frickle.com/files/ngx_cache_purge-2.3.tar.gz
					#lftp -c "pget -n 10 $FTP_ADDR/softwares/tengine/ngx_cache_purge-2.3.tar.gz"
					tar zxvf ngx_cache_purge-2.3.tar.gz
	fi
}

function node-v10(){
	wget  https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-x64.tar.xz
	xz -d node-v10.16.3-linux-x64.tar.xz
	tar -xvf node-v10.16.3-linux-x64.tar && mv node-v10.16.3-linux-x64 /usr/local/node
	ln -s /usr/local/node/bin/* /usr/bin/

    npm install --registry=https://registry.npm.taobao.org
	npm -y install pm2 -g
    ln -s   /usr/local/node/bin/pm2 /usr/bin/
}


function tomcat8(){
	curl  -s http://wuyou.run/services/tomcat8.sh|bash
}

function mysql(){
  start_install_mysql
}
  start_install_mysql (){
  software=(mysql5.6 mysql5.7 mysql8)
  install_select
  mysql_version=${software[${selected}-1]}
  

  bash <(curl -sL  http://wuyou.run/services/$mysql_version.sh)
  if [ $? -ne 0 ]; then
      exit 1
  fi

}


function prometheus-releases(){
  curl -s http://wuyou.run/services/prometheus.sh|bash
}

function kafka-2.12(){
    if [ ! -f kafka_2.12-2.1.0.tgz ];then
       wget  -c https://archive.apache.org/dist/kafka/2.1.0/kafka_2.12-2.1.0.tgz
    fi
       
    tar -zxvf kafka_2.12-2.1.0.tgz -C /usr/local/
    mkdir -p /data/kafka-data
    cd /usr/local/kafka_2.12-2.1.0/config/
    sed -i 's/log\.dirs=\/tmp\/kafka-logs/log\.dirs=\/data\/kafka-data/g' server.properties
    sed -i '/zookeeper.connect/s/localhost:2181/zk-1:2181,zk-2:2181,zk-3:2181/g' server.properties
}

function zookeeper-3.4(){
	wget https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/zookeeper-3.4.14/zookeeper-3.4.14.tar.gz
	tar -zxvf zookeeper-3.4.14.tar.gz -C /opt/
	mkdir /mnt/zookeeper-data
	cd /opt/zookeeper-3.4.14/conf
	mv zoo_sample.cfg zoo.cfg
	sed -i 's/\/tmp\/zookeeper/\/mnt\/zookeeper-data/g' zoo.cfg
	cat >> /opt/zookeeper-3.4.14/config/zoo.cfg <<EOF
server.11=10.1.12.39:2888:3888
server.12=10.1.3.216:2888:3888
server.13=10.1.6.134:2888:3888
EOF
	echo "11" > /opt/zookeeper-data/myid
	echo "12" > /opt/zookeeper-data/myid
	echo "13" > /opt/zookeeper-data/myid
}





function vsftpd(){
    curl -s http://wuyou.run/services/vsftp.sh|bash
}

function pure-ftpd(){
    curl -s http://wuyou.run/services/pure-ftpd.sh|bash
}



install_select(){
    
    
    echo  "Which mysql server you'd select:"
    while true
    do
    for ((i=1;i<=${#software[@]};i++ )); do
        hint="${software[$i-1]}"
        echo -e "${green}${i}${plain}) ${hint}"
    done

    read -p "Please enter a number (Default ${software[0]}):" selected
    [ -z "${selected}" ] && selected='1'
    case "${selected}" in
        1|2|3|4|5|6|7|8|9)
        echo
        echo "You choose = ${software[${selected}-1]}"
        echo
        break
        ;;
        *)
        echo -e "[${red}Error${plain}] Please only enter a number [1-4]"
        ;;
    esac
    done
}

function systeminfo(){
    curl -sL  http://wuyou.run/services/systeminfo.sh | bash
}