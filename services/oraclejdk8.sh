#!/bin/bash
###
# @Author: admin@attacker.club
# @Date: 2022-07-30 18:03:20
# @LastEditTime: 2023-11-02 16:45:34
# @Description:
###
# curl  -s  http://wuyou.run/services/oraclejdk8.sh| bash

function oraclejdk8() {

    if [ ! -f jdk-*.tar.gz ]; then

        wget -c https://repo.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz

        if [ $? != 0 ]; then
            echo "下载失败 !!!"
            exit 1
        fi
    fi
    tar -zxvf jdk-*.tar.gz -C /usr/local/
    rm -f /usr/bin/java
    ln -s /usr/local/jdk1.8*/bin/java /usr/bin/java

    cat >>/etc/profile <<EOF
export JAVA_HOME=/usr/local/jdk1.8.0_202/
export CLASSPATH=\$JAVA_HOME/lib:\$CLASSPATH
export JRE_HOME=\$JAVA_HOME/jre 
export PATH=\$JAVA_HOME/bin:\$PATH
EOF
    . /etc/profile && java -version
}

oraclejdk8
