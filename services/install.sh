#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

########################################################
# author: admin@attacker.club
# website: http://opsbase.cn
# description: Service installation script
#  bash <(curl -sL http://wuyou.run/go/install.sh) 
########################################################



function color_msg() {
  case "$1" in
  "error"|"red")
    echo -e "\e[1;31m$2\e[0m"
    ;;
  "warn"|"yellow")
    echo -e "\e[1;33m$2\e[0m"
    ;;
  "success"|"green")
    echo -e "\e[1;32m$2\e[0m"
    ;;
  "info"|"blue")
    echo -e "\e[1;34m$2\e[0m"
    ;;
  esac
}


# 判断是否root用户
if [ $(id -u) -ne 0 ]; then 
  color_msg error "### This script must be run as root !!!"
  exit 1
fi

if [ $# -lt 1 ];then



  color_msg green  "\n\tUsage:  bash <(curl -sL http://wuyou.run/go/install.sh) openresty \n"

  color_msg error "List of Support Services:"

curl -sL http://wuyou.run/services/install.options.sh |grep function|awk '{print $2}'|cut -d   '(' -f 1

exit 1

fi


# # 打印系统信息
# curl -sL http://wuyou.run/go/systeminfo.sh | bash




# 获取当前目录
Current_DIR=$(dirname $0)
#echo "当前位置:$Current_DIR"
Bin_DIR=$(cd $Current_DIR && pwd)
#echo "当前路径:$Bin_DIR"
# 获取当前时间
now=$(date +%Y%m%d%H%M%S)


rm -f /tmp/env.conf /tmp/install.sh.base /tmp/install.options.sh >/dev/null 2>&1 &
# 清理历史文件


if [ -f env.conf ];then

   ENV_FILE"$Current_DIR/env.conf"
   BASE_FILE"$Current_DIR/install.sh.base"
   SERVICES_FILE="$Current_DIR/do_services.sh"
else 
   wget -cqP  /tmp/ http://wuyou.run/services/env.conf  
   wget -cqP  /tmp/ http://wuyou.run/services/install.sh.base 
   wget -cqP  /tmp/ http://wuyou.run/services/install.options.sh 


   ENV_FILE="/tmp/env.conf"
   BASE_FILE="/tmp/install.sh.base"
   SERVICES_FILE="/tmp/install.options.sh"
fi





# 判断是否存在配置文件
if [ -f $BASE_FILE ]; then
  . $ENV_FILE
  . $BASE_FILE
else
  
  color_msg error "### The file is missing !!!"
  exit 1
fi





# 删除历史日志
rm install.*.log -f >/dev/null 2>&1 &

main $1 | tee  $1.install.`date +%Y%m%d%H%M%S`.log
echo ">> Log: $1.install.`date +%Y%m%d%H%M%S`.log"