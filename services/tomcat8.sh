#!/bin/bash
# curl  -s http://wuyou.run/services/install-tomcat8.sh|bash


function tomcat8(){

	tomcat_version=$(curl -sL https://mirrors.cnnic.cn/apache/tomcat/tomcat-8|grep v8 |grep -oP  "\d+\.\d+.\d+"|head -1)
    if [ ! -f apache-tomcat-$tomcat_version.tar.gz ]; then
        wget -c https://mirrors.cnnic.cn/apache/tomcat/tomcat-8/v$tomcat_version/bin/apache-tomcat-$tomcat_version.tar.gz
        if [ $? != 0 ]; then
            echo "下载失败 !!!"
            exit 1
        fi	
    fi
    
	mkdir -p /opt/tomcat
	tar -zxvf apache-tomcat-${tomcat_version}.tar.gz -C /opt/tomcat --strip-components=1

	# wget http://build.up-gram.com/soft/tomcat/tomcat -O /etc/init.d/tomcat
	# chmod +x /etc/init.d/tomcat
	# chkconfig --add tomcat
	# chkconfig tomcat on
}

tomcat8