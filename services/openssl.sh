#!/bin/bash

# 检查是否为 root 用户
if [ "$EUID" -ne 0 ]; then
    echo "请以 root 用户身份运行此脚本"
    exit 1
fi

# 安装必要的工具
yum install -y wget gcc make perl

# 备份原始的 OpenSSL 配置文件
cp /etc/ssl/openssl.cnf /etc/ssl/openssl.cnf.backup

# 下载 OpenSSL 源码包
wget --no-check-certificate https://www.openssl.org/source/openssl-1.1.1l.tar.gz

# 解压源码包
tar -zxvf openssl-1.1.1l.tar.gz

# 进入源码目录
cd openssl-1.1.1l

# 配置 OpenSSL
./config

# 编译和安装 OpenSSL
make
make install

# 更新 OpenSSL 库链接
ldconfig

# 恢复原始的 OpenSSL 配置文件
mv /etc/ssl/openssl.cnf.backup /etc/ssl/openssl.cnf

# 清理工作目录
cd ..
rm -rf openssl-1.1.1l
rm openssl-1.1.1l.tar.gz

echo "OpenSSL 更新完成"