yum -y install pam pam-devel openldap-devel openssl-devel
###
 # @Author: admin@attacker.club
 # @Date: 2022-07-30 18:03:20
 # @LastEditTime: 2022-12-01 01:20:51
 # @Description: 
### 


if [ ! -f ss5*.gz ]; then
   # wget http://d.opsbase.cn/ss5-3.8.9-8.tar.gz
   wget  "--no-check-certificate"  https://udomain.dl.sourceforge.net/project/ss5/ss5/3.8.9-8/ss5-3.8.9-8.tar.gz
fi

tar zxf ss5-*.gz
cd ss5-*
./configure
make &&make install
chmod +x /etc/init.d/ss5


cat >/etc/opt/ss5/ss5.conf <<EOF
# auth 0.0.0.0/0 - -
# permit - 0.0.0.0/0 - 0.0.0.0/0 - - - - -

# 密码认证
auth 0.0.0.0/0 - u
permit u 0.0.0.0/0 - 0.0.0.0/0 - - - - -
EOF

cat >/etc/opt/ss5/ss5.passwd <<EOF
admin vpnpass
EOF

grep 10800  /etc/sysconfig/ss5 ||cat > /etc/sysconfig/ss5<<EOF
SS5_OPTS=" -u root -b 0.0.0.0:10800"
#修改默认端口
EOF

# 开机自启动(3.8.9-8的一个bug，重启会删掉/var/run/ss5/,导致开机自启动时无法创建pid文件)
mkdir /var/run/ss5/
echo mkdir  /var/run/ss5/ >> /etc/rc.d/rc.local
chmod +x /etc/rc.d/rc.local
/etc/init.d/ss5  restart
chkconfig --add ss5
chkconfig --level 345 ss5 on

curl -U admin:vpnpass --socks5 127.0.0.1:10800 https://ipinfo.io