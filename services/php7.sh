#!/bin/bash
# --------------------------------------------------
#Author:  LJ
#Email:   admin@attacker.club

#Last Modified: 2019-08-23 18:06:54
#Description: curl -s http://wuyou.run/services/php7.sh|bash
# --------------------------------------------------



CPU_NUM=$(cat /proc/cpuinfo | grep processor | wc -l)


cd /usr/local/src/

yum install cmake -y
cmake_version=`rpm -q cmake|grep  -Po  "\d"|head -1`
if [ cmake_version -lt 3 ];then
    yum remove cmake -y
  
  if [ ! -f cmake-3.15.2.tar.gz ];then
     wget -c https://github.com/Kitware/CMake/releases/download/v3.15.2/cmake-3.15.2.tar.gz
  fi

tar zxvf cmake-3.15.2.tar.gz && cd cmake-3.15.2
./bootstrap
  if [ $CPU_NUM -gt 1 ];then
    make -j$CPU_NUM
  else
    make
  fi
  
  make install && cd ..
  ln -s /usr/local/bin/cmake /usr/bin/
fi



yum remove libzip -y
if [ ! -f libzip-1.5.2.tar.gz ];then
   wget -c https://libzip.org/download/libzip-1.5.2.tar.gz
fi

tar zxvf libzip-1.5.2.tar.gz && cd libzip-1.5.2
mkdir build
cd build
cmake ..
if [ $CPU_NUM -gt 1 ];then
    make -j$CPU_NUM
else
    make
fi
make install && cd ..

PATH=$PATH:/usr/local/php/bin
export PATH

yum install -y libxml2 libxml2-devel openssl openssl-devel bzip2 bzip2-devel libcurl libcurl-devel libjpeg libjpeg-devel libpng libpng-devel freetype freetype-devel gmp gmp-devel libmcrypt libmcrypt-devel readline readline-devel libxslt libxslt-devel
yum install -y gcc gcc-c++  make zlib zlib-devel pcre pcre-devel  libjpeg libjpeg-devel libpng libpng-devel freetype freetype-devel libxml2 libxml2-devel glibc glibc-devel glib2 glib2-devel bzip2 bzip2-devel ncurses ncurses-devel curl curl-devel e2fsprogs e2fsprogs-devel krb5 krb5-devel openssl openssl-devel openldap openldap-devel nss_ldap openldap-clients openldap-servers





cp -frp /usr/lib64/libldap* /usr/lib/
ln -s /usr/local/lib/libiconv.so.2 /usr/lib64/

echo '/usr/local/lib64
/usr/local/lib
/usr/lib
/usr/lib64'>>/etc/ld.so.conf
# 更新配置
ldconfig -v

ln -s /usr/lib64/liblber* /usr/lib/ 



###############################开始安装php###############################

yum -y remove php*
pkill php

phpversion=php-7.3.26
rm -rf $phpversion
if [ ! -f $phpversion.tar.gz ];then
  wget -c  https://www.php.net/distributions/$phpversion.tar.gz
  
fi

tar zxf $phpversion.tar.gz
cd $phpversion

./configure \
--prefix=/usr/local/php \
--with-config-file-path=/usr/local/php/etc \
--with-mysqli --with-pdo-mysql \
--with-iconv-dir --with-freetype-dir --with-jpeg-dir --with-png-dir \
--with-zlib --with-libxml-dir --enable-simplexml --enable-xml --disable-rpath \
--enable-bcmath --enable-soap --enable-zip  --enable-fpm \
--with-fpm-user=www --with-fpm-group=www \
--enable-mbstring --enable-sockets   \
--with-gd --with-gettext --with-curl  --with-ldap \
--with-openssl --with-mhash --enable-opcache  --disable-debug  --disable-fileinfo

# enable 是启用 PHP 源码包自带，但是默认不启用的扩展，比如 ftp 和 exif 扩展。
# with 是指定扩展依赖的资源库的位置，如果是默认位置，就可以留空


if [ $CPU_NUM -gt 1 ];then
    make ZEND_EXTRA_LIBS='-liconv -llber' -j$CPU_NUM
else
    make ZEND_EXTRA_LIBS='-liconv -llber'
fi
make install



# adjust php.ini 是php解析器的配置文件
cp  php.ini-production  /usr/local/php/etc/php.ini
sed -i 's/post_max_size = 8M/post_max_size = 128M/g' /usr/local/php/etc/php.ini
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 128M/g' /usr/local/php/etc/php.ini
sed -i 's/;date.timezone =/date.timezone = PRC/g' /usr/local/php/etc/php.ini
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/g' /usr/local/php/etc/php.ini
sed -i 's/max_execution_time = 30/max_execution_time = 300/g' /usr/local/php/etc/php.ini
sed -i "/^max_input_time/c max_input_time = 300 "  /usr/local/php/etc/php.ini

sed -i "/^mysqli.default_socket/c mysqli.default_socket = /var/lib/mysql/mysql.sock"  /usr/local/php/etc/php.ini

# adjust php-fpm 是应用配置文件
cp /usr/local/php/etc/php-fpm.conf.default /usr/local/php/etc/php-fpm.conf
cp /usr/local/php/etc/php-fpm.d/www.conf.default  /usr/local/php/etc/php-fpm.d/www.conf

# sed -i 's,user = nobody,user=www,g'  /usr/local/php/etc/php-fpm.d/www.conf
# sed -i 's,group = nobody,group=www,g' /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,^pm.min_spare_servers = 1,pm.min_spare_servers = 5,g'  /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,^pm.max_spare_servers = 3,pm.max_spare_servers = 35,g'  /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,^pm.max_children = 5,pm.max_children = 100,g'  /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,^pm.start_servers = 2,pm.start_servers = 20,g'  /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,;pid = run/php-fpm.pid,pid = run/php-fpm.pid,g'  /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,;error_log = log/php-fpm.log,error_log = log/php-fpm.log,g'   /usr/local/php/etc/php-fpm.d/www.conf
sed -i 's,;slowlog = log/$pool.log.slow,slowlog = log/\$pool.log.slow,g'   /usr/local/php/etc/php-fpm.d/www.conf
sed -i  "s/;always_populate/always_populate/" /usr/local/php/etc/php-fpm.d/www.conf


#self start
install -v -m755 sapi/fpm/init.d.php-fpm  /etc/init.d/php-fpm

groupadd --system www
useradd --system  -g www -d /www -s /sbin/nologin -c "Web service" www
# useradd  -M -s /sbin/nologin -g www  -d /www www
# 添加www组和www用户



/etc/init.d/php-fpm start

