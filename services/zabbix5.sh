#!/bin/bash
# bash <(curl -sL http://wuyou.run/services/zabbix5.sh)


# 编译安装官方文档: https://www.zabbix.com/documentation/current/manual/installation/install
# curl -s  http://zabbix.dt-pn1.com|grep -wPo  "\d{0,3}\.\d{0,3}" 
function message()
{
  case "$1" in
      "warn")
      echo -e "\e[1;31m$2\e[0m"
      ;;
      "notice")
      echo -e "\e[1;35m$2\e[0m"
      ;;
      "info")
      echo -e "\e[1;33m$2\e[0m"
      ;;
  esac
}

pkill zabbix
rm /usr/local/zabbix/ /etc/init.d/zabbix_* /etc/zabbix/  /zabbix/  -rf
message "notice" "清理zabbix残余文件"


read -p 'mysql localhost password:' pwd

if   [ ! -f zabbix-*.tar.gz  ];then
  wget -c https://cdn.zabbix.com/zabbix/sources/stable/5.0/zabbix-5.0.7.tar.gz 
fi


yum install  -y mlocate c++ gcc telnet openldap openldap-devel  net-snmp-devel net-snmp-utils  curl-devel unixODBC-devel OpenIPMI-devel libssh2-devel  java-1.8.0-openjdk-devel java-1.8.0-openjdk
#  autoconf 
updatedb
mysql_config=`locate -r  mysql_config$`
echo $mysql_config
tar zxvf  zabbix-*.tar.gz
cd  zabbix-*

groupadd --system zabbix
useradd --system -g zabbix -d /usr/local/zabbix/ -s /sbin/nologin -c "Zabbix Monitoring System" zabbix

./configure user=zabbix group=zabbix \
--prefix=/usr/local/zabbix --sysconfdir=/etc/zabbix \
--enable-server --enable-proxy --enable-agent \
--enable-ipv6 --enable-java  --with-net-snmp --with-libcurl \
--with-openipmi --with-unixodbc --with-ldap --with-ssh2 \
--with-mysql=$mysql_config

if [ $? -ne 0 ];then
 message "warn" "编译失败";exit 1
fi

make install
message "info" "编译安装完毕 ！"

mysqladmin  -p$pwd ping  2>&1| grep  -c alive
if [ $? -ne 0 ];then
 message "warn" "mysql is off";exit 1
fi


mysql -p$pwd -e "select version()"
if [ $? -ne 0 ];then
 message "warn" "Mysql localhost password validation fails ！";exit 1
fi


#数据库导入: https://www.zabbix.com/documentation/current/manual/appendix/install/db_scripts
zbx_pwd=$(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 9) #　随机密码
mysql  -p$pwd -e "drop database if exists zabbix;"
message   "notice"  "删除数据库"
mysql  -p$pwd -e   "create database zabbix character set utf8 collate utf8_bin;"
message "info" "创建zabbix数据库"
mysql  -p$pwd -e   "create user  'zabbix'@'localhost'   IDENTIFIED by $zbx_pwd;"
mysql  -p$pwd -e   "grant all privileges on zabbix.* to  'zabbix'@'localhost';"
message "info"  "创建zabbix数据库登录账号密码"


mysql  -p$pwd  -f zabbix < database/mysql/schema.sql
# stop here if you are creating database for Zabbix proxy
mysql  -p$pwd   -f zabbix < database/mysql/images.sql
mysql  -p$pwd  -f zabbix < database/mysql/data.sql
message "info" "数据导入成功 ！"

# web站点配置
ps aux |grep php
if [ $? -ne 0 ];then
 message "warn" "php is off";exit 1
fi

www=`ps axu |grep  pool |grep -v grep| awk '{print $1}'|uniq`

mkdir /zabbix
cp -axv ui /zabbix
chown $www.$www  /zabbix
message "info"  "拷贝文件到站点；添加目录所属"

cp misc/init.d/fedora/core/zabbix_* /etc/init.d/ 
chmod 755 /etc/init.d/zabbix_*
message "info"  "拷贝执行程序到/etc/init.d"


sed -i "/BASEDIR=/c BASEDIR=/usr/local/zabbix" /etc/init.d/zabbix_server
sed -i "/BASEDIR=/c BASEDIR=/usr/local/zabbix" /etc/init.d/zabbix_agentd

cp /etc/zabbix/zabbix_server.conf /etc/zabbix/zabbix_server.conf_bak
cat >/etc/zabbix/zabbix_server.conf<<EOF
LogFile=/tmp/zabbix_server.log
DBHost=localhost
DBName=zabbix
DBUser=zabbix
DBPassword=$zbx_pwd
DBPort=3306

JavaGateway=127.0.0.1
JavaGatewayPort=10052
StartJavaPollers=5
StartDiscoverers=10

AlertScriptsPath=/etc/zabbix/alertscripts
ExternalScripts=/etc/zabbix/externalscripts
#外部脚本目录
#FpingLocation=/usr/sbin/fping
EOF
mkdir /etc/zabbix/alertscripts
mkdir /etc/zabbix/externalscripts
chown zabbix.zabbix /etc/zabbix/ -R



cat >/usr/local/src/zabbix.conf<<EOF
server {
    listen       80;
    server_name  _;
        index index.html  index.php;
        root /zabbix;
        location ~ .*\.(php|php5)?$
        {
                fastcgi_pass  127.0.0.1:9000;
                fastcgi_index index.php;
                include fastcgi.conf;
        }
        location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
        {
                expires 30d;
        }
        location ~ .*\.(js|css)?$
        {
                expires 1h;
        }
        access_log off;
}
EOF
echo '>>zabbix ok' > /usr/local/src/zabbix.txt 
echo 
echo 'web登录账号 Admin;zabbix' >>  /usr/local/src/zabbix.txt 
echo "mysql账号 zabbix;${zbx_pwd}" >>  /usr/local/src/zabbix.txt 
cat  /usr/local/src/zabbix.txt 
echo "重新加载nginx配置，打开http://$ip:80"