#!/bin/bash
# bash oracle-11g.sh 

# 清理历史数据
rm /home/oracle-11g/ /tmp/database/ -rf
userdel  -r oracle
# 创建oracle用户和组,设置oracle密码
groupadd oinstall
groupadd dba
useradd -g oinstall -G dba oracle
echo "oracle:oracle" | chpasswd

# 设置Oracle安装目录
ORACLE_BASE=/home/oracle-11g
ORACLE_HOME=$ORACLE_BASE/product/11.2.0

# 关闭安全策略
setenforce 0
sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
systemctl disable firewalld
systemctl stop firewalld

# 安装必要的依赖项
if [ ! -d "rpm" ]; then
   yum install -y libXext binutils compat-libcap1 compat-libstdc++-33 gcc gcc-c++ glibc glibc-devel ksh libaio libaio-devel libgcc libstdc++ libstdc++-devel libxcb make sysstat unzip
else
   echo "本地yum安装"
   yum  install -y rpm/*
fi


if [ $# -lt 1 ]; then
    HOSTNAME="pro-oracle-1" # 传参少于1个使用默认主机名
  else
    HOSTNAME=$1     # 使用参数作为主机名
  fi
  if [ -f /etc/hostname ]; then
    echo "$HOSTNAME" >/etc/hostname
    hostnamectl  set-hostname pro-oracle-1
  fi
  grep $HOSTNAME /etc/hosts || echo "127.0.0.1 $HOSTNAME" >>/etc/hosts


# 修改内核参数
cat << EOF >> /etc/sysctl.conf
fs.file-max = 6815744
kernel.sem = 250 32000 100 128
kernel.shmmni = 4096
kernel.shmall = 1073741824
kernel.shmmax = 4398046511104
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576
net.ipv4.ip_local_port_range = 9000 65500
EOF
sysctl -p

# 修改文件句柄限制
grep  oracle /etc/security/limits.conf || cat << EOF >> /etc/security/limits.conf
oracle soft nofile 1024
oracle hard nofile 65536
oracle soft nproc 2047
oracle hard nproc 16384
EOF

# 修改登录配置文件
grep pam_limits.so /etc/pam.d/login || cat << EOF >> /etc/pam.d/login
session required /lib64/security/pam_limits.so
session required pam_limits.so
EOF



# 创建所需的目录并授权给相应的用户和组
mkdir -p $ORACLE_HOME/dbs \
    $ORACLE_BASE/oraInventory \
    $ORACLE_BASE/install/rspfmt_dbinstall_response_schema_v11_2_0
chown -R oracle:oinstall $ORACLE_BASE
chmod -R 775 $ORACLE_BASE

# 修改bash_profile文件
grep ulimit /home/oracle/.bash_profile || cat << EOF >> /home/oracle/.bash_profile
if [ \$USER = "oracle" ]; then
  if [ \$SHELL = "/bin/ksh" ]; then
    ulimit -p 16384
    ulimit -n 65536
  else
    ulimit -u 16384 -n 65536
  fi
fi

export ORACLE_BASE=$ORACLE_BASE
export ORACLE_HOME=$ORACLE_HOME
export ORACLE_PID=orcl
export ORACLE_SID=orcl
export PATH=\$PATH:\$HOME/bin:\$ORACLE_HOME/bin
export LD_LIBRARY_PATH=\$ORACLE_HOME/lib:/usr/lib
export LANG="zh_CN.UTF-8"
export NLS_LANG="SIMPLIFIED CHINESE_CHINA.AL32UTF8"
export NLS_DATE_FORMAT='yyyy-mm-dd hh24:mi:ss'
# export DISPLAY=:0.0
EOF



# 判断是否已经解压
if [ ! -d "/tmp/database" ]; then
    # 解压Oracle 11g安装文件
    unzip linux.x64_11gR2_database_1of2.zip -d /tmp/
    unzip linux.x64_11gR2_database_2of2.zip -d /tmp/
fi

# 修改响应文件
cat << EOF > /tmp/database/response/db_install.rsp
oracle.install.responseFileVersion=$ORACLE_BASE/install/rspfmt_dbinstall_response_schema_v11_2_0
# 安装类型
oracle.install.option=INSTALL_DB_SWONLY
# 主机名称
ORACLE_HOSTNAME=$HOSTNAME
# 安装组
UNIX_GROUP_NAME=oinstall
# INVENTORY目录
INVENTORY_LOCATION=$ORACLE_BASE/inventory
# 选择语言
SELECTED_LANGUAGES=en,zh_CN
# oracle_home
ORACLE_HOME=$ORACLE_HOME 
# oracle_base 
ORACLE_BASE=$ORACLE_BASE
# oracle版本
oracle.install.db.InstallEdition=EE
oracle.install.db.isCustomInstall=true
oracle.install.db.customComponents=oracle.server:11.2.0.1.0,oracle.sysman.ccr:10.2.7.0.0,oracle.xdk:11.2.0.1.0,oracle.rdbms.oci:11.2.0.1.0,oracle.network:11.2.0.1.0,oracle.network.listener:11.2.0.1.0,oracle.rdbms:11.2.0.1.0,oracle.options:11.2.0.1.0,oracle.rdbms.partitioning:11.2.0.1.0,oracle.oraolap:11.2.0.1.0,oracle.rdbms.dm:11.2.0.1.0,oracle.rdbms.dv:11.2.0.1.0,orcle.rdbms.lbac:11.2.0.1.0,oracle.rdbms.rat:11.2.0.1.0
# dba用户组
oracle.install.db.DBA_GROUP=dba
# oper用户组
oracle.install.db.OPER_GROUP=oinstall
# 数据库类型
oracle.install.db.config.starterdb.type=GENERAL_PURPOSE
# globalDBName 全局数据库名称
oracle.install.db.config.starterdb.globalDBName=orcl
oracle.install.db.config.starterdb.SID=orcl
oracle.install.db.config.starterdb.characterSet=AL32UTF8
# 设置为 true 时，安装程序会使用自动内存管理
oracle.install.db.config.starterdb.memoryOption=true
# oracle.install.db.config.starterdb.memoryLimit=
oracle.install.db.config.starterdb.installExampleSchemas=false
oracle.install.db.config.starterdb.enableSecuritySettings=true
# 默认密码
oracle.install.db.config.starterdb.password.ALL=oracle
oracle.install.db.config.starterdb.password.SYS=oracle
oracle.install.db.config.starterdb.password.SYSTEM=oracle
# 手动管理安全更新
SECURITY_UPDATES_VIA_MYORACLESUPPORT=false
DECLINE_SECURITY_UPDATES=true

EOF


# 判断是否已经安装
if [ ! -f "$ORACLE_HOME/bin/sqlplus" ]; then
    # 安装Oracle 11g
     su - oracle -c '/tmp/database/runInstaller -silent -force  -ignorePrereq  -responseFile /tmp/database/response/db_install.rsp'
    # su - oracle -c '/tmp/database/runInstaller -debug -silent -force  -ignorePrereq  -responseFile /tmp/database/response/db_install.rsp'

    # 添加profile变量
    grep PS /etc/profile || echo '''PS1="\[\e[37;1m\][\[\e[32;1m\]\u\[\e[37;40m\]@\[\e[34;1m\]\h \[\e[0m\]\t \[\e[35;1m\]\W\[\e[37;1m\]]\[\e[m\]/\\$" ''' >>/etc/profile
    grep HISTTIMEFORMAT /etc/profile || cat >>/etc/profile <<EOF
export HISTTIMEFORMAT="%F %T \$(whoami) " 
export HISTSIZE=10000 
EOF
else
    echo "Oracle 11g已经安装过了!"
    exit 0
fi

while true
do
  if grep "Finished product-specific" $ORACLE_HOME/install/root_pro-oracle-*.log; then
                                                   
    break
  else
    echo "Waiting for Oracle installation to complete..."
    sleep 60
  fi
done
echo "Oracle 11g已成功安装!"







# echo "开始初始化数据库和启动数据库"
# # 创建初始化参数文件（init.ora）
# cat <<EOF > $ORACLE_HOME/dbs/init$ORACLE_SID.ora
# db_name='orcl'
# memory_target=1G
# processes=200
# control_files=($ORACLE_HOME/dbs/cntrlorcl.dbf)
# EOF


# cat << EOF >> $ORACLE_BASE/dbca.rsp
# GDBNAME = "orcl"
# SID = "orcl"
# SYSPASSWORD = "oracle"
# SYSTEMPASSWORD = "oracle"
# CHARACTERSET="AL32UTF8" 
# NATIONALCHARACTERSET="UTF8"
# EOF


# # 使用dbca创建数据库
# if [ ! -f "$ORACLE_HOME/dbs/orcl.dbf" ]; then
#     su - oracle -c "dbca -silent -createDatabase -responseFile $ORACLE_BASE/dbca.rsp"
#     echo "Oracle数据库已成功创建!"
# else
#     echo "Oracle数据库已经存在!"
# fi

# # 启动Oracle监听器
# cp /tmp/database/response/netca.rsp   /home/oracle/netca.rsp
# su - oracle -c "lsnrctl start"
# su - oracle -c "netca -silent -responsefile /home/oracle/netca.rsp"

# # 启动Oracle数据库
# su - oracle -c "sqlplus / as sysdba << EOF
# startup nomount;
# alter database mount;
# alter database open;
# exit;
# EOF"
# echo "Oracle数据库已成功启动!"

# # 创建表空间
# su - oracle -c "sqlplus / as sysdba << EOF
# CREATE TABLESPACE users DATAFILE '$ORACLE_HOME/dbs/users01.dbf' SIZE 100M AUTOEXTEND ON;
# exit;
# EOF"
# echo "表空间已成功创建!"