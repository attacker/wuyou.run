#!/bin/bash


# master节点安装：bash kubernets-install.sh master
# worker节点安装：bash kubernets-install.sh node


# 指定 Kubernetes 版本
kubernetes_version="1.27.4"

# 判断传递的参数，如果没有传递或传递的是错误参数，则默认安装master节点
node_type=${1:-"master"}

# 脚本用途说明
cat <<EOF
该脚本用于安装 Kubernetes 集群，并根据地区选择合适的镜像源。
请在运行脚本之前确认:
===================================================================
1. 安装集群Master节点: bash kubernets-install.sh master
2. 安装worker节点: kubernets-install.sh worker
3. 当前用户是 root 用户
4. 确保系统网络畅通，可以访问外部镜像源
5. 指定kubernetes安装版本
6. 默认使用flannel网络组件,可注释并改为install_network_plugin_calico
===================================================================
EOF

# 检查当前用户是否为 root 用户
check_root_user() {
    if [[ $EUID -ne 0 ]]; then
        echo "请使用 root 用户执行此脚本。"
        exit 1
    fi
}

# 判断是否为中国地区
is_china() {
    # 使用简单的方法判断，您也可以根据实际需求添加更多判断条件
    if [[ $(curl -sSL https://ipapi.co/country/) = "CN" ]]; then
        return 0
    else
        return 1
    fi
}

# 根据地区选择镜像源
select_country() {
    if is_china; then
        echo "检测在中国地区，将使用国内镜像源。"
        docker_image_repository="registry.aliyuncs.com/google_containers"
        yum_repository="https://mirrors.aliyun.com/kubernetes"
        apt_repository="https://mirrors.aliyun.com/kubernetes/apt"
        flannel="https://gitee.com/mirrors/flannel/raw/master/Documentation/kube-flannel.yml"
        calico="https://docs.projectcalico.org/v3.20/manifests/calico.yaml --image-repository=registry.cn-hangzhou.aliyuncs.com/calico"

    else
        echo "检测不在中国地区，将使用官方镜像源。"
        docker_image_repository="registry.k8s.io"
        yum_repository="https://packages.cloud.google.com"
        apt_repository="https://apt.kubernetes.io"
        flannel="https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml"
        calico="https://docs.projectcalico.org/v3.20/manifests/calico.yaml"
    fi
}

# 检查是否已安装 Kubernetes
check_kubernetes_installed() {
    if command -v kubeadm >/dev/null 2>&1; then
        echo "已检测到已安装的 Kubernetes。"
        read -p "是否卸载已存在的 Kubernetes？(y/n): " uninstall_choice
        if [[ $uninstall_choice = "y" || $uninstall_choice = "Y" ]]; then
            uninstall_kubernetes
        else
            echo "已取消安装。"
            exit 0
        fi
    fi
}
# 卸载 Kubernetes
uninstall_kubernetes() {
    echo "正在卸载 Kubernetes..."

    case $os in
    ubuntu)
        uninstall_kubernetes_ubuntu
        ;;
    centos)
        uninstall_kubernetes_centos
        ;;
    amazon_linux)
        uninstall_kubernetes_centos
        ;;
    *)
        echo "不支持的操作系统。"
        exit 1
        ;;
    esac

    echo "Kubernetes 已成功卸载。"
}

# 获取操作系统信息
get_os_info() {
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        if [[ $ID = "ubuntu" ]]; then
            os="ubuntu"
        elif [[ $ID = "centos" ]]; then
            os="centos"
        elif [[ $ID = "amzn" ]]; then
            os="amazon_linux"
        fi
    elif [ -f /etc/redhat-release ]; then
        if grep -q "CentOS Linux release 7" /etc/redhat-release; then
            os="centos"
        fi
    fi
}

# 卸载 Kubernetes（Ubuntu）
uninstall_kubernetes_ubuntu() {
    echo "正在卸载 Kubernetes..."
    if command -v kubeadm &>/dev/null; then
        kubeadm reset -f
    else
        echo "kubeadm 未找到，无法执行重置操作。请手动重置 Kubernetes。"
    fi
    if command -v kubectl &>/dev/null; then
        kubectl delete -f $flannel
        kubectl delete -f $calico
        apt remove -y kubeadm kubelet kubectl containerd
        rm -rf /etc/kubernetes /var/lib/etcd /var/lib/kubelet
    else
        echo "kubectl 未找到，无法执行删除操作。请手动删除相关资源。"
    fi
}

# 卸载 Kubernetes（CentOS）
uninstall_kubernetes_centos() {
    echo "正在卸载 Kubernetes..."
    if command -v kubectl &>/dev/null; then
        kubectl delete -f $flannel
        kubectl delete -f $calico
        yum --debuglevel=1 remove -y kubeadm kubelet kubectl containerd bash-completion
        yum autoremove -y
        rm -rf /etc/kubernetes /var/lib/etcd /var/lib/kubelet

    else
        echo "kubectl 未找到，无法执行删除操作。请手动删除相关资源。"
    fi

}

# 关闭并禁用防火墙（Ubuntu、CentOS）
disable_firewall() {
    echo "正在关闭并禁用防火墙..."
    if [[ $os = "ubuntu" ]]; then
        ufw disable
    elif [[ $os = "centos" || $os = "amazon_linux" ]]; then
        systemctl stop firewalld
        systemctl disable firewalld
        # 清空iptables策略
        iptables -F
        iptables -X
        iptables -Z
        iptables -F -t nat
        iptables -X -t nat
        iptables -Z -t nat
        iptables -P INPUT ACCEPT
        if [ -s /etc/selinux/config ]; then
            setenforce 0
            sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
        fi
    fi
}

# 关闭并禁用 Swap
disable_swap() {
    echo "正在关闭并禁用 Swap..."
    swapoff -a
    sed -i '/swap/d' /etc/fstab
}

# 优化内核参数
optimize_kernel() {
    echo "正在优化内核参数..."
    sysctl_file="/etc/sysctl.d/kubernetes.conf"
    echo "net.bridge.bridge-nf-call-ip6tables = 1" >$sysctl_file
    echo "net.bridge.bridge-nf-call-iptables = 1" >>$sysctl_file
    echo "net.ipv4.ip_forward=1" >>$sysctl_file
    echo "vm.max_map_count=262144" >>$sysctl_file
    sysctl -p $sysctl_file
}

# 禁用透明大页
disable_transparent_hugepage() {
    echo "禁用透明大页..."
    thp_file="/etc/systemd/system/disable-thp.service"
    echo "[Unit]" >$thp_file
    echo "Description=Disable Transparent Huge Pages (THP)" >>$thp_file
    echo "DefaultDependencies=no" >>$thp_file
    echo "After=local-fs.target" >>$thp_file
    echo "Before=apparmor.service" >>$thp_file
    echo "" >>$thp_file
    echo "[Service]" >>$thp_file
    echo "Type=oneshot" >>$thp_file
    echo "ExecStart=/bin/sh -c 'echo never > /sys/kernel/mm/transparent_hugepage/enabled && echo never > /sys/kernel/mm/transparent_hugepage/defrag'" >>$thp_file
    echo "" >>$thp_file
    echo "[Install]" >>$thp_file
    echo "WantedBy=multi-user.target" >>$thp_file
    chmod 664 $thp_file
    systemctl daemon-reload
    systemctl enable disable-thp
    systemctl start disable-thp
}

# 安装 kubeadm、kubelet 和 kubectl
install_kubernetes() {
    echo "正在安装 kubeadm、kubelet 和 kubectl（版本：$kubernetes_version）..."

    if [[ $os = "ubuntu" ]]; then
        apt update
        apt install -y apt-transport-https ca-certificates curl bridge-utils
        modprobe br_netfilter # 加载所需的内核模块
        curl -fsSL $apt_repository/doc/apt-key.gpg | apt-key add -
        echo "deb $apt_repository kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list

        apt update
        apt install -y kubeadm=$kubernetes_version-00 kubelet=$kubernetes_version-00 kubectl=$kubernetes_version-00
    elif [[ $os = "centos" || $os = "amazon_linux" ]]; then
        cat <<EOF >/etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=${yum_repository}/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=${yum_repository}/yum/doc/yum-key.gpg
${yum_repository}/yum/doc/rpm-package-key.gpg
EOF

        yum --debuglevel=1 install -y kubeadm-$kubernetes_version kubelet-$kubernetes_version kubectl-$kubernetes_version
        systemctl enable kubelet
        
        modprobe br_netfilter # 加载所需的内核模块
        echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables
        
        echo "添加bash-completion 自动补全"
        yum install bash-completion -y
        source /usr/share/bash-completion/bash_completion
        source <(kubectl completion bash)
        echo "source <(kubectl completion bash)" >>~/.bashrc
    fi
}

# 安装 Containerd
install_containerd() {
    echo "正在安装 Containerd..."
    if [[ $os = "centos" || $os = "amazon_linux" ]]; then
        yum install yum-utils -y
        yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
        yum --debuglevel=1 install -y containerd
    elif [[ $os = "ubuntu" ]]; then
        apt install -y containerd
    fi
    mkdir -p /etc/containerd
    # 生成默认配置
    containerd config default >/etc/containerd/config.toml
    # 配置 systemd cgroup 驱动程序
    sed -i 's#SystemdCgroup = false#SystemdCgroup = true#' /etc/containerd/config.toml
    sed -i "s#registry.k8s.io#${docker_image_repository}#" /etc/containerd/config.toml
    systemctl restart containerd
    systemctl enable containerd
}

# 执行 kubeadm init 并复制 kubeconfig 文件
initialize_kubernetes_cluster() {
    if command -v kubeadm &>/dev/null; then
        kubeadm reset -f
    else
        echo "kubeadm 未找到，无法执行重置操作。请手动重置 Kubernetes。"
        exit 1
    fi

    echo "正在执行 kubeadm init..."
    kubeadm init --kubernetes-version=v${kubernetes_version} \
        --image-repository=${docker_image_repository} \
        --service-cidr=10.96.0.0/16 \
        --pod-network-cidr=10.244.0.0/16 \
        -v=5

    # --kubernetes-version 指定要安装的Kubernetes版本
    # --image-repository=registry.k8s.io  容器镜像仓库默认地址
    # --service-cidr  Kubernetes Service的IP地址范围
    # --pod-network-cidr Kubernetes Pod的IP地址范围
    # --control-plane-endpoint=test-k8s-lb.opsbase.cn:6443 控制平面终结点地址,用于在高可用集群中指定负载均衡器的地址。
    echo "已成功执行 kubeadm init。"
    # ctr 查看镜像list
    ctr image ls
    echo "正在复制 kubeconfig 文件..."
    mkdir -p $HOME/.kube
    \cp /etc/kubernetes/admin.conf $HOME/.kube/config
    chown $(id -u):$(id -g) $HOME/.kube/config
    echo "kubeconfig 文件已复制到 $HOME/.kube/config。"
}

# 安装网络组件（Flannel）
install_network_plugin_flannel() {
    echo "正在安装 Flannel 网络组件..."
    echo $flannel
    kubectl apply -f $flannel
}

# 安装网络组件（Calico）
install_network_plugin_calico() {
    echo "正在安装 Calico 网络组件..."
    kubectl create -f $calico
}

# 主函数
main() {
    select_country
    get_os_info
    check_root_user
    check_kubernetes_installed
    disable_firewall
    disable_swap
    disable_transparent_hugepage
    install_kubernetes
    install_containerd
    optimize_kernel
    if [[ "$node_type" = "master" ]]; then
        initialize_kubernetes_cluster
        install_network_plugin_flannel
        # 如果想使用 Calico 网络组件，注释掉上面的 "flannel" 函数，然后取消"calico" 行的注释
        # install_network_plugin_calico
    else
        echo "slave节点,跳过集群初始化操作。"
    fi

}

# 主函数
main