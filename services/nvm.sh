#!/bin/bash
########################################################
# author: 以谁为师
# website: http://opsbase.cn
# description: Service installation script
# curl -s http://wuyou.run/services/nvm.sh |bash
########################################################


#### -----------  打印颜色 ----------- #### 
function color_message() {
  case "$1" in
  "error"|"red")
    echo -e "\e[1;31m$2\e[0m"
    ;;
  "warn"|"yellow")
    echo -e "\e[1;33m$2\e[0m"
    ;;
  "info"|"blue")
    echo -e "\e[1;34m$2\e[0m"
    ;;
  "success"|"green")
    echo -e "\e[1;32m$2\e[0m"
    ;;
  esac
}

color_message  "info" "---- install base package ----"
  yum --debuglevel=1 install git -y


color_message  "info" "---- installation package ----"
if   [ ! -f nvm_offline.tar.gz ];then
  git clone git://github.com/creationix/nvm.git ~/.nvm
  echo "source ~/.nvm/nvm.sh" >> ~/.bashrc
else
  tar zxvf nvm_offline.tar.gz -C ~
  echo "source ~/.nvm/nvm.sh" >> ~/.bashrc
fi