yum -y install pam pam-devel openldap-devel openssl-devel
###
 # @Author: admin@attacker.club
 # @Date: 2022-07-30 18:03:20
 # @LastEditTime: 2022-11-30 16:07:51
 # @Description: 
### 


if [ ! -f ss5*.gz ]; then
# wget http://superb-dca2.dl.sourceforge.net/project/ss5/ss5/3.8.9-8/ss5-3.8.9-8.tar.gz
    wget  "--no-check-certificate"  https://udomain.dl.sourceforge.net/project/ss5/ss5/3.8.9-8/ss5-3.8.9-8.tar.gz
fi

tar zxf ss5-*.gz
cd ss5-*
./configure
make &&make install
chmod +x /etc/init.d/ss5


cat >/etc/opt/ss5/ss5.conf <<EOF
auth 0.0.0.0/0 - -
permit - 0.0.0.0/0 - 0.0.0.0/0 - - - - -
EOF

grep 10800  /etc/sysconfig/ss5 ||cat > /etc/sysconfig/ss5<<EOF
SS5_OPTS=" -u root -b 0.0.0.0:55557"
#修改默认端口
EOF

# 开机自启动(3.8.9-8的一个bug，重启会删掉/var/run/ss5/,导致开机自启动时无法创建pid文件)
mkdir /var/run/ss5/

/etc/init.d/ss5  restart