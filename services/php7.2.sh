#!/bin/bash

yum install epel-release -y
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

yum -y remove php*
# 清理 旧php

# yum -y install php72w php72w-cli php72w-fpm php72w-common php72w-devel 
# php精简拓展

yum -y install php72w php72w-cli php72w-fpm php72w-common php72w-devel php72w-embedded php72w-gd php72w-mbstring php72w-mysqlnd php72w-opcache php72w-pdo php72w-xml
# 豪华版拓展

# yum list php* 查看可以安装的php版本

systemctl enable php-fpm && systemctl start php-fpm
# 启动服务