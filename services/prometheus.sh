#!/bin/bash
###
 # @author: 以谁为师
 # @site: opsbase.cn
 # @Date: 2022-03-23 23:40:32
 # @LastEditTime: 2022-03-24 00:06:06
 # @Description: curl -s http://wuyou.run/services/prometheus.sh|bash
### 

version=`curl -s  https://github.com/prometheus/prometheus/releases| grep linux-amd64.tar.gz|grep -oP  "\d+\.\d+\.\d+" |head -1`

if [ ! -f prometheus-*linux-amd64.tar.gz ];then
    wget -c https://github.com/prometheus/prometheus/releases/download/v${version}/prometheus-${version}.linux-amd64.tar.gz
fi

tar zxvf prometheus-*.linux-amd64.tar.gz -C /usr/local
mv /usr/local/prometheus-*.linux-amd64  /usr/local/prometheus


cat >/etc/systemd/system/prometheus.service << EOF
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/
After=network.target

[Service]
# Type设置为notify时服务会不断重启
Type=simple
User=prometheus

# --storage.tsdb.path是可选项，默认数据目录在运行目录的./dada目录中
ExecStart=/usr/local/prometheus/prometheus --config.file=/usr/local/prometheus/prometheus.yml  \
--storage.tsdb.path=/opt/data/prometheus
Restart=on-failure
[Install]

WantedBy=multi-user.target
EOF
groupadd prometheus
useradd -g prometheus -m -d /var/lib/prometheus -s /sbin/nologin prometheus
mkdir /opt/data/prometheus -p # 数据盘
chown prometheus.prometheus -R /usr/local/prometheus /opt/data/prometheus

systemctl daemon-reload
systemctl restart prometheus && systemctl enable prometheus
systemctl status prometheus