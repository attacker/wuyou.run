#!/bin/bash
###
# @Author: Logan.Li
# @Gitee: https://gitee.com/attacker
# @email: admin@attacker.club
# @Date: 2023-10-14 00:28:10
# @LastEditTime: 2023-10-14 02:03:23
# @Description:
###

# 设置源代码目录
source_dir="/usr/local/src"

# 安装依赖
sudo yum -y install wget gcc make zlib-devel perl-CPAN

# 进入源代码目录
cd $source_dir
if [ ! -f openssl-1.1.1w.tar.gz ]; then
    wget --no-check-certificate https://www.openssl.org/source/openssl-1.1.1w.tar.gz
fi

# 解压
tar -xzf openssl-*.tar.gz

# 进入 OpenSSL 目录
cd openssl-*
# 配置、编译和安装 OpenSSL
./config shared
make
sudo make install

# 更新动态链接器缓存
ldconfig
# 显示已安装 OpenSSL 的版本
openssl version
