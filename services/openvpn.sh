#!/bin/bash
# curl -s http://wuyou.run/services/openvpn.sh |bash

yum remove openvpn -y
rm /etc/openvpn -rf
source /etc/profile
pkill openvpn
yum --debuglevel=1 remove openvpn -y
rm -rf /etc/openvpn
yum --debuglevel=1 install openvpn openvpn-auth-ldap openssl-devel -y
if [ -d /etc/openvpn ]; then
    cd /etc/openvpn
else
    echo "没有发现openvpn目录,安装失败"
    exit 0
fi
# 证书
curl -fsSL -O --max-time 10 --retry 3 --retry-delay 5 http://wuyou.run/services/openvpn.tar.gz
tar zxf openvpn.tar.gz
chmod +x checkpwd.sh

##### openvpn配置文件 #####
cat >/etc/openvpn/server.conf <<EOF
user openvpn
group openvpn

# 选择协议和端口
proto tcp  # 支持TCP/UDP
port 1194  # 服务器监听端口

# VPN通道类型
dev tun    # 支持创建TUN路由IP通道或TAP以太网通道

# 保持连接
keepalive 10 120  # 保持连接的时间，默认为30秒
max-clients 1000 # 设置最大的客户端用户数，没有这句则默认无限

# 持久性设置
persist-key  # 通过keepalive检测超时后，重新启动VPN，不重新读取keys，保留第一次使用的keys
persist-tun  # 检测超时后，重新启动VPN，一直保持TUN是linkup的。否则网络会先linkdown然后再linkup
duplicate-cn # 允许多个客户端使用相同的公共证书登录

# 证书和认证设置
ca ca.crt
cert server.crt
key server.key
dh dh.pem

# 用户名和密码方式登录
script-security 3
verify-client-cert none
username-as-common-name
auth-user-pass-verify /etc/openvpn/checkpwd.sh via-env

ifconfig-pool-persist ipp.txt 
server 10.8.0.0 255.255.255.0  # VPN客户端网段
#push "dhcp-option DNS 223.5.5.5"  # 下发dns

# 下发路由
#push "route 192.168.0.0 255.255.255.0 vpn_gateway"  # 下发指定路由
push "redirect-gateway def1"  # 下发默认路由

# 日志设置
verb 3
log /etc/openvpn/openvpn.log
log-append /etc/openvpn/openvpn.log
status /etc/openvpn/openvpn-status.log

EOF

touch openvpn-password.log
chown openvpn.openvpn * -R

cat >/etc/openvpn/psw-file <<EOF
admin vpnpass
EOF

##### 配置转发 #####
echo 1 >/proc/sys/net/ipv4/ip_forward
grep ip_forward /etc/sysctl.conf || echo "net.ipv4.ip_forward=1" >>/etc/sysctl.conf
# 地址伪装转换
iptables -t nat -I POSTROUTING -j MASQUERADE
# iptables -t nat -D POSTROUTING -s 10.8.0.0/24 -j SNAT --to-source 192.168.0.21

##### 服务启动 #####
systemctl enable openvpn@server && systemctl restart openvpn@server

##### vpn客户端 #####
cat >/etc/openvpn/client/client.ovpn <<EOF
client
dev tun   # 支持创建tun路由ip通道或tap以太网通道
port 1194 # 服务器监听端口
proto udp # 支持TCP/udp
remote ddns.attacker.club # vpn服务器地址
ca ca.crt # ca证书

######## 客户端指定vpn服务器为默认网关 ########
#redirect-gateway def1 # 默认客户端所有流量走vpn网关

######## 客户端指定路由  ########
#route-nopull     # 客户端连接openvpn后不从服务端拉取路由表
#max-routes 1000  # 设置路由的最大条数,默认是100
#route 192.168.2.0 255.255.255.0 vpn_gateway # 使192.168.2.0/24网段,走vpn网关

# 断线自动重新连
resolv-retry infinite

auth-nocache    # 密码不存内存
auth-user-pass  # 输入密码认证
#auth-user-pass pass.txt # 保存自动登录密码

route-method exe
# ns-cert-type server #弃用
remote-cert-tls server
route-delay 2
verb 3
EOF

cp /etc/openvpn/ca.crt /etc/openvpn/client/
