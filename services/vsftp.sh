#!/bin/bash
###
 # @Author: admin@attacker.club
 # @Date: 2022-07-30 18:03:20
 # @LastEditTime: 2022-10-13 23:18:09
 # @Description: 
 # 1. 主动模式: 传输端口21 数据端口20。 局域网 或 防火墙，路由器NAT地址转换和端口转发
 # 2. 被动模式 服务端需要指定被动端口,外层防火墙，网关，slb等需要放开所有指定的被动端口
### 

Install() {
  

  yum remove vsftpd -y && rm /etc/vsftpd/ -rf
  yum install vsftpd -y
}

Set_conf() {

  cat >/etc/vsftpd/vsftpd.conf <<EOF
anonymous_enable=NO
# 禁止匿名用户访问
anon_upload_enable=NO
# 禁止匿名用户上传
anon_mkdir_write_enable=NO
# 取消匿名写入权限
local_enable=YES
# 允许本地用户登录
write_enable=YES
# 给用户写入权限
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
xferlog_std_format=YES

pam_service_name=vsftpd
tcp_wrappers=YES



userlist_deny=NO
# 设置＂NO＂为白名单，加入user_list里的用户才能访问
chroot_local_user=YES
# 伪根，限制ftp用户只能在其主目录下
allow_writeable_chroot=YES
# 根目录可写

# seccomp_sandbox=NO　
# 500 OOPS: 421 Service not available, remote server has closed connection

connect_from_port_20=YES
listen_ipv6=NO
listen=YES
listen_port=6021

# 启用被动模式
# pasv_enable=YES
# pasv_min_port=35000
# pasv_max_port=35500
EOF
}

Run() {
  #repo=$(df | grep dev | sort -nrk 2 | head -1 | awk '''{if(length($NF)==1) print $NF"repo";else print $NF"/repo"}''')
  # 最大分区做ftp家目录；是根目录不加 "/"

  userdel ftpcenter &>/dev/null
  # 清理历史账号
  useradd ftpcenter
  # 添加用户

  service vsftpd restart
  chkconfig vsftpd on
}

set_passwd() {
  # read -p 'set password:' pwd
  pwd=$(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 9) #　随机密码
  echo "${pwd}" | passwd --stdin "ftpcenter"             #　修改密码
}

Install
Set_conf
Run
set_passwd

echo "ftp账号:ftpcenter/${pwd}" >passwd.txt
echo "++++++++++++++++ Complete the installation　！ +++++++++++++++++"