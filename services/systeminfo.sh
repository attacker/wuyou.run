#!/bin/bash

########################################################
# author: admin@attacker.club
# website: http://opsbase.cn
# description: Service installation script
#  curl -sL http://wuyou.run/go/systeminfo.sh | bash
########################################################



RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
SKYBLUE='\033[0;36m'
PLAIN='\033[0m'


get_system_info() {
	cname=$( awk -F: '/model name/ {name=$2} END {print name}' /proc/cpuinfo | sed 's/^[ \t]*//;s/[ \t]*$//' )
	cores=$( awk -F: '/model name/ {core++} END {print core}' /proc/cpuinfo )
	freq=$( awk -F: '/cpu MHz/ {freq=$2} END {print freq}' /proc/cpuinfo | sed 's/^[ \t]*//;s/[ \t]*$//' )
	corescache=$( awk -F: '/cache size/ {cache=$2} END {print cache}' /proc/cpuinfo | sed 's/^[ \t]*//;s/[ \t]*$//' )
	tram=$( free -m | awk '/Mem/ {print $2}' )
	uram=$( free -m | awk '/Mem/ {print $3}' )
	bram=$( free -m | awk '/Mem/ {print $6}' )
	swap=$( free -m | awk '/Swap/ {print $2}' )
	uswap=$( free -m | awk '/Swap/ {print $3}' )
	up=$( awk '{a=$1/86400;b=($1%86400)/3600;c=($1%3600)/60} {printf("%d days %d hour %d min\n",a,b,c)}' /proc/uptime )
	load=$( w | head -1 | awk -F'load average:' '{print $2}' | sed 's/^[ \t]*//;s/[ \t]*$//' )
	opsy=$( get_opsy )
	arch=$( uname -m )
	lbit=$( getconf LONG_BIT )
	kern=$( uname -r )

  disk_unit=$(lsblk |egrep '^(v|s)d[a-z]' |awk '{print $4}' |sed -n '1p' |sed 's/\(.*\)\(.\)$/\2/')
  disk_space=$(lsblk |egrep '^(v|s)d[a-z]' |awk '{print $4}'|sed 's/[a-Z]//'|awk '{disk[$1]++} END {for(i in disk){print i}}' |awk '{sum +=$1};END{print sum}')
	disk_total_size=${disk_space}${disk_unit} 
	disk_used_size=$(df -kTP|awk '/^\/dev/ {sum += $(NF-3)};END { printf ("%.1f",sum/1024/1024)}')

	tcpctrl=$( sysctl net.ipv4.tcp_congestion_control | awk -F ' ' '{print $3}' )

	virt_check
}



get_opsy() {
    [ -f /etc/redhat-release ] && awk '{print ($1,$3~/^[0-9]/?$3:$4)}' /etc/redhat-release && return
    [ -f /etc/os-release ] && awk -F'[= "]' '/PRETTY_NAME/{print $3,$4,$5}' /etc/os-release && return
    [ -f /etc/lsb-release ] && awk -F'[="]+' '/DESCRIPTION/{print $2}' /etc/lsb-release && return
}


virt_check(){
        if hash ifconfig 2>/dev/null; then
                eth=$(ifconfig)
        fi

        virtualx=$(dmesg) 2>/dev/null

    if  [ $(which dmidecode) ]; then
                sys_manu=$(dmidecode -s system-manufacturer) 2>/dev/null
                sys_product=$(dmidecode -s system-product-name) 2>/dev/null
                sys_ver=$(dmidecode -s system-version) 2>/dev/null
        else
                sys_manu=""
                sys_product=""
                sys_ver=""
        fi

        if grep docker /proc/1/cgroup -qa; then
            virtual="Docker"
        elif grep lxc /proc/1/cgroup -qa; then
                virtual="Lxc"
        elif grep -qa container=lxc /proc/1/environ; then
                virtual="Lxc"
        elif [[ -f /proc/user_beancounters ]]; then
                virtual="OpenVZ"
        elif [[ "$virtualx" == *kvm-clock* ]]; then
                virtual="KVM"
        elif [[ "$cname" == *KVM* ]]; then
                virtual="KVM"
        elif [[ "$cname" == *QEMU* ]]; then
                virtual="KVM"
        elif [[ "$virtualx" == *"VMware Virtual Platform"* ]]; then
                virtual="VMware"
        elif [[ "$virtualx" == *"Parallels Software International"* ]]; then
                virtual="Parallels"
        elif [[ "$virtualx" == *VirtualBox* ]]; then
                virtual="VirtualBox"
        elif [[ -e /proc/xen ]]; then
                virtual="Xen"
        elif [[ "$sys_manu" == *"Microsoft Corporation"* ]]; then
                if [[ "$sys_product" == *"Virtual Machine"* ]]; then
                        if [[ "$sys_ver" == *"7.0"* || "$sys_ver" == *"Hyper-V" ]]; then
                                virtual="Hyper-V"
                        else
                                virtual="Microsoft Virtual Machine"
                        fi
                fi
        else
                virtual="Dedicated"
        fi
}



print_system_info() {
        echo -e " CPU Model            : ${SKYBLUE}$cname${PLAIN}" >$log
        echo -e " CPU Cores            : ${YELLOW}$cores Cores ${SKYBLUE}$freq MHz $arch${PLAIN}" | tee -a $log
        echo -e " CPU Cache            : ${SKYBLUE}$corescache ${PLAIN}" | tee -a $log
        echo -e " OS                   : ${SKYBLUE}$opsy ($lbit Bit) ${YELLOW}$virtual${PLAIN}" | tee -a $log
        echo -e " Kernel               : ${SKYBLUE}$kern${PLAIN}" | tee -a $log
        echo -e " Total Space          : ${SKYBLUE}$disk_used_size GB / ${YELLOW}$disk_total_size ${PLAIN}" | tee -a $log
        echo -e " Total RAM            : ${SKYBLUE}$uram MB / ${YELLOW}$tram MB ${SKYBLUE}($bram MB Buff)${PLAIN}" | tee -a $log
        echo -e " Total SWAP           : ${SKYBLUE}$uswap MB / $swap MB${PLAIN}" | tee -a $log
        echo -e " Uptime               : ${SKYBLUE}$up${PLAIN}" | tee -a $log
        echo -e " Load Average         : ${SKYBLUE}$load${PLAIN}" | tee -a $log
        echo -e " TCP CC               : ${YELLOW}$tcpctrl${PLAIN}" | tee -a $log

        sed  -i 's/\x1b\[[^\x1b]*m//g' $log # 除去颜色控制字符
}


main(){
  log=system.log
  get_system_info
  print_system_info
}


main