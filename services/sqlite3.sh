#!/bin/bash


# 指定期望的 SQLite 版本
sqlite_version="3440200"

# 下载 SQLite 源代码
wget https://www.sqlite.org/2023/sqlite-autoconf-${sqlite_version}.tar.gz

# 解压源代码
tar xvf sqlite-autoconf-${sqlite_version}.tar.gz

# 进入源代码目录
cd sqlite-autoconf-${sqlite_version}

# 配置、编译和安装 SQLite
./configure --prefix=/usr/local/sqlite3.${sqlite_version}
make
make install

# 验证安装
/usr/local/bin/sqlite3 --version

# 替换系统默认的 sqlite3
mv /usr/bin/sqlite3 /usr/bin/sqlite3_backup
ln -s /usr/local/bin/sqlite3 /usr/bin/sqlite3

# 验证替换
sqlite3 --version
# 将路径传递给共享库
export LD_LIBRARY_PATH="/usr/local/lib"

echo "SQLite3 已成功安装，并设置为默认版本。"