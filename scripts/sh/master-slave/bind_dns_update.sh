#!/bin/bash

nowtime=`date +'%Y-%m-%d %H:%M:%S'`

num=`find /var/named/app/  -type f -mmin -5  |wc -l`
# 查找半小时内变动的dns文件

if [ $num -ne  0 ];then
  systemctl restart named
  echo "${nowtime}  complete"  >> name_update.log
fi




grep named  /var/spool/cron/root &>/dev/null  ||echo  '*/3 * * * * /root/named_update.sh'  >>  /var/spool/cron/root
# crontab计划任务

