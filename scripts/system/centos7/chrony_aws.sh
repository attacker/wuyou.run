#!/bin/bash

yum install -y chrony

cat >  /etc/chrony.conf<< EOF
server 0.amazon.pool.ntp.org iburst
server 1.amazon.pool.ntp.org iburst
server 2.amazon.pool.ntp.org iburst
server 3.amazon.pool.ntp.org iburst

driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
logdir /var/log/chrony
local stratum 10
# 即使server指令中时间服务器不可用，也允许将本地时间作为标准时间授时给其它客户端
EOF


systemctl enable chronyd && systemctl start chronyd
chronyc sources # 检查时间同步