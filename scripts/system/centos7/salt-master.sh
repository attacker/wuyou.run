#!/bin/bash
###
# @Author: 以谁为师
# @Website: attacker.club
# @Date: 2020-08-29 12:30:41
# @LastEditTime: 2020-08-29 14:33:04
# @Description:
###

rpm --import https://repo.saltstack.com/yum/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub

cat >/etc/yum.repos.d/saltstack.repo <<EOF
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS \$releasever
baseurl=https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest/SALTSTACK-GPG-KEY.pub
EOF

yum install salt-master salt-minion \
  salt-ssh \
  salt-syndic \
  salt-cloud -y

cat >/etc/salt/master <<EOF
max_open_files: 100000 # 文件硬限制
worker_threads: 5 # 线程的数量
zmq_backlog: 1000 # 监听队列大小/ backlog
pub_hwm: 1000 # 发布者接口 ZeroMQPubServerChannel
batch_safe_limit: 100 # 用来自动切换到批处理模式执行
batch_safe_size: 8    #   默认8

auto_accept: True # 自动确认salt-key
hash_type: sha256

# 指定files和pillar环境的路径
file_roots:
  base:
    - /srv/salt/base
  dev:
    - /srv/salt/dev
  prod:
    - /srv/salt/prod
  ci:
    - /data/jenkins

#pillar_opts: True
pillar_roots:
  base:
    - /srv/salt/pillar

# 主机分组
nodegroups:
  web: 'E@nginx and E@web'
# mongo: 'E@mongo*'
# redis_and_mongo: 'E@mongodb and  E@redis'
  group1: 'L@host172,host174' # 基于域名
  group2: 'S@192.168.10.172'  # 基于ip地址
  group3: 'P@os:CentOS'       # 基于Grains的正则匹配
  group4: 'E@xx-web'          #  基于minion id的主机名正则匹配(常用)
  apps: '*  and  not N@web and not N@base and  not N@db'
# salt -N apps test.ping #检测应用虚拟机

cachedir: /var/cache/salt/master
# 存放缓存信息目录
keep_jobs: 2
# 缓存时间
log_level: warning
# 日志级别
EOF

cd /srv
git clone git@gitee.com:attacker/salt.git
# 下载自定义配置

systemctl restart salt-master
