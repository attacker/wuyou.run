#!/bin/bash
###
# @Author: 以谁为师
# @Website: attacker.club
# @Date: 2020-08-29 12:30:41
# @LastEditTime: 2020-08-29 15:03:14
# @Description: bash <(curl -s  http://wuyou.run/scripts/system/centos7/salt-minion.sh)
Master address:salt.opsbase.cn
###

rpm --import https://repo.saltstack.com/yum/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub

cat >/etc/yum.repos.d/saltstack.repo <<EOF
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS \$releasever
baseurl=https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest/SALTSTACK-GPG-KEY.pub
EOF

Salt_minion() {
  #sudo wget -P /etc/yum.repos.d/  yum.ops.net/salt-repo/salt-repo.repo
  sudo yum install salt-minion -y

  cat >/etc/salt/minion <<EOF
master: ${host}
#master:
#    - salt.ops.net
#    - salt2.ops.net

id: $HOSTNAME
tcp_keepalive: True
tcp_keepalive_idle: 300
tcp_keepalive_cnt: -1
tcp_keepalive_intvl: -1
EOF

  sudo rm /etc/salt/pki/minion -rf && sudo chkconfig salt-minion on && sudo service salt-minion restart
}

read -p 'Master address:' host
Salt_minion
