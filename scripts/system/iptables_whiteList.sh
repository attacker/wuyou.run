#!/bin/bash

#############---- 安装iptables ----#############
if [ ! -f /etc/sysconfig/iptables  ];then
yum install  iptables-services -y  # 安装iptables防火墙
chkconfig iptables on
systemctl enable  iptables
systemctl disable  firewalld
# 开机启动项
systemctl stop  firewalld
service iptables restart
fi


###########---- 清空iptables策略 ----###########
iptables -F # 默认清空filter表
iptables -t nat -F # 清空nat表
iptables -X
iptables -t nat -X

##########################################---- INPUT链配置 ----#######################################
iptables -P INPUT DROP  # INPUT默认拒绝
iptables -A INPUT -i lo -j ACCEPT # 允许本地数据包
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT # 建立起来的连接保持允许

## 开放http、https、zabbix-agent、vpn...服务
#iptables -A INPUT -p tcp -m state --state NEW -m tcp --dport 30000:35000 -j ACCEPT
iptables -A INPUT -p tcp -m multiport --dports 22,2021,6022,2023 -j ACCEPT


iptables -A INPUT -p gre  -j ACCEPT #GRE端口打开
iptables -A INPUT -p tcp --dport 53 -j ACCEPT #允许DNS服务端口的tcp数据包流入
iptables -A INPUT -p udp --dport 53 -j ACCEPT #允许DNS服务端口的udp数据包流入

## ICMP协议
#iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
#iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
#iptables -A INPUT -p icmp --icmp-type fragmentation-needed -j ACCEPT
#iptables -I INPUT  -p icmp -j ACCEPT

# '---- 放行白名单地址 ----'
iptables -A INPUT -p tcp -s 183.247.191.164 -j ACCEPT
iptables -A INPUT -p tcp -s 172.16.0.0/8 -j ACCEPT

################---- 安全类-SYN ----################
## 防止SYN攻击 轻量
iptables -N syn-flood
iptables -A INPUT -p tcp --syn -j syn-flood
iptables -I syn-flood -p tcp -m limit --limit 3/s --limit-burst 6 -j RETURN
iptables -A syn-flood -j REJECT
## 打开 syncookie （轻量级预防 DOS 攻击）
sysctl -w net.ipv4.tcp_syncookies=1 &>/dev/null


iptables-save &>/dev/null
service iptables save &>/dev/null
#保存iptable防火墙配置到本地