#!/bin/bash
###
# @Author: 以谁为师
# @Website: attacker.club
# @Date: 2020-08-05 10:40:47
# @LastEditTime: 2020-08-24 14:53:05
# @Description: Internet initialization scripts
###

Set_limits() {
  echo -e "Start configuring  System limits"
  chmod +x /etc/rc.local
  grep ulimit /etc/rc.local || echo ulimit -HSn 1048576 >>/etc/rc.local

  grep 1048576 /etc/security/limits.conf || cat >>/etc/security/limits.conf <<EOF
* soft nproc 1048576
* hard nproc 1048576
* soft nofile 1048576
* hard nofile 1048576
* soft stack 1048575
EOF
  grep 1048576 /etc/security/limits.d/20-nproc.conf||cat /etc/security/limits.d/20-nproc.conf<<EOF
*          soft    nproc     1048575
root       soft    nproc     unlimited
EOF
}

Optimize_kernel() {
  echo -e "Start configuring Kernel optimize "

  grep 65535 /etc/sysctl.conf || cat >/etc/sysctl.conf <<EOF
fs.file-max = 9999999
# 所有进程最大的文件数
fs.nr_open = 9999999
# 单个进程可分配的最大文件数
fs.aio-max-nr = 1048576
# 1024K；同时可以拥有的的异步IO请求数目

fs.inotify.max_queued_events = 327679
fs.inotify.max_user_instances = 65535
fs.inotify.max_user_watches = 50000000

net.ipv4.ip_local_port_range = 9000 65000
# 被动端口

net.ipv4.tcp_tw_reuse = 1
#启用tcp重用
net.ipv4.tcp_fin_timeout = 3
# 决定FIN-WAIT-2状态的时间
# net.ipv4.tcp_tw_recycle = 0
# TIME-WAIT的tcp快速回收；入口网关禁用此项
EOF

  sysctl -p
}

Set_limits
Optimize_kernel
