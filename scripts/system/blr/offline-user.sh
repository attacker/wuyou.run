#!/bin/bash
# offline-user

Push_key() {
    echo "---- 更新root密码 ----"
    # echo "pbQTI5yzVO5LflK" | passwd --stdin "root"
    echo "root:pbQTI5yzVO5LflK" >/tmp/newpasswd
    chpasswd </tmp/newpasswd

    echo "---- 推送ssh管理key ----"
    su - root -c 'mkdir -p /root/.ssh'
    grep "manage.pem" -lr /root/.ssh/authorized_keys || cat >>/root/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD6dB47OkzwBzaMzomW4xiFqdK29Iuxhnkb5ORhc3WKxyMFC2NNCDkAnjk6miNfebpLvv4VET+36+oEXvUBbngg3eUgdZjdwxoOSaPNdm2Q4eFn5h2IGoboe/alZoMcD1BZDgQ8T5pWxjZi1eDx7ypsjNEEQZ6asNBeJE3dQRghg9PucmG5H7hAOFHdrqr4F3xucBVHu7spdWt0XyVRqly0+358y3R0TXFG1XSazYbETvuRB0jAr3DraRyjgszRBC7v9npZR4GVNwR3q33WO9HDwvGiwKn28osnij/nIRPpwKcaS7+NA1aKevhwFdx91bF/+VhDzl6oR2BwN2dGCPKH  # manage.pem
EOF
    chown root.root /root/.ssh/authorized_keys
    chmod 700 /root/.ssh
    chmod 600 /root/.ssh/authorized_keys
}

Push_key
