#!/bin/bash
###
# @Author: 以谁为师
# @Website: attacker.club
# @Date: 2020-08-26 11:42:48
# @LastEditTime: 2020-08-26 11:43:23
# @Description:
###

####---- 清空iptables策略 ----####
iptables -F
iptables -X
iptables -Z
iptables -F -t nat
iptables -X -t nat
iptables -Z -t nat

####---- INPUT链设置 ----####
iptables -P INPUT ACCEPT
#开放INPUT链
iptables-save
service iptables save

iptables -L -n
# -n  不做解析，dns异常时此命令更快返回内容
