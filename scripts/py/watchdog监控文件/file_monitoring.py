import sys
import time
import shlex
from custom_log import log_start
logger = log_start('watchdog')

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

def bash(cmd):
    """
    执行bash命令
    """
    logger.info(cmd)
    return shlex.os.system(cmd)


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        info = "文件被修改了 %s"%event.src_path
        logger.info(info)
        bash("ip add")

    # def on_created(self, event):
    #     info = "文件被创建了 %s" % event.src_path
    #     logger.info(info)

 
if __name__ == "__main__":

    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()