#!/usr/bin/env python
# coding=utf-8
'''
Author: 以谁为师
Website: attacker.club
Date: 2020-10-18 22:21:54
LastEditTime: 2020-10-18 23:28:07
Description: 
'''
from pyzabbix import ZabbixAPI
import getpass
import sys


def loginZabbix(ZABBIX_SERVER, USER, PASSWD):
    '''登陆zabbix，返回登陆对象'''
    user = ZabbixAPI(ZABBIX_SERVER)
    user.login(USER, PASSWD)
    return user


def hostAllInfo(user, host_name):
    '''根据主机名，返回该主机的所有信息'''
    host = user.host.get(search={"name": host_name}, output="extend")
    if len(host) != 0:
        return host
    else:
        return None


def hostIdInfo(user, host_name):
    '''根据主机名，返回该主机id'''
    host_id = user.host.get(search={"name": host_name}, output=["hostid"])
    if len(host_id) != 0:
        return host_id[0]["hostid"]
    else:
        return None


def hostItemAllInfo(user, host_name, item):
    '''根据主机名和监控key，返回该监控项的数据'''
    item = user.item.get(host=host_name, search={
                         "key_": item}, output="extend")
    if len(item) != 0:
        return item
    else:
        return None


def hostItemIdInfo(user, host_name, item):
    '''根据主机名和监控key，返回该监控项的数据'''
    item_id = user.item.get(host=host_name, search={
                            "key_": item}, output=["itemid"])
    if len(item_id) != 0:
        return item_id[0]["itemid"]
    else:
        return None


if __name__ == "__main__":
    url = "http://54.241.xxx.xxx/zabbix/"
    passwd = getpass.getpass()

    # zapi  = loginZabbix(url, "admin", passwd)
    zapi = ZabbixAPI(url)
    zapi.login("admin", passwd)
    res = hostAllInfo(zapi, "185.193")
    print(res)

    for h in zapi.host.get(output="extend"):
        """
        打印所有hostid
        """
        print(h['hostid'])
