#!/usr/bin/env python
# coding=utf-8
'''
Author: 以谁为师
Website: attacker.club
Date: 2020-08-10 00:09:10
LastEditTime: 2020-08-10 00:23:35
Description: 

'''


import requests


class Webapi():
    def __init__(self):

        self.url = "http://127.0.0.1:8000/users"

    def get_url(self):
        res = requests.get(self.url)
        return res.json()


G = Webapi()
print(G.get_url())
