#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import subprocess
"""
Author:  LJ
Email:   admin@attacker.club
Time:    2020/2/28 15:03
Description:lazy tools
ln -s xx tools /usr/local/bin/
"""

version = '1.0.0'
root_path = os.path.dirname(__file__)
file_conf = sys.argv[0].split('.')[0]+".conf"


def parse_args():

    parser = argparse.ArgumentParser(
        usage="lazy tools",
        description="懒人命令集: 整合常用命令行语句 ^_^ ",
        add_help=False,
        formatter_class=lambda prog: argparse.RawTextHelpFormatter(
            prog, max_help_position=50)
    )
    parser.add_argument("--help",
                        action="help",
                        help="查看帮助信息")

    parser.add_argument("-L", "--log",
                        default="info",
                        help="设置LOG级别")

    parser.add_argument('-t', '--type',
                        # required=True,  # (必备参数)
                        help="类型, git自动提交；")

    parser.add_argument('-p', '--port',
                        type=int,
                        default=8000,
                        help='服务器端口，默认为8000')

    parser.add_argument('-h', '--host',
                        default='0.0.0.0',
                        help='服务器IP，默认为0.0.0.0')

    parser.add_argument('-s', '--source', nargs='*', help="源地址目录,可输入多个")
    parser.add_argument('-c', '--config',
                        help="读取脚本同名的配置文件: " + file_conf,
                        default=file_conf)
    parser.add_argument('-o', '--output',
                        help='输出文件名称')

    args = parser.parse_args()
    # print("打印所有变量",vars(args))
    return args


def run_shell(cmd):
    """
    执行系统命令
    """
    (status, output) = subprocess.getstatusoutput(cmd)
    context = {
        'status': status,
        'output': output,
    }
    return context


def git():
    ret = run_shell(
        "git pull && git add -A && git commit -m 'Automatically submit'  && git push")
    return ret


def pyenv():
    ret = run_shell(
        "$(which  python3) -m venv env")
    return ret


if __name__ == '__main__':

    if len(sys.argv) == 1:
        """ 如果没有参数则查看帮助信息 """
        sys.argv.append("--help")

    args = parse_args()
    # print("打印变量\n端口:", args.port, '\n配置文件:', args.config)

    if args.type in dir():
        execute_command = eval(args.type)
        print(execute_command())
