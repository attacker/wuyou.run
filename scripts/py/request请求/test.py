#!/usr/bin/env python
# coding=utf-8
'''
Author: 以谁为师
Website: attacker.club
Date: 2020-09-30 11:10:43
LastEditTime: 2020-10-16 18:39:43
Description:
'''
from bs4 import BeautifulSoup

import requests
import re


class Webapi():
    def __init__(self, host):
        self.url = "http://%s:25000/queries" % (host)
        self.host = host

    def get_data(self):
        try:
            res = requests.get(self.url, timeout=1)
            html = res.text
            soup = BeautifulSoup(html, 'html.parser')
            queries_str = soup.findAll(string=re.compile("queries in flight"))
            self.queries = re.findall(r'\d+', queries_str[0])[0]

            wait_str = soup.find_all(
                string=re.compile("waiting to be closed"))
            self.wait = re.findall(r'\d+', wait_str[0])[0]
        except Exception as e:
            exit


host_list = [
    "10.88.0.114",
    "10.88.0.200",
    "10.88.0.201",
    "10.88.0.61",
    "10.88.0.62",
    "10.88.0.112",
    "10.88.0.113",
    "10.88.0.114"
]


for host in host_list:
    G = Webapi(host)
    try:
        G.get_data()
        # print(host, "queries in flight", G.queries)
        # print(host, "waiting to be closed", G.wait)

    except AttributeError as e:
        pass
