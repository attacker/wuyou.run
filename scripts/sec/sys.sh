#!/bin/bash


iptables -D INPUT -s  192.168.66.41 -j ACCEPT
iptables -D INPUT -s  192.168.66.42 -j ACCEPT

iptables -I INPUT -s  192.168.66.41 -j ACCEPT
iptables -I INPUT -s  192.168.66.42 -j ACCEPT
#iptables -I INPUT -s   172.16.140.118 -j DROP


iptables -D INPUT -p tcp --dport 6022 -j DROP
iptables -A INPUT -p tcp --dport 6022 -j DROP

iptables -nL