# curl -s http://wuyou.run/scripts/sec/openssh-update.sh|bash
yum install -y openssl-devel gcc   # 依赖


wget -c https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-8.8p1.tar.gz
tar zxvf openssh-8.8p1.tar.gz && cd openssh-8.8p1
./configure --prefix=/usr/local/openssh
make -j2 && make install

# 老文件移动
mv /usr/libexec/openssh/sftp-server /usr/local/src
mv /usr/bin/ssh-* /usr/local/src
mv /usr/bin/{scp,sftp,ssh} /usr/local/src
mv /usr/sbin/sshd /usr/local/src
systemctl stop sshd

# 复制命令文件
\cp /usr/local/openssh/sbin/sshd /usr/sbin/
\cp /usr/local/openssh/bin/{ssh,ssh-keygen} /usr/bin/
cp contrib/ssh-copy-id /usr/bin/ssh-copy-id
chmod 775  /usr/bin/ssh*

cp contrib/redhat/sshd.init /etc/init.d/sshd # 启动文件
cat >/usr/lib/systemd/system/sshd.service <<EOF
[Unit]
Description=OpenSSH server daemon
Documentation=man:sshd(8) man:sshd_config(5)
After=network.target
[Service]
ExecStart=/usr/sbin/sshd
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload

sed -i '32aPermitRootLogin yes' /usr/local/openssh/etc/sshd_config
mv /etc/ssh/sshd_config /etc/ssh/sshd_config_bak
ln -s  /usr/local/openssh/etc/sshd_config  /etc/ssh/
systemctl restart sshd