#!/bin/sh
###
 # @Author: admin@attacker.club
 # @Date: 2022-11-12 10:27:15
 # @LastEditTime: 2022-11-12 10:27:20
 # @Description: 
### 
echo "======== start clean docker containers logs ========"
logs=$(find /var/lib/docker/containers/ -name *-json.log)
for log in $logs
do
echo "clean logs : $log"
cat /dev/null > $log
done
echo "======== end clean docker containers logs ========"