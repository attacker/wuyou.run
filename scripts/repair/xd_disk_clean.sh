#!/bin/bash
#  rm /scripts/xd_disk_clean.sh -f; mkdir /scripts ; wget -P /scripts http://wuyou.run/scripts/repair/xd_disk_clean.sh ;chmod 750 /scripts/xd_disk_clean.sh 
# */5 * * * * /scripts/xd_disk_clean.sh

# 清理磁盘空间
function clean() {
    year=`date '+%Y'`
    
    if [ $year > 1 ]; then
      echo "ok"
    else
      echo "error"
      year=2
    fi
    echo $year
    
    echo "开始清理磁盘空间"

    journalctl --vacuum-size=50M

    find /var/log/ -name "*.log*" -mtime +3 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    find /var/log/ -name "*$year*"  -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    find /tmp -name "*$year*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    
    if  [ -d /home/xidian/ ];then
      find /home/xidian/  -name  "*201*.log*" -mtime +3 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
      find /home/xidian/  -name  "*202*.log*" -mtime +3 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
      find /home/xidian/  -name  "*log.*202*" -mtime +3 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi

    if  [ -d /home/xidian/.pm2/logs ];then
      find /home/xidian/.pm2/logs  -name  "*log-20*" -mtime +3 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
      echo > /home/xidian/.pm2/logs/checkin-out.log
      echo > /home/xidian/.pm2/logs/netRoomPms-out.log
    fi

    

    if [ -f /var/spool/mail/root ];then
      echo >  /var/spool/mail/root
    fi


    



    

}



# 判断磁盘使用率
num=$(df -h |awk -F"[ %]+" '/[0-9]/  { if ($5>=90) print $5,$6}'|wc -l)
if [ $num != 0 ];then
        clean
else
        echo "磁盘空闲"
fi


