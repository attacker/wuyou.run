#!/bin/bash
###
# @Author: admin@attacker.club
# @Date: 2023-03-16 16:57:09
# @LastEditTime: 2023-10-27 16:35:54
# @Description: Pro 清理
###
# rm /opt/bl-disk_clean.sh -f ; wget -P /opt http://wuyou.run/scripts/repair/bl-disk_clean.sh ;chmod 750 /opt/bl-disk_clean.sh
# */15 * * * *  /opt/bl-disk_clean.sh

## 清理磁盘空间
function clean() {
    year=$(date '+%Y')

    if [ $year ] >1; then
        echo "ok"
    else
        echo "error"
        year=2
    fi
    echo $year

    echo "开始清理磁盘空间"
    journalctl --vacuum-size=50M

    if [ -d /data ]; then
        find /data -name "*20*.log" -mtime +3 -type f -print -exec rm -f {} \; >>/tmp/delete.log 2>&1
    fi

    if [ -f /var/spool/mail/root ]; then
        echo >/var/spool/mail/root
    fi
    if [ -f /var/log/messages ]; then
        echo >/var/log/messages
        rm /var/log/*20* -f
    fi

    if [ -d /var/lib/docker/containers ]; then
        # 清空 Docker 日志文件而不删除
        find /var/lib/docker/containers -name "*-json.log" -type f -exec sh -c 'echo -n > "{}"' \;
    fi
}

## 检查磁盘使用率是否超过 75%
num=$(df -h | awk -F"[ %]+" '/[0-9]/  { if ($5>=75) print $5,$6}' | wc -l)
if [ $num != 0 ]; then
    clean
else
    echo "磁盘空闲"
fi
