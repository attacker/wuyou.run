#!/bin/bash
#  rm /scripts/tz_disk_clean.sh -f; mkdir /scripts ; wget -P /scripts http://wuyou.run/scripts/repair/tz_disk_clean.sh ;chmod 750 /scripts/tz_disk_clean.sh 
# */5 * * * * /scripts/tz_disk_clean.sh

# 清理磁盘空间
function clean() {
    year=`date '+%Y'`
    
    if [ $year > 1 ]; then
      echo "ok"
    else
      echo "error"
      year=2
    fi
    echo $year
    
    echo "开始清理磁盘空间"
    find /var/log/ -name "*.log*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    find /var/log/ -name "*$year*"  -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    find /tmp -name "*$year*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    
    if  [ -d /mnt/sdf/sdg_log ];then
      find /mnt/sdf/sdg_log  -name  "*.log*" -mtime +1 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi
    if [ -d /usr/share/tomcat/logs ];then
      find /usr/share/tomcat/logs  -name  "*$year*" -mtime +1 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi
     if [ -d /mnt/sdf/boss/log ];then
      find /mnt/sdf/boss/log  -name  "*$year*" -mtime +1 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi

    if [ -d /mnt/sdf/dingtone/log ];then
      find /mnt/sdf/dingtone/log  -name  "*.log*" -mtime +1 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi 

    if [ -d /mnt/sdg/dingtone/log ];then
      find /mnt/sdg/dingtone/log -name  "*.log*" -mtime +1 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi

    if [ -d /mnt/tomcat/logs ];then
      find /mnt/tomcat/logs -name  "*$year*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi

    if [ -d usr/share/tomcat6/logs ];then
      find /usr/share/tomcat6/logs -name  "*$year*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi

    if [ -d /mnt/vpn/logs ];then
      find /mnt/vpn/logs -name  "*$year*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
      find /mnt/vpn/logs -name  "*log*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi
     if [ -d /mnt/logs ];then
      find /mnt/logs -name  "*$year*" -mtime +0 -type f -print -exec rm -f {} \;  >>/tmp/delete.log 2>&1
    fi
    


    if [ -f /var/spool/mail/root ];then
      echo >  /var/spool/mail/root
    fi


    



    

}



# 判断磁盘使用率
num=$(df -h |awk -F"[ %]+" '/[0-9]/  { if ($5>=90) print $5,$6}'|wc -l)
if [ $num != 0 ];then
        clean
else
        echo "磁盘空闲"
fi


