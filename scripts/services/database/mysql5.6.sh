
#!/bin/bash
# --------------------------------------------------
#Author:  LJ
#Email:   admin@attacker.club

#Last Modified: 2020-08-23 23:52:38
#Description:  bash <(curl -sL http://wuyou.run/scripts/sh/services/mysql5.6.sh)
# --------------------------------------------------


function color_message()
{ 
  case "$1" in
      "warn")
      echo -e "\e[1;31m$2\e[0m"
      ;;
      "info")
      echo -e "\e[1;33m$2\e[0m"
      ;;
  esac
}

function confirm()
{
  read -p 'Are you sure to Continue?[Y/n]:' answer
  case $answer in
  Y | y)
        echo -e "\n\t\t\e[44;37m Running the script \e[0m\n";;
  N | n)
        echo -e "\n\t\t\033[41;36mExit the script \e[0m\n"  && exit 0;;
  *)
        echo -e "\n\t\t\033[41;36mError choice \e[0m\n"  && exit 1;;
  esac
}



confirm



mysqldir=/usr/local/mysql
# 安装目录
datadir=/home/data/mysql
# 数据目录

dbrootpwd=$(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 9) 
#　随机数据库root密码


/etc/init.d/mysqld stop
pkill mysqld
rm $mysqldir  /data/mysql/  /etc/my.cnf  /etc/init.d/mysqld -rf
#关闭已有mysql，并删除mysql文件

yum install -y  mysql-devel perl  autoconf automake imake libxml2-devel\
 expat-devel cmake gcc gcc-c++ libaio libaio-devel bzr bison libtool ncurses5-devel
#更新依赖包
 
if [ ! -f mysql*glibc*tar.gz  ];then
    
    wget -c http://mirrors.ustc.edu.cn/mysql-ftp/Downloads/MySQL-5.6/mysql-5.6.47-linux-glibc2.12-x86_64.tar.gz
fi

tar  zxvf mysql-5*gz -C /usr/local
mv /usr/local/mysql-5* $mysqldir

groupadd -g 501 mysql || groupadd  mysql
useradd  -u 501  -M -s /sbin/nologin -g mysql -d $mysqldir mysql || useradd   -M -s /sbin/nologin -g mysql -d $mysqldir mysql

useradd mysql -g mysql -M -s /sbin/nologin
mkdir -p $datadir
chown mysql:mysql -R $datadir
chown mysql:mysql -R $mysqldir

cp $mysqldir/support-files/mysql.server /etc/init.d/mysqld -rf
#sed -i "s#BASEDIR=#BASEDIR=$mysqldir#g" /etc/init.d/mysqld
#sed -i "s#datadir=#datadir=$datadir#g" /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld
# my.cf
cat > /etc/my.cnf << EOF

[client]
port = 3306
socket = /tmp/mysql.sock

[mysqld]
port = 3306
socket = /tmp/mysql.sock

basedir = $mysqldir
pid-file = $datadir/mysql.pid
user = mysql
bind-address = 0.0.0.0
server-id = 1

# DATA STORAGE #
datadir = $datadir

# LOG
log_error = $datadir/error.log
slow-query-log-file = $datadir/slow.log
slow_query_log = 1


# name-resolve
skip-name-resolve
skip-host-cache

# 基础设置
thread_cache_size=64 
# 服务器线程缓存；根据内存1G>8、2G>16
open_files_limit=65535
# 最大文件连接数
max_connections=80000
# 默认1000
wait_timeout=1800 
# 非交互式连接默认8小时，改为30分钟;控制连接最大空闲时长（mysql api程序,jdbc连接数据库等）
interactive_timeout=2880000 
# 交互式连接:mysql工具、mysqldump等
max_allowed_packet=128M 
# 限制server接受的数据包大小


# default-storage-engine= InnoDB 
# # 默认存储引擎
innodb_buffer_pool_size=10G 
# InnoDB使用该参数指定大小的内存来缓冲数据和索引
innodb_log_buffer_size=16M 
# 事务在内存中的缓冲，也就是日志缓冲区的大小， 默认设置即可，具有大量事务的可以考虑设置为16M

skip-name-resolve 
# 消除DNS解析时间，只能IP地址方式连接

# MySQL读入缓冲区的大小
read_buffer_size=16M # 对表的顺序扫描请求非常频繁修改该值
read_rnd_buffer_size=16M # MySQL的随机读缓冲区大小、MySQL的顺序读缓冲区大小
sort_buffer_size=16M # 连接超时时间、保持时间、最大传输数据包大小


EOF

Memtatol=`free -m | grep 'Mem:' | awk '{print $2}'`
if [ $Memtatol -gt 1500 -a $Memtatol -le 2500 ];then
        sed -i 's@^thread_cache_size.*@thread_cache_size = 16@' /etc/my.cnf
        sed -i 's@^query_cache_size.*@query_cache_size = 16M@' /etc/my.cnf
        sed -i 's@^key_buffer_size.*@key_buffer_size = 16M@' /etc/my.cnf
        sed -i 's@^innodb_buffer_pool_size.*@innodb_buffer_pool_size = 128M@' /etc/my.cnf
        sed -i 's@^tmp_table_size.*@tmp_table_size = 32M@' /etc/my.cnf
        sed -i 's@^table_open_cache.*@table_open_cache = 256@' /etc/my.cnf
elif [ $Memtatol -gt 2500 -a $Memtatol -le 3500 ];then
        sed -i 's@^thread_cache_size.*@thread_cache_size = 32@' /etc/my.cnf
        sed -i 's@^query_cache_size.*@query_cache_size = 32M@' /etc/my.cnf
        sed -i 's@^key_buffer_size.*@key_buffer_size = 64M@' /etc/my.cnf
        sed -i 's@^innodb_buffer_pool_size.*@innodb_buffer_pool_size = 512M@' /etc/my.cnf
        sed -i 's@^tmp_table_size.*@tmp_table_size = 64M@' /etc/my.cnf
        sed -i 's@^table_open_cache.*@table_open_cache = 512@' /etc/my.cnf
elif [ $Memtatol -gt 3500 ];then
        sed -i 's@^thread_cache_size.*@thread_cache_size = 64@' /etc/my.cnf
        sed -i 's@^query_cache_size.*@query_cache_size = 64M@' /etc/my.cnf
        sed -i 's@^key_buffer_size.*@key_buffer_size = 256M@' /etc/my.cnf
        sed -i 's@^innodb_buffer_pool_size.*@innodb_buffer_pool_size = 1024M@' /etc/my.cnf
        sed -i 's@^tmp_table_size.*@tmp_table_size = 128M@' /etc/my.cnf
        sed -i 's@^table_open_cache.*@table_open_cache = 1024@' /etc/my.cnf
fi
$mysqldir/scripts/mysql_install_db --defaults-file=/etc/my.cnf  --basedir=$mysqldir
export PATH=$mysqldir/bin:$PATH
[ -z "`cat /etc/profile | grep $mysqldir`" ] && echo "export PATH=$mysqldir/bin:\$PATH" >> /etc/profile 
.  /etc/profile


/etc/init.d/mysqld restart
color_message  "info"  "启动mysql"

$mysqldir/bin/mysql -e "grant all privileges on *.* to root@'%' identified by \"$dbrootpwd\" with grant option;" 2>/dev/null
#$mysqldir/bin/mysql -e "grant all privileges on *.* to root@'localhost' identified by \"$dbrootpwd\" with grant option;" 2>/dev/null
#添加localhost和远程root账号密码

$mysqldir/bin/mysql -uroot -p$dbrootpwd -e "delete from mysql.user where Password='';" 2>/dev/null
$mysqldir/bin/mysql -uroot -p$dbrootpwd -e "delete from mysql.db where User='';" 2>/dev/null
$mysqldir/bin/mysql -uroot -p$dbrootpwd -e "delete from mysql.proxies_priv where Host!='localhost';" 2>/dev/null
$mysqldir/bin/mysql -uroot -p$dbrootpwd -e "drop database test;" 2>/dev/null
#删除空密码账号

ln -s $mysqldir/lib/libmysqlclient.so.18 /usr/lib64/libmysqlclient.so.18

echo 
echo   -e "\t\t\t\tMysql密码:\033[3;032m\"$dbrootpwd\"  \033[0m\n"
echo  
echo  "mysql账号:root/$dbrootpwd" >passwd.txt 
