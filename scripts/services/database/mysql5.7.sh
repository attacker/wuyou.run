#!/bin/bash
###
# @Author: 以谁为师
# @Website: attacker.club
# @Date: 2020-09-07 23:41:53
# @LastEditTime: 2020-09-08 03:03:29
# @Description:
###

# rpm -qa | grep mariadb
# rpm -e ... --nodeps
# wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
# rpm -ivh mysql-community-release-el7-5.noarch.rpm
# yum install -y mysql-server

wget https://cdn.mysql.com/archives/mysql-5.7/mysql-5.7.30-linux-glibc2.12-x86_64.tar.gz

tar zxvf mysql-5.7.30-linux-glibc2.12-x86_64.tar.gz
mv mysql-5.7.30-linux-glibc2.12-x86_64 /usr/local/mysql5.7

cat >/usr/local/mysql5.7/my.cnf <<EOF
[mysqld]
server-id = 1
port = 3307

socket=/tmp/mysql5.7.sock
character-set-server=utf8
basedir = /usr/local/mysql5.7

datadir= /home/mysql/mysql5.7_data
log_error = error.log
slow_query_log = 1
slow_query_log_file = slow.log

character-set-server = utf8mb4
#数据库字符集对应一些排序等规则，注意要和character-set-server对应
collation-server = utf8mb4_general_ci
#设置client连接mysql时的字符集,防止乱码
init_connect='SET NAMES utf8mb4'
explicit_defaults_for_timestamp = true


interactive_timeout = 1800
wait_timeout = 1800

skip-grant-tables
# 无密码登录

max_allowed_packet = 128M

log-bin=mysql-bin
expire_logs_days = 3
binlog_format =  mixed

#一般设置物理存储的60% ~ 70%
innodb_buffer_pool_size = 2G
#5.7.6之后默认16M
#innodb_log_buffer_size = 16777216

#CPU多核处理能力设置，假设CPU是2颗8核的，读多，写少可以设成2:6的比例
innodb_write_io_threads = 8
innodb_read_io_threads = 8




[mysqldump]
quick
max_allowed_packet = 128M
[mysqld_safe]
#增加每个进程的可打开文件数量
open-files-limit = 28192
EOF

# 安装
/usr/local/mysql5.7/bin/mysqld --defaults-file=/usr/local/mysql5.7/my.cnf \
  --initialize-insecure \
  --user=mysql \
  --basedir=/usr/local/mysql5.7 \
  --datadir=/home/mysql/mysql5.7_data

# my.cnf: skip-grant-tables=1
# 登录:mysql -P3307 -uroot  -h127.0.0.1 -p
# update mysql.user set authentication_string=password('123456') where user='root' and Host = 'localhost';
# 重启登录: mysql -P3307 -uroot  -h127.0.0.1 -p123456
# 远程账号:

# 启动
/usr/local/mysql5.7/bin/mysqld \
  --defaults-file=/usr/local/mysql5.7/my.cnf \
  --user=mysql
# /usr/local/mysql5.7/bin/mysqld_safe --defaults-file=/usr/local/mysql5.7/my.cnf 2>&1 > /dev/null &
# 启动多实例参考

cat >/etc/systemd/system/mysqld5.7.service <<EOF
[Unit]
Description=MySQL Server
Documentation=man:mysqld(8)
Documentation=http://dev.mysql.com/doc/refman/en/using-systemd.html
After=network.target
After=syslog.target

[Install]
WantedBy=multi-user.target

[Service]
User=mysql
Group=mysql
ExecStart=/usr/local/mysql5.7/bin/mysqld --defaults-file=/usr/local/mysql5.7/my.cnf
LimitNOFILE = 5000
EOF

systemctl daemon-reload

systemctl start mysqld5.7
