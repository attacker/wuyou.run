# pure-ftpd

# install
yum remove pure-ftpd -y
yum install pure-ftpd -y

# user
groupdel ftpgroup
groupadd ftpgroup
userdel ftpuser
useradd ftpuser

mkdir /repo
# useradd -g ftpgroup -M -s /sbin/nologin -d /repo ftpuser
chown ftpuser.ftpgroup /repo -R

cat >>/etc/pure-ftpd/pure-ftpd.conf <<EOF
ChrootEveryone  yes           
# 锁定所有用户到家目录中 
DontResolve  no
# 禁止反向解析
NoAnonymous  yes
# 不允许匿名用户
MaxClientsNumber            100 
# 最大的客户端数量
MaxClientsPerIP             20 
# 同一个IP允许8个链接
LimitRecursion              2000 10      
# 别表最大显示2000个文件，最深8个目录
PureDB    /etc/pure-ftpd/pureftpd.pdb
# 虚拟用户数据库，CentOS下默认位置
# Bind      0.0.0.0,6021
# 绑定IP和端口
EOF

systemctl restart pure-ftpd

pure-pw list
# 查看用户
# pure-pw passwd
# 虚拟用户修改密码
# pure-pw userdel   pure
# 删除虚拟用户
pure-pw useradd ftpcenter -u ftpuser -d /repo -m
# u虚拟用户 d目录 m则写入用户数据库
