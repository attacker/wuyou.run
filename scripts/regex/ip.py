import re



def found_ip(content):
    """ 提取包含的ip,生成列表 """
    iplist = re.findall(
        r"\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}", content)
    if iplist:
        return iplist


if __name__ == '__main__':
    content = """317969;"41.1.2";"92.38.163.85";"CentOS-7-amd64";"KVM-SSD-2";"Luxembourg";"Daily charges";"Active";"4.95 EUR / Month";
630427;"42.1.1";"5.188.168.30";"CentOS-7-amd64";"KVM-SSD-2-TII";"Turkey
630434;"42.1.14";"5.188.168.48";"CentOS-7-amd64";"KVM-SSD-2-TII";"Turkey
634476;"42.1.15";"5.188.168.98";"CentOS-7-amd64";"KVM-SSD-2-TII";"Turkey
634483;"42.1.16";"5.188.168.102";"CentOS-7-amd64";"KVM-SSD-2-TII";"Turkey
636407;"42.2.5";"92.38.190.235";"CentOS-7-amd64";"KVM-SSD-1-RC-OLD";"Russia
659906;"43.1.11";"146.185.219.241";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
659913;"43.1.12";"146.185.219.242";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
659920;"43.1.13";"146.185.219.243";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
659933;"43.1.14";"146.185.219.244";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
659946;"43.1.15";"146.185.219.245";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
314208;"43.1.7";"146.185.219.182";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
314215;"43.1.8";"146.185.219.183";"CentOS-7-amd64";"KVM-SSD-2-BZI";"Israel
317982;"44.1.2";"5.188.108.22";"CentOS-7-amd64";"KVM-SSD-2-PL1";"Poland
178429;"46.1.2";"92.38.171.123";"CentOS-7-amd64";"KVM-SSD-2-CM";"Spain
774508;"in-streaming-14.3.1";"5.188.228.196";"CentOS-7-amd64";"KVM-SSD-1-WW";"India
761540;"jp-streaming-5.3.1";"95.85.91.42";"CentOS-7-amd64";"KVM-SSD-2-CC1";"Japan
""" 

    # pattern = re.compile(r"\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}")
    # matchObj = pattern.match(content)
    # print(matchObj)

    hostip = found_ip(content)
    print(hostip)