#!/bin/bash

skywalking-host=$1

wget -c http://software.opsbase.cn/skywalking/skywalking-agent-es7.tar.gz
mkdir /opt/skywalking
tar zxvf skywalking-agent-es7.tar.gz -C /opt/skywalking

sed -i 's/192.168.66.210/${skywalking-host}/' /opt/skywalking/agent/config/agent.config