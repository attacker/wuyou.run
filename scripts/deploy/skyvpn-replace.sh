#!/bin/bash

# curl -s  http://wuyou.run/scripts/deploy/skyvpn-replace.sh |bash -s global
# curl -s  http://wuyou.run/scripts/deploy/skyvpn-replace.sh |bash -s site





# 脚本开始时间
starttime=$(date +'%Y-%m-%d %H:%M:%S')

# 判断变量值不为空
if [ -z "$1" ]; then
    echo "please input parameter \$1"
    exit 1
else
    args=$1
    echo "current parameter ${args}"
fi

## global_config ##
#cp /mnt/sdf/dingtone/global.properties /mnt/sdf/dingtone/global.properties_backup
#cp /mnt/sdf/dingtone/pstn.properties /mnt/sdf/dingtone/pstn.properties_backup

## site
#cp  /mnt/sdf/dingtone/billing.properties /mnt/sdf/dingtone/billing.properties_backup
#cp  /mnt/sdf/dingtone/config.properties  /mnt/sdf/dingtone/config.properties_backup


function replace_global(){
## BAKCUP

sed -i  "/^aws.redis.host/c aws.redis.host=pro-skyvpn-redis6.omyi1r.ng.0001.usw1.cache.amazonaws.com" \
/mnt/sdf/dingtone/*.properties



#### smssver
sed -i  "/^email.url=/c email.url=http://smssvr.skyvpnapi.com:8080/smssvr/sendEmailCommon" \
 /mnt/sdf/dingtone/global.properties

echo "/mnt/sdf/dingtone/global.properties:"
grep ^email.url  /mnt/sdf/dingtone/global.properties

sed -i  "/^email.url=/c email.url=http://smssvr.skyvpnapi.com:8080/smssvr/sendEmailCommon" \
 /mnt/sdf/dingtone/pstn.properties

echo "/mnt/sdf/dingtone/pstn.properties:"
grep  ^email.url /mnt/sdf/dingtone/pstn.properties



#### global
sed -i "/^global.url/c global.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/global.properties /mnt/sdf/dingtone/pstn.properties

sed -i "/^global.dingtone.url=/c  global.dingtone.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*.properties
sed -i "/^global.coverMe.url=/c  global.coverMe.url=http://coverme-global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*.properties


#### push
sed -i "/^push.url/c push.url=http://dt-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es

sed -i "/^push.dingtone.url/c push.dingtone.url=http://dt-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*.properties 

sed -i "/^push.coverMe/c push.coverMe.url=http://coverme-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*.properties 

sed -i "/^PushUrl=/c   PushUrl=http://dt-push.skyvpnapi.com:8080/pushsvr" \
/mnt/sdf/dingtone/*.properties



### apiGatewayUrl
sed -i "/^apiGatewayUrl/c  apiGatewayUrl=http://gateway2svr.skyvpnapi.com:9230" \
/mnt/sdf/dingtone/configglobal.properties  /mnt/sdf/dingtone/global.properties

### monitorUrl
sed -i "/^monitorUrl/c monitorUrl=http://monitor.skyvpnapi.com:8080/alertCenter/v1/event/report" \
/mnt/sdf/dingtone/global.properties

	
### Adserver 广告
sed -i "/^ADServerUrl/c   ADServerUrl=http://adserver.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*.properties

sed -i "/^ADServerUrl/c   ADServerDomain=adserver.skyvpnapi.com" \
/mnt/sdf/dingtone/*.properties

### appwall　广告
sed  -i  "/^appwallUrl/c appwallUrl=http://appwallserver.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es

}


function  replace_site(){
## BAKCUP


sed -i  "/^aws.redis.host/c aws.redis.host=pro-skyvpn-redis6.omyi1r.ng.0001.usw1.cache.amazonaws.com" \
/mnt/sdf/dingtone/*.properties

sed -i  "/^BOSS_OFFLINEMSG.aws.redis.host/c BOSS_OFFLINEMSG.aws.redis.host=pro-skyvpn-redis6.omyi1r.ng.0001.usw1.cache.amazonaws.com" \
/mnt/sdf/dingtone/*.properties

sed -i  "/^memcached.url/c memcached.url=dt-websvr-cache-pn1.ehh8cl.cfg.usw1.cache.amazonaws.com" \
/mnt/sdf/dingtone/*.properties




####  smssver
sed -i  "/^email.url/c  email.url=http://smssvr.skyvpnapi.com:8080/smssvr/sendEmailCommon" \
/mnt/sdf/dingtone/billing.properties
echo "/mnt/sdf/dingtone/billing.properties:"
grep  ^email.url /mnt/sdf/dingtone/billing.properties

sed -i  "/^SmsUrl/c SmsUrl=http://smssvr.skyvpnapi.com:8080/smssvr" \
/mnt/sdf/dingtone/config.properties
echo "/mnt/sdf/config.properties:"
grep  ^SmsUrl /mnt/sdf/dingtone/config.properties


####  global
sed -i "/^global.url=/c  global.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/billing.properties
sed -i "/^global.dingtone.url=/c  global.dingtone.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/billing.properties
sed -i "/^global.coverMe.url=/c  global.coverMe.url=http://coverme-global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/billing.properties

sed -i "/^GWebsvrUrl=/c GWebsvrUrl=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/config.properties

#### dingtone site
sed -i "/^WebSrvIp=/c  WebSrvIp=dingtone-site.skyvpnapi.com" \
/mnt/sdf/dingtone/config.properties


### push
sed -i "/^push.coverMe=/c push.coverMe.url=http://coverme-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es
sed -i "/^push.url=/c push.url=http://dt-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es
sed -i "/^push.dingtone.url/c push.dingtone.url=http://dt-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*.properties 

sed -i "/^PushUrl=/c PushUrl=http://dt-push.skyvpnapi.com:8080/pushsvr" \
/mnt/sdf/dingtone/*.properties 


### transfer
sed -i "/^transfer.url=/c transfer.url=https://transfer.skyvpnapi.com" \
/mnt/sdf/dingtone/*es
sed -i "/^TranferUrl=/c TranferUrl=https://transfer.skyvpnapi.com" \
/mnt/sdf/dingtone/*es

sed -i "/^ApplyCallerIdIp/c  ApplyCallerIdIp=global.skyvpnapi.com" \
/mnt/sdf/dingtone/*es

sed -i "/^WebSrvIp=/c WebSrvIp=dingtone-site.skyvpnapi.com" \
/mnt/sdf/dingtone/*es


sed -i "/^dtpayUrl=/c dtpayUrl=http://apigatewayv2.skyvpnapi.com" \
/mnt/sdf/dingtone/*es

sed -i "/^SkyVpnWebServerUrl=/c  SkyVpnWebServerUrl=http://webserver.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es


### appwall　广告
sed  -i  "/^appwallUrl/c appwallUrl=http://appwallserver.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es

sed  -i  "/^push.dingtone.url/c push.dingtone.url=http://dt-push.bitvpnapi.com:8080" \
/mnt/sdf/dingtone/*es

sed  -i  "/^ADServerDomain=/c ADServerDomain=adserver.skyvpnapi.com" \
/mnt/sdf/dingtone/*es

sed  -i  "/^ADServerUrl/c ADServerUrl=http://adserver.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/*es


}



# 根据不同入参执行函数
case $1 in
    "G"|"global")
        replace_global
        ;;
    "S"|"site")
        replace_site
        ;;
    *)
        echo "Use available parameters  \$1: $0 global | site"
        ;;
esac



