#!/bin/bash

# curl -s  http://wuyou.run/scripts/deploy/hostname.sh  |bash  -s DeepWeb-vpnforce-1



HOSTNAME=$1


Set_hostname() {

  if [ -f /etc/hostname ]; then
    echo "$HOSTNAME" >/etc/hostname
  fi
  sed -i "/HOSTNAME/c HOSTNAME=$HOSTNAME" /etc/sysconfig/network || echo "HOSTNAME=$HOSTNAME" >>/etc/sysconfig/network
  hostname $HOSTNAME
  grep $HOSTNAME /etc/hosts || echo "127.0.0.1 $HOSTNAME" >>/etc/hosts
}


Set_profile() {
  grep PS /etc/profile || echo '''PS1="\[\e[37;1m\][\[\e[32;1m\]\u\[\e[37;40m\]@\[\e[34;1m\]\h \[\e[0m\]\t \[\e[35;1m\]\W\[\e[37;1m\]]\[\e[m\]/\\$" ''' >>/etc/profile
  grep HISTTIMEFORMAT /etc/profile || cat >>/etc/profile <<EOF
#export TMOUT=5000
export HISTTIMEFORMAT="%F %T \$(whoami) " 
export HISTSIZE=10000 
EOF
}

Set_hostname
Set_profile