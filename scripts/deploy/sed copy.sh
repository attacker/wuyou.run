#!/bin/bash

# curl -s  http://wuyou.run/scripts/deploy/sed.sh |bash -s global
# curl -s  http://wuyou.run/scripts/deploy/sed.sh |bash -s site






# 脚本开始时间
starttime=$(date +'%Y-%m-%d %H:%M:%S')

# 判断变量值不为空
if [ -z "$1" ]; then
    echo "please input parameter \$1"
    exit 1
else
    args=$1
    echo "current parameter ${args}"
fi

## global_config ##



function replace_global(){
## BAKCUP
cp /mnt/sdf/dingtone/global.properties /mnt/sdf/dingtone/global.properties_backup
cp /mnt/sdf/dingtone/pstn.properties /mnt/sdf/dingtone/pstn.properties_backup

#### smssver
sed -i  "/^email.url=/c email.url=http://smssvr.skyvpnapi.com:8080/smssvr/sendEmailCommon" \
 /mnt/sdf/dingtone/global.properties

echo "/mnt/sdf/dingtone/global.properties:"
grep ^email.url  /mnt/sdf/dingtone/global.properties

sed -i  "/^email.url=/c email.url=http://smssvr.skyvpnapi.com:8080/smssvr/sendEmailCommon" \
 /mnt/sdf/dingtone/pstn.properties

echo "/mnt/sdf/dingtone/pstn.properties:"
grep  ^email.url /mnt/sdf/dingtone/pstn.properties



#### global
sed -i "/^global.url/c global.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/global.properties
sed -i "/^global.url=/c  global.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/pstn.properties

#### dt-push
sed -i "/^push.url/c push.url=http://dt-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/pstn.properties /mnt/sdf/dingtone/global.properties
sed -i "/^push.dingtone.url/c push.dingtone.url=http://dt-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/pstn.properties /mnt/sdf/dingtone/global.properties

#### push.coverMe
sed -i "/^push.coverMe/c push.coverMe.url=http://coverme-push.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/pstn.properties /mnt/sdf/dingtone/global.properties

}


function  replace_site(){
## BAKCUP
cp  /mnt/sdf/dingtone/billing.properties /mnt/sdf/dingtone/billing.properties_backup
cp  /mnt/sdf/dingtone/config.properties  /mnt/sdf/dingtone/config.properties_backup

####  smssver
sed -i  "/^email.url/c  email.url=http://smssvr.skyvpnapi.com:8080/smssvr/sendEmailCommon" \
/mnt/sdf/dingtone/billing.properties
echo "/mnt/sdf/dingtone/billing.properties:"
grep  ^email.url /mnt/sdf/dingtone/billing.properties

sed -i  "/^SmsUrl/c SmsUrl=http://smssvr.skyvpnapi.com:8080/smssvr" \
/mnt/sdf/dingtone/config.properties
echo "/mnt/sdf/config.properties:"
grep  ^SmsUrl /mnt/sdf/dingtone/config.properties


####  global
sed -i "/^global.url/c  global.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/billing.properties
sed -i "/^global.dingtone.url/c  global.dingtone.url=http://global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/billing.properties
sed -i "/^global.coverMe.url/c  global.coverMe.url=http://coverme-global.skyvpnapi.com:8080" \
/mnt/sdf/dingtone/billing.properties

#### dingtone site
sed -i "/^WebSrvIp/c  WebSrvIp=dingtone-site.skyvpnapi.com" \
/mnt/sdf/dingtone/config.properties


### dt-push
dt-push.skyvpnapi.com   
}



# 根据不同入参执行函数
case $1 in
    "G"|"global")
        replace_global
        ;;
    "S"|"site")
        replace_site
        ;;
    *)
        echo "Use available parameters  \$1: $0 global | site"
        ;;
esac



