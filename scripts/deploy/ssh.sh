#!/bin/bash
###
 # @Author: admin@attacker.club
 # @Date: 2022-07-30 18:03:20
 # @LastEditTime: 2023-02-23 15:08:41
 # @Description: 
### 

# curl -s  http://wuyou.run/scripts/deploy/ssh.sh  |bash



Set_ssh() {
  sed -i '/^#Port/c Port 6022' /etc/ssh/sshd_config &>/dev/null # 默认端口修改
  grep '#PermitRootLogin'  /etc/ssh/sshd_config && sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin yes/g" /etc/ssh/sshd_config
  grep '#UseDNS yes' /etc/ssh/sshd_config && sed -i "s/#UseDNS yes/UseDNS no/g" /etc/ssh/sshd_config
  grep '#AuthorizedKeysFile' /etc/ssh/sshd_config && sed -i "s/#AuthorizedKeysFile/AuthorizedKeysFile/" /etc/ssh/sshd_config
  grep 'GSSAPIAuthentication yes' /etc/ssh/sshd_config && sed -i "s/GSSAPIAuthentication yes/GSSAPIAuthentication no/g" /etc/ssh/sshd_config
  service sshd restart 
  # sshd服务重启
}



Set_ssh