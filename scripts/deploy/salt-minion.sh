
#!/bin/bash

# curl -s  http://wuyou.run/scripts/deploy/salt-minion.sh  |bash  -s  127.0.0.1




Server=$1

yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el7.noarch.rpm -y


yum remove salt-minion -y &&rm /etc/salt/pki/minion/ -rf # 清理key
yum install salt-minion -y 
[ -f "/etc/salt/minion" ] && cat > /etc/salt/minion <<EOF
master: ${Server}

id: $HOSTNAME
tcp_keepalive: True
tcp_keepalive_idle: 300
tcp_keepalive_cnt: -1
tcp_keepalive_intvl: -1
EOF

chkconfig salt-minion on &&  service  salt-minion restart
#ystemctl enable salt-minion &&  systemctl restart  salt-minion