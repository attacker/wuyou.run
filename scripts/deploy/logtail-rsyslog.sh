#!/bin/bash


yum install rsyslog -y


(grep "127.0.0.1:9999" -lr /etc/rsyslog.conf  )||cat >> /etc/rsyslog.conf  << EOF
# ### end of the forwarding rule ###
*.* @@127.0.0.1:9999
EOF

sudo service rsyslog restart