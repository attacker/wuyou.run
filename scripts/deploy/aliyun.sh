#!/bin/bash

# 阿里云助手： https://github.com/aliyun/aliyun_assist_client
#  cd ./aliyun_assist_client && go build  && aliyun-service -d

wget "https://aliyun-client-assist-cn-hangzhou.oss-cn-hangzhou-internal.aliyuncs.com/linux/aliyun_assist_latest.rpm"
rpm -ivh --force aliyun_assist_latest.rpm

aliyun-service -v
# 查看版本
systemctl restart aliyun