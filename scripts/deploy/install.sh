#!/bin/bash

INSTALL_HOME=/usr/local/src
PRG="$0"
PRGDIR=`dirname "$PRG"`
WORKDIR=$PRGDIR
DOWNLOAD_URL="http://software.cn.opsbase.cn"
SCRIPS_HOME="/opt"
REDIS_VERSION="4.0-rc3"
ZBX_SCIPTS_CONF="/etc/zabbix/zabbix_agentd.d"

if [ ! -d  ${SCRIPS_HOME} ]; then
    mkdir -p ${SCRIPS_HOME}
fi

# echo color function, smarter
function echo_e() {
    #Error, Failed
    [ $# -ne 1 ] && return 0
    echo -e "\033[31m$1\033[0m"
}
function echo_y() {
    # Success
    [ $# -ne 1 ] && return 0
    echo -e "\033[32m$1\033[0m"
}
function echo_w() {
    # Warning
    [ $# -ne 1 ] && return 0
    echo -e "\033[33m$1\033[0m"
}
function echo_d() {
    # Debug
    [ $# -ne 1 ] && return 0
    echo -e "\033[34m$1\033[0m"
}
# end echo color function, smarter

function rel_sys(){
    if  uname -r | grep -P "amzn2"; then
	    sys_v="amzn2"
	elif uname -r | grep -P "amzn1"; then
        sys_v="zmzn1"
    elif uname -r | grep -P "el6"; then
        sys_v="el6"
    elif uname -r | grep -P "el7"; then
        sys_v="el7"
	else
		echo_e "Don't know the sys version..."
		exit -1
	fi
}

function install_topenv(){
	yum install -y epel-release	
	yum install -y gcc-c++ ncurses-devel libuuid-devel msgpack openssl-devel libzstd-devel snappy-devel bzip2-devel libatomic libatomic_ops-devel  devtoolset-7-libatomic-devel gdb
	#yum clean all
	#yum makecache
}

function init_sys(){
	#对系统进行初始化
	rel_sys

    if [ $sys_v == "amzn2" ]; then
        amazon-linux-extras install epel
    else
        yum -y install epel-release
    fi

	for PACKAGE in wget net-tools traceroute iftop sysstat unzip ntp vim lrzsz;
	do
		yum -y install $PACKAGE
	done

	cat >>/etc/profile <<EOF
	ulimit -S -c unlimited
	ulimit -HSn 65535
EOF

	curl -s https://bootstrap.pypa.io/get-pip.py | /usr/bin/python || echo_e "pip install bad"

	#pip install ntplib

cat >> /etc/security/limits.conf <<EOF

* 		soft nproc 32768
* 		hard nproc 65536
*       soft  nofile   655350
*       hard  nofile   655350

*       soft  core     524288000

*       soft  sigpending 65535
*       hard  sigpending 65535
EOF

. /etc/profile
	sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
	sed -i 's/SELINUX=permissive/SELINUX=disabled/g' /etc/selinux/config
	setenforce 0
	##config openssh server
	sed -i 's/^#Port 22/Port 1022/g' /etc/ssh/sshd_config
	#sed -i 's/^#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
	###sshd : sshd_config,forbid root login
	#sed -i -e "s/^#StrictModes yes/StrictModes yes/" \
	# -e "s/^#MaxAuthTries 6/MaxAuthTries 6/"   \
	# -e "s/^#GSSAPIAuthentication no/GSSAPIAuthentication no/" \
	# -e "s/^GSSAPIAuthentication yes/#GSSAPIAuthentication yes/" \
	# -e "s/^GSSAPICleanupCredentials yes/#GSSAPICleanupCredentials yes/" \
	# -e "s/^#UseDNS yes/UseDNS no/" \
	#/etc/ssh/sshd_config
	service sshd restart



cat>>/etc/bashrc<<EOF
####vi->vim#####
#alias vi='vim'
#TMOUT=3600
export HISTFILESIZE=500000
export HISTSIZE=500000
export PROMPT_COMMAND="history -a"
export HISTTIMEFORMAT="%Y-%m-%d_%H:%M:%S "

EOF
. /etc/bashrc


cat >>/etc/sysctl.conf <<EOF
#net driver backlog for all socket, for very high speed network and data traffic;default is 1000
net.core.netdev_max_backlog = 20000

#the max ip packet of SoftIRQ can handle  in one cpu tick,default is 64
net.core.dev_weight = 256

#NAPI driver use this to determine total dev_weight of all interface(eth0,eth1...)
net.core.netdev_budget = 2048

#set core read memory for per socket (default:512, max: 8m) ,default for all kind of protocol
net.core.rmem_default = 524288
net.core.rmem_max = 8388608

#set core write memory for per socket(default:512, max: 8m),default for all kind of protocol
net.core.wmem_default = 524288
net.core.wmem_max = 8388608

#set each udp socket min read memory(2M),sysctl -w net.ipv4.udp_rmem_min=2097152
net.ipv4.udp_rmem_min = 2097152
#set each udp socket min write memory(1M),sysctl -w net.ipv4.udp_wmem_min=258048
net.ipv4.udp_wmem_min = 1048576

#each tcp socket read memory setting [min:64k,default:128k, max:8m],overwrite  net.core.rmem_default and net.core.rmem_max for tcp socket
net.ipv4.tcp_rmem = 65536 131072 8388608

#each tcp socket write memory setting [min:64k,default:128k, max:8m],overwrite  net.core.wmem_default and net.core.wmem_max for tcp socket
net.ipv4.tcp_wmem = 65536 131072 8388608

#max queue size for sync requesting
net.ipv4.tcp_max_syn_backlog = 8192

#max queue size for waiting for accepted at listening queue
net.core.somaxconn = 8192

#max unix-domain-datagram packets in the waiting queue ,as default it is 10
#cat /proc/sys/net/unix/max_dgram_qlen
#The SO_SNDBUF socket option does have an effect for UNIX domain sockets, but the SO_RCVBUF option does not
net.unix.max_dgram_qlen = 65535

#enable tcp syn cookie that will apply when syn queue is full
#now we have big syn queue that is ok to turn of syncookie
net.ipv4.tcp_syncookies = 1

#enable faster fast-retransfer of thin-stream, this property will work  when match thin-stream packet character
net.ipv4.tcp_thin_dupack = 1

#enable linear timeout of thin-stream; this property will work  when match thin-stream packet character
net.ipv4.tcp_thin_linear_timeouts = 1

#timestamps may cause the connection from same sub-domain refused by timestamp, 3g devices of same provider maybe in same domain
net.ipv4.tcp_timestamps = 0

#set output local port range
net.ipv4.ip_local_port_range = 12768 63000

#max number of TIME_WAIT sockets, over this the socket will be released quickly
net.ipv4.tcp_max_tw_buckets = 3000

#enable time-wait recycle hat has negative effect for client from same ip(nat)
#for server that has frequent webapi/rest api or client connect ,allow recycle is good
net.ipv4.tcp_tw_recycle = 0

#tcp syn ack try number(handle connect in)
net.ipv4.tcp_synack_retries = 5

#tcp syn send retry (handle connect out)
net.ipv4.tcp_syn_retries = 5

#wait time fo tcp socket keeping fin_wait
net.ipv4.tcp_fin_timeout = 5

#config max-files,sysctl -w fs.file-max=2097152
fs.file-max = 2097152

#config iptable ���ٱ��С���Ա������
#net.ipv4.ip_conntrack_max=10240

#reserved 256m memory for system
vm.min_free_kbytes = 262144

#low possibility for memory swap, default :60; for high memory instance may use 0
#vm.swappiness = 0

#config coredump
kernel.core_uses_pid = 1
kernel.core_pattern = /tmp/core-%e-%p-%t


EOF


}

function update_kernel(){
	rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
	rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm

	yum install -y yum-utils
	yum --enablerepo=elrepo-kernel -y install kernel-ml kernel-devel
	grub2-set-default 0
	echo_y "\033[31m Please Reboot it \[033[0m"
}

function install_reids(){
		yum install -y http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
		yum --enablerepo=remi -y install redis
}
function install_redis_standalone(){
        REDIS_PASSWORD="171010"
        echo "1.download and make redis......"
        [ `pgrep redis-server | wc -l` -ne 0 ] && echo_e "Install redis failed, there is redis running!" && return 1
        cd $INSTALL_HOME
        if [ -f redis-${REDIS_VERSION}-binary.tar.gz ];then
                echo_w "redis-${REDIS_VERSION}-binary.tar.gz is exist!!!"
        else
                wget ${DOWNLOAD_URL}/soft/redis/redis-${REDIS_VERSION}-binary.tar.gz .
        fi
        [ ! -f "redis-${REDIS_VERSION}-binary.tar.gz" ] && echo "No redis binary/code !!!" && return 1
        tar -xf redis-${REDIS_VERSION}-binary.tar.gz -C /usr/local/ && cd /usr/local/
        if [ ! -f redis-${REDIS_VERSION}/src/redis-server ]; then
                cd redis-${REDIS_VERSION}
                make
                cd -
        fi
        echo "2.start redis server......."
        if [ -f "redis-${REDIS_VERSION}/src/redis-server" ]; then
                [ ! -d "redis-${REDIS_VERSION}/deploy" ] && mkdir "redis-${REDIS_VERSION}/deploy"
                cd "redis-${REDIS_VERSION}/deploy"
                ../src/redis-server --bind 127.0.0.1 --port 30001 --cluster-config-file nodes-30001.conf --dbfilename dump-30001.rdb --logfile 30001.log --daemonize yes --requirepass ${REDIS_PASSWORD}
                cd -
        fi
        sleep 1
        [ `pgrep redis-server | wc -l` -eq 0 ] && echo_y "Install redis failed!" && return 2
}

function install_zabbix_uage(){
    echo_y "$0 install_zabbix SVRNAME \n\
                SVRNAME: ZABBIX MONITOR NAME"
}


function install_zabbix(){
	#aws
    shift
	ZBX_V=3.4
	ZBX_CHILD_V=14
    ZBX_DOWNURL="http://repo.zabbix.com/zabbix"
    ZBX_SERVER="54.241.26.36"
    SVRNAME=$1

    if [ $# -eq 0 -o $# -ne 1 ]; then
        install_zabbix_uage
        exit -1
    fi

    rel_sys

    if [ $sys_v == "amzn2" -o $sys_v == "el7" ]; then

        rpm -Uvh ${ZBX_DOWNURL}/${ZBX_V}/rhel/7/x86_64/zabbix-get-${ZBX_V}.${ZBX_CHILD_V}-1.el7.x86_64.rpm
        rpm -Uvh ${ZBX_DOWNURL}/${ZBX_V}/rhel/7/x86_64/zabbix-sender-${ZBX_V}.${ZBX_CHILD_V}-1.el7.x86_64.rpm
        rpm -Uvh ${ZBX_DOWNURL}/${ZBX_V}/rhel/7/x86_64/zabbix-agent-${ZBX_V}.${ZBX_CHILD_V}-1.el7.x86_64.rpm
    else
        rpm -Uvh ${ZBX_DOWNURL}/${ZBX_V}/rhel/6/x86_64/zabbix-get-${ZBX_V}.${ZBX_CHILD_V}-1.el6.x86_64.rpm
        rpm -Uvh ${ZBX_DOWNURL}/${ZBX_V}/rhel/6/x86_64/zabbix-sender-${ZBX_V}.${ZBX_CHILD_V}-1.el6.x86_64.rpm
        rpm -Uvh ${ZBX_DOWNURL}/${ZBX_V}/rhel/6/x86_64/zabbix-agent-${ZBX_V}.${ZBX_CHILD_V}-1.el6.x86_64.rpm
    fi

	 wget http://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-get-3.4.14-1.el7.x86_64.rpm
     wget http://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-sender-3.4.14-1.el7.x86_64.rpm
	 wget http://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-agent-3.4.14-1.el7.x86_64.rpm

	# wget http://repo.zabbix.com/zabbix/3.4/rhel/6/x86_64/zabbix-get-3.4.14-1.el6.x86_64.rpm
    # wget http://repo.zabbix.com/zabbix/3.4/rhel/6/x86_64/zabbix-sender-3.4.14-1.el6.x86_64.rpm
	# wget http://repo.zabbix.com/zabbix/3.4/rhel/6/x86_64/zabbix-agent-3.4.14-1.el6.x86_64.rpm	

	sed -i -e "s/Server=.*/Server=${ZBX_SERVER}/g" \
	-e "s/ServerActive=.*/ServerActive=${ZBX_SERVER}/g" \
	-e 's/# StartAgents=3/StartAgents=0/g' \
	-e "s/^Hostname=Zabbix server/Hostname=${SVRNAME}/g"  /etc/zabbix/zabbix_agentd.conf

    service zabbix-agent start
    chkconfig zabbix-agent on

}


function install_zabbix_yum(){
	#local version=$1
	#wget https://repo.zabbix.com/zabbix/3.4/rhel/6/x86_64/zabbix-release-3.4-1.el6.noarch.rpm
	rpm -Uvh https://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-release-3.4-2.el7.noarch.rpm
}


function install_mysql(){
	yum -y install http://dev.mysql.com/get/mysql57-community-release-el7-10.noarch.rpm
	yum -y install mysql-community-server
	systemctl start  mysqld.service
    systemctl enable  mysqld.service

	oldpasswd=`grep 'temporary password' /var/log/mysqld.log | awk '{print $NF}'`

	ALTER USER 'root'@'localhost' IDENTIFIED BY "$pass";
	create databases $dbname;
	echo "bind-address = 127.0.0.1" >> /etc/mysql.cnf

}

function install_openjdk(){
	local jdkv=$1
	yum -y install java-${jdkv}-openjdk java-${jdkv}-openjdk-devel
}

function install_oraclejdk8(){
	wget http://build.up-gram.com/soft/jdk/jdk-8u111-linux-x64.tar.gz 
	tar -zxvf jdk-8u111-linux-x64.tar.gz -C /opt/
cat >> /etc/profile <<EOF
export JAVA_HOME=/opt/jdk1.8.0_111
export CLASSPATH=\$JAVA_HOME/lib:\$CLASSPATH
export JRE_HOME=\$JAVA_HOME/jre 
export PATH=\$JAVA_HOME/bin:\$PATH
EOF
	. /etc/profile && java -version
}

function install_oraclejdk7(){
	wget http://build.up-gram.com/soft/jdk/jdk-7u15-linux-x64.tar.gz
	tar -zxvf jdk-7u15-linux-x64.tar.gz -C /opt/
cat >> /etc/profile <<EOF
export JAVA_HOME=/opt/jdk1.7.0_15
export CLASSPATH=\$JAVA_HOME/lib:\$CLASSPATH
export JRE_HOME=\$JAVA_HOME/jre 
export PATH=\$JAVA_HOME/bin:\$PATH
EOF
	. /etc/profile && java -version
}


function install_website(){
	yum -y install apache php php-mysql
}


function install_yumnginx(){
	#创建ssl文件存放目录
	mkdir -p /mnt/sdg/website/ssl/
	#创建程序存放目录
	mkdir -p /home/tengzhan/frontend 
	sudo yum -y install nginx | sudo amazon-linux-extras install nginx1.12 
	mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
	wget http://build.up-gram.com/soft/nginx/nginx.conf -O /etc/nginx/nginx.conf
	wget http://build.up-gram.com/soft/nginx/proxy.conf -O /etc/nginx/proxy.conf
	wget http://build.up-gram.com/soft/nginx/website.conf -O /etc/nginx/conf.d/website.conf
	echo -e "\033[31m Check the nginx config and start it \033[0m"
}


function install_pip(){
	curl -s https://bootstrap.pypa.io/get-pip.py | /usr/bin/python
	ln -s /usr/local/python2/bin/pip2 /usr/bin/pip
}


function install_docker(){
	yum install -y yum-utils device-mapper-persistent-data lvm2
	yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	#yum-config-manager --enable docker-ce-edge
	yum list docker-ce --showduplicates | sort -r
}


function install_tomcat(){
	tomcat_v="apache-tomcat-8.5.23_dt.tar.gz"
	mkdir -p /mnt/tomcat/logs/
	groupadd tomcat
	mkdir -p /opt/tomcat
	useradd -s /bin/bash -g tomcat -d /opt/tomcat tomcat
	usermod -L tomcat
	#wget http://www-us.apache.org/dist/tomcat/tomcat-8/v8.5.33/bin/apache-tomcat-8.5.33.tar.gz
	if [ ! -f $tomcat_v ]; then
		wget http://build.up-gram.com/soft/tomcat/${tomcat_v}
	fi		
	tar -zxvf ${tomcat_v} -C /opt/tomcat --strip-components=1
	cd /opt/tomcat
	chgrp -R tomcat conf
	chmod g+rwx conf
	chmod g+r conf/*
	chown -R tomcat logs/ temp/ webapps/ work/
	chown -R tomcat /mnt/tomcat/logs/
	chgrp -R tomcat bin
	chgrp -R tomcat lib
	chmod g+rwx bin
	chmod g+r bin/

	wget http://build.up-gram.com/soft/tomcat/tomcat -O /etc/init.d/tomcat
	chmod +x /etc/init.d/tomcat
	chkconfig --add tomcat
	chkconfig tomcat on

# 	cat >> /etc/systemd/system/tomcat.service<<EOF
# [Unit]
# Description=Apache Tomcat Web Application Container
# After=syslog.target network.target

# [Service]
# Type=forking

# Environment=JAVA_HOME=/usr/lib/jvm/jre
# Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
# Environment=CATALINA_HOME=/opt/tomcat
# Environment=CATALINA_BASE=/opt/tomcat
# Environment='CATALINA_OPTS=-Xms16384M -Xmx32768M -server -XX:+UseParallelGC'
# Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

# ExecStart=/opt/tomcat/bin/startup.sh
# ExecStop=/bin/kill -15 $MAINPID

# User=tomcat
# Group=tomcat

# [Install]
# WantedBy=multi-user.target

# EOF

# 	yum install haveged
# 	systemctl start haveged.service
# 	systemctl enable haveged.service
# 	systemctl start tomcat.service
# 	systemctl enable tomcat.service
}



function install_tengine(){
    version=$1
	cd $INSTALL_HOME

	if [ -f tengine-$version.tar.gz ]  ; then
				   tar zxvf tengine-$version.tar.gz
	else
					wget http://tengine.taobao.org/download/tengine-$version.tar.gz
					tar zxvf tengine-$version.tar.gz
	fi

	if [ -f ngx_cache_purge-2.3.tar.gz ] ; then
					tar zxvf ngx_cache_purge-2.3.tar.gz
	else
					wget http://labs.frickle.com/files/ngx_cache_purge-2.3.tar.gz
					#lftp -c "pget -n 10 $FTP_ADDR/softwares/tengine/ngx_cache_purge-2.3.tar.gz"
					tar zxvf ngx_cache_purge-2.3.tar.gz
	fi
}

# install_tomcat(){
# 	wget http://mirrors.shu.edu.cn/apache/tomcat/tomcat-8/v8.5.37/bin/apache-tomcat-8.5.37.tar.gz
# }

function install_zk(){
	wget https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/zookeeper-3.4.14/zookeeper-3.4.14.tar.gz
	tar -zxvf zookeeper-3.4.14.tar.gz -C /opt/
	mkdir /mnt/zookeeper-data
	cd /opt/zookeeper-3.4.14/conf
	mv zoo_sample.cfg zoo.cfg
	sed -i 's/\/tmp\/zookeeper/\/mnt\/zookeeper-data/g' zoo.cfg
	cat >> /opt/zookeeper-3.4.14/config/zoo.cfg <<EOF
server.11=10.1.12.39:2888:3888
server.12=10.1.3.216:2888:3888
server.13=10.1.6.134:2888:3888
EOF
	echo "11" > /opt/zookeeper-data/myid
	echo "12" > /opt/zookeeper-data/myid
	echo "13" > /opt/zookeeper-data/myid
}


function install_kafka(){
	wget https://archive.apache.org/dist/kafka/2.1.0/kafka_2.12-2.1.0.tgz
	tar -zxvf kafka_2.12-2.1.0.tgz -C /opt/
	mkdir -p /mnt/kafka-data
	cd /opt/kafka_2.12-2.1.0/config/
	sed -i 's/log\.dirs=\/tmp\/kafka-logs/log\.dirs=\/mnt\/kafka-data/g' server.properties
	sed -i '/zookeeper.connect/s/localhost:2181/10.1.12.39:12181,10.1.3.216:12181,10.1.6.134:12181/g' server.properties
}

function install_es(){
	wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.0.tar.gz
}

function install_es_for_yum(){

	cat >> /etc/yum.repos.d/elasticsearch.repo<<EOF
[elasticsearch-7.x]	
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md

EOF
	sudo yum -y install elasticsearch
	mkdir -p /mnt/{elasticsearch-data,elasticsearch-log}
	sed -i 's#/var/lib/elasticsearch#/mnt/elasticsearch-data#g' /etc/elasticsearch/elasticsearch.yml
	sed -i 's#/var/log/elasticsearch#/mnt/elasticsearch-log#g' /etc/elasticsearch/elasticsearch.yml
}

function install_node(){
	wget  https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-x64.tar.xz
	xz -d node-v10.16.3-linux-x64.tar.xz
	tar -xvf node-v10.16.3-linux-x64.tar && mv node-v10.16.3-linux-x64 /usr/local/node
	ln -s /usr/local/node/bin/node  /usr/bin/node
	ln -s /usr/local/node/bin/npm /usr/bin/npm
	npm -y install pm2 -g
	ln -s /usr/local/node/bin/pm2 /usr/bin/pm2
}

function install_php(){
	local php_v=$1
	#php_tar=php-${php_v}.tar.gz
	#7.1.33
	wget --no-check-certificate https://www.php.net/distributions/php-${php_v}.tar.gz -O $INSTALL_HOME/php-${php_v}.tar.gz
	tar -zxvf php-${php_v}.tar.gz 

}





function install_openresry(){
	rel_sys
    OPENRESTY_HOMEE="/usr/local/openresty"
    sudo yum install -y yum-utils
    if [ $sys_v =~ "amzn" ]; then
        sudo yum-config-manager --add-repo https://openresty.org/package/amazon/openresty.repo
    else
        sudo yum-config-manager --add-repo https://openresty.org/package/rhel/openresty.repo
    fi
    sudo yum -y install openresty
    cd $OPENRESTY_HOMEE
    mkdir -p nginx/{conf.d, svrssl}
	mv nginx/conf/nginx.conf nginx/conf/nginx.conf.bak
	wget http://build.up-gram.com/soft/nginx/nginx.conf -O nginx/conf/nginx.conf
	wget http://build.up-gram.com/soft/nginx/proxy.conf -O nginx/conf/proxy.conf
	wget http://build.up-gram.com/soft/nginx/website.conf -O nginx/conf/conf.d/website.conf
}




case $1 in 
	init_sys)
			init_sys
			;;
	install_reids)
			install_reids
			;;
    install_redis_standalone)
            install_redis_standalone
            ;;
	install_zabbix)
			install_zabbix $*
			;;
	install_zabbix_yum)
			install_zabbix_yum
			;;
	install_mysql)
			install_mysql
			;;
	install_openjdk7)
			install_openjdk 1.7.0
			;;
	install_openjdk8)
			install_openjdk 1.8.0
			;;
	install_yumnginx)
			install_yumnginx
			;;
	install_pip)
			install_pip
			;;
	install_docker)
			install_docker
			;;										
	install_tomcat)
			install_tomcat
			;;
	install_tengine)
			install_tengine 2.3.1
			;;
	install_zk)
			install_zk
			;;
	install_kafka)
			install_kafka
			;;									
	install_es_for_yum)
			install_es_for_yum
			;;
	install_node)
			install_node
			;;
	install_oraclejdk7)
			install_oraclejdk7
			;;
	install_oraclejdk8)
			install_oraclejdk8
			;;
	*)
		echo "Usage:
				init_sys
				install_reids
				install_redis_standalone
				install_zabbix
				install_zabbix_yum
				install_mysql
				install_openjdk7
				install_openjdk8
				install_oraclejdk7
				install_oraclejdk8
				install_nginx
				install_pip
				install_docker
				install_tomcat
				install_tengine
				install_zk
				install_kafka
				install_es_for_yum
				install_node
			;;
esac																
