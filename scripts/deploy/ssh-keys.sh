#!/bin/bash

# curl -s  http://wuyou.run/scripts/deploy/ssh-keys.sh  |bash

Push_key ()
{
    
    su - root -c 'mkdir -p /root/.ssh'
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCWTZuvSNlzGjSf7f4TBSE6EeMxCHNk6UZeiy/EP0qVnuMnNzY/PSdm7+xZjk6THeGGBKC4FKOQEaG7diOLrVvcC61WQbXCSIA09OIHjzt5xw/Pp0rCNZqrPC4p+b04PMV+nDXKOo9+/S79NBgiQwDmiQck9P+Fj5A7q3mQi4DQBlSpVlSZ26DgLzSLqiJD4A6/65r84e3Df2hJnb5yeAv9+5e04a36OECI5vzJJUdVSYuwhII4cc9hX+gTpGUZclG4bcilLjgAey2S7as56f/KWYaYuCprCCvK6/YK/HF35mpVbLxe4dpfPgi3NMKrPYlDw98ddrvQR2CHgtQqvIFZ skyvpn-default">>/root/.ssh/authorized_keys
    chown root.root /root/.ssh/authorized_keys
    chmod 700 /root/.ssh ;chmod 600 /root/.ssh/authorized_keys


    useradd admin
    su - admin -c 'mkdir -p /home/admin/.ssh'
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3pPyWrNAYEIVq2yxN287isqYN8Uex3MH3ksGF9NJF2WvVnkKMohxxrS2cp9IZrOgh7CUvPUvBgud8Croi41jaSUF58SuaUGjiVllE62dwyu58NKzPZ097Jk/LRBirYbSx9Ug6Hx95zs49pk2IJCj69lP4BZeOFWVM2YM9LbIMuZdIYJLdedAqdEPTk3LyHMwEEMJ0kc2VpyfnogYiKEIKvRlnggBLt+221vR1piY0lUFeo/+vY03KUcSekHkMfpOKYvc4roZXFewQ/hjzIG6itgYN4L+vIPLiW5I1g2PzTmQOz0GOlPKjW7UK22LiQrA+MUP71LpgaH0l0AdtZNLp admin.pem.pub">>/home/admin/.ssh/authorized_keys
    chown admin.admin /home/admin/.ssh/authorized_keys
    chmod 700 /home/admin/.ssh ;chmod 600 /home/admin/.ssh/authorized_keys
}
Push_key