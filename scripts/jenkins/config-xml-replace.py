'''
Author: Logan.Li
Gitee: https://gitee.com/attacker
email: admin@attacker.club
Date: 2023-08-06 14:53:17
LastEditTime: 2023-08-06 15:02:06
Description: 
'''
import os
import fileinput

def traverse_directories(directory):
    """
    遍历目录并输出所有找到的config.xml文件的路径

    Args:
        directory (str): 要遍历的目录路径
    """
    for root, dirs, files in os.walk(directory):
        dirs[:] = [d for d in dirs if d.startswith('market')]
        for file in files:
            if file == "config.xml":
                file_path = os.path.join(root, file)
                print("替换文件:",file_path)
                with fileinput.FileInput(file_path, inplace=True) as file:
                    for line in file:
                        line = line.replace("e830533b-f6c8-xxxxxx", "01ec90a6-123e-444a-xxxxx")
                        print(line, end='')
                     
def main():
    directory = "/data/jenkins/jobs"
    traverse_directories(directory)

if __name__ == "__main__":
    main()