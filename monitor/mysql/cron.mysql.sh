#!/bin/bash
###
# @Author: 以谁为师
# @Website: attacker.club
# @Date: 2020-10-15 23:26:29
# @LastEditTime: 2020-10-16 01:06:29
# @Description:
###

username=$1
password=$2
addr=$3
zabbix_server=$4
zabbix_agent=$5

mysql_bin=$(which mysql)
# mysql客户端

max_connections=$($mysql_bin -u$username -p$password -h$addr -e" show variables where Variable_name='max_connections';" 2>/dev/null | awk '{print $2}')
current_connections=$($mysql_bin -u$username -p$password -h$addr -e"show global status where Variable_name='Threads_connected';" 2>/dev/null | awk '{print $2}')
mysql_slow=$($mysql_bin -u$username -p$password -h$addr -e"show global status like '%slow%';" 2>/dev/null | awk '/queries/ {print $2}')

echo "max_connections:$max_connections"
echo "current_connections:$current_connections"
echo "mysql_slow:$mysql_slow"

zabbix_sender -z $zabbix_server -p 10051 -s $zabbix_agent -k zbx.mysql_slow -o $mysql_slow
zabbix_sender -z $zabbix_server -p 10051 -s $zabbix_agent -k zbx.current_connections -o $current_connections
zabbix_sender -z $zabbix_server -p 10051 -s $zabbix_agent -k zbx.max_connections -o $max_connections

zabbix_sender --help
#-z服务器 -s 本机 -k key -o 自定义输出文本或者数字  -vv
