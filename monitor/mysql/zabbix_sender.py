#!/usr/bin/env python
# -*- coding: utf-8 -*-

# python zabbix_sender.py  prod-rds-1


import sys
import os

try:
    import configparser
except:
    import ConfigParser as configparser


config = configparser.ConfigParser()
config.read('/Users/lijingjing/mysql_pool.conf')
config.sections()

if len(sys.argv) >= 2:
    instance_name = sys.argv[1]
else:
    print("No parameters")
    exit()


if instance_name in config:
    try:
        mysql_address = config[instance_name]['address']
        mysql_username = config[instance_name]['username']
        mysql_passwd = config[instance_name]['password']
    except Exception as e:
        print(e)
else:
    print('参数不存在')


zabbix_server = sys.argv[2]
zabbix_agent = sys.argv[3]
os.system("./cron.mysql.sh %s %s %s %s %s" % (
    mysql_username, mysql_passwd, mysql_address, zabbix_server, zabbix_agent))
