import json
import re
import sys
if sys.version > '3':
    from urllib import request 
else:
   import urllib2  as request

"""
python domain.discovery.py 
"""

def getList(url):
    response = request.urlopen(url)
    res = response.readlines()
    return res

def output(url_list):
    url_json = []
    for line in url_list:
        try:
            url, desc = line.decode().strip().split('|')
            url_json += [{'{#DESC}': desc.strip(), '{#URL}': url.strip()}]
        except IndexError as e:
            logger.error(str(e))

    data = json.dumps({'data': url_json},sort_keys=True, indent=4, separators=(',',':'))
    print(data)
       
def main(url):
    ret = getList(url)
    if ret:
        output(ret)


if __name__ == '__main__':
    url = 'http://54.241.26.36:8280/zabbix/domain.txt'
    main(url)