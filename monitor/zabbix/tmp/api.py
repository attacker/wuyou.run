'''
Author: admin@attacker.club
Date: 2022-08-19 10:45:34
LastEditTime: 2022-08-19 11:21:40
Description: 
'''
import requests
import subprocess
import sys


def run_exec(cmd):
    """ 执行系统命令 """
    (status, output) = subprocess.getstatusoutput(cmd)
    context = {
        'status': status,
        'output': output,
    }
    print (context)

def getData(url):
    req = requests.get(url)
    if req.status_code == 200:
        return req.json()['data']
    


if __name__ == "__main__":
    url="https://lbwapi-admin.ramboplay.com/redwarnumber/online/data"
    
    ZBX_Server = "172.26.144.18"
    try:
        data = getData(url)
        run_exec(f"/usr/bin/zabbix_sender -z %s -p 10051 -s %s -k custom.redwarws  -o {data}" )
    except AttributeError as e:
        pass

    
