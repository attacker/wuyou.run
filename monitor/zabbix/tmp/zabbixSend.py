'''
Author: admin@attacker.club
Date: 2022-08-19 10:45:34
LastEditTime: 2022-08-19 12:44:55
Description: 
'''
import requests
import subprocess
import sys


def command(cmd):
    """ 执行系统命令 """
    (status, output) = subprocess.getstatusoutput(cmd)
    context = {
        'status': status,
        'output': output,
    }
    print (context)

def getData(url):
    req = requests.get(url)
    if req.status_code == 200:
        return req.json()['data']
    


if __name__ == "__main__":
    url="https://lbwapi-admin.ramboplay.com/redwarnumber/online/data"
    
    max = 2 # 重复发送次数
    zbxserver = "127.0.0.1"
    hostname = "Zabbix server" # 主机名一致
    data = getData(url)

    for i in range(max):
        command('/usr/bin/zabbix_sender -z "{zbxserver}" -p 10051 -s "{hostname}" -k ws.battles  -o {data["battles"]}')
        command('/usr/bin/zabbix_sender -z "{zbxserver}" -p 10051 -s "{hostname}" -k ws.gamers  -o {data["gamers"]}')
        command('/usr/bin/zabbix_sender -z "{zbxserver}" -p 10051 -s "{hostname}" -k ws.users  -o {data["users"]}')
