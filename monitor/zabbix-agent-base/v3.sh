#!/bin/bash

# curl -s  http://wuyou.run/monitor/zabbix-agent-base/v3.sh|bash  -s   zabbix-ip   hostname  active


ZBX_SERVER=$1 # zabbix服务器地址
HOSTNAME=$2  # 主机名称
mode=$3 # 模式Active，agent


yum remove zabbix-agent -y
if [ -f /usr/local/src/zabbix-agent-*.rpm ]; then
    yum install -y /usr/local/src/zabbix-agent-*.rpm
else
    rpm -e zabbix-release
    rpm -Uvh https://repo.zabbix.com/zabbix/3.0/rhel/7/x86_64/zabbix-release-3.0-1.el7.noarch.rpm 
    yum --debuglevel=1  install   zabbix-agent -y
fi



Set_hostname() {
  if [ -f /etc/hostname ]; then
    echo "$HOSTNAME" >/etc/hostname
  fi
  sed -i "/HOSTNAME/c HOSTNAME=$HOSTNAME" /etc/sysconfig/network || echo "HOSTNAME=$HOSTNAME" >>/etc/sysconfig/network
  hostname $HOSTNAME
  grep $HOSTNAME /etc/hosts || echo "127.0.0.1 $HOSTNAME" >>/etc/hosts
}

Set_zabbix_conf(){
    cat >/etc/zabbix/zabbix_agentd.conf<<EOF
PidFile=/var/run/zabbix/zabbix_agentd.pid
LogFile=/var/log/zabbix/zabbix_agentd.log
LogFileSize=0
Server=${ZBX_SERVER}
ServerActive=${ZBX_SERVER}
Hostname=$HOSTNAME
Include=/etc/zabbix/zabbix_agentd.d/*.conf
EOF
    if [ "$mode" == "active" ];then
        sed -i  '/Server=/d' /etc/zabbix/zabbix_agentd.conf
        sed -i '4a,StartAgents=0' /etc/zabbix/zabbix_agentd.conf
    fi
    service   zabbix-agent restart
}

Set_hostname
Set_zabbix_conf





