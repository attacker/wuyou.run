#!/bin/bash
###
 # @Author: admin@attacker.club
 # @Date: 2022-08-18 17:24:58
 # @LastEditTime: 2022-08-19 09:54:39
 # @Description: 
### 
# curl -sL http://wuyou.run/monitor/zabbix-agent-base/zabbix-agent2.sh | bash -s 172.26.144.18

ZBX_SERVER=$1

if [ -f /root/zabbix-agent2-6.0.7-1.el8.x86_64.rpm ]; then
    yum --debuglevel=1 install -y /root/zabbix-agent2-6.0.7-1.el8.x86_64.rpm
else
    yum --debuglevel=1 install -y https://repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/zabbix-agent2-6.0.7-1.el8.x86_64.rpm
fi

Set_zabbix_conf(){
    cat >/etc/zabbix/zabbix_agent2.conf<<EOF
PidFile=/run/zabbix/zabbix_agent2.pid
LogFile=/var/log/zabbix/zabbix_agent2.log
LogFileSize=0

StartAgents=0 # 表示关闭被动模式
ServerActive=${ZBX_SERVER}
Hostname=$HOSTNAME
Include=/etc/zabbix/zabbix_agent2.d/*.conf
Include=./zabbix_agent2.d/plugins.d/*.conf
ControlSocket=/tmp/agent.sock
EOF
    systemctl restart zabbix-agent2
}

Set_zabbix_conf