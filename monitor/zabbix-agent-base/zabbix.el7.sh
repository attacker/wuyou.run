#!/bin/bash
###
 # @Author: admin@attacker.club
 # @Date: 2022-09-08 23:50:44
 # @LastEditTime: 2022-09-09 00:56:53
 # @Description: 
### 
# curl -s http://zabbix.c5game.com/agent/zabbix.el7.sh|bash  -s  zabbix.c5game.com  cdh-us-11


ZBX_SERVER=$1
HOSTNAME=$2


#### 安装 agent  ####
if [ -f /root/zabbix-agent2-6.0.7-1.el7.x86_64.rpm ]; then
    yum install -y /root/zabbix-agent2-6.0.7-1.el7.x86_64.rpm
else
    yum --debuglevel=1  install http://zabbix.c5game.com/agent/zabbix-agent2-6.0.7-1.el7.x86_64.rpm  -y
fi


#### 修改主机名   ####
Set_hostname() {
    if [ -f /etc/hostname ]; then
        echo "$HOSTNAME" >/etc/hostname
    fi
    sed -i "/HOSTNAME/c HOSTNAME=$HOSTNAME" /etc/sysconfig/network || echo "HOSTNAME=$HOSTNAME" >>/etc/sysconfig/network
    hostname $HOSTNAME
    grep $HOSTNAME /etc/hosts || echo "127.0.0.1 $HOSTNAME" >>/etc/hosts
}


#### 修改agent配置  ####
Set_zabbix_conf(){
    cat >/etc/zabbix/zabbix_agent2.conf<<EOF
PidFile=/run/zabbix/zabbix_agent2.pid
LogFile=/var/log/zabbix/zabbix_agent2.log
LogFileSize=0

ServerActive=${ZBX_SERVER}
Hostname=$HOSTNAME
Include=/etc/zabbix/zabbix_agent2.d/*.conf
Include=./zabbix_agent2.d/plugins.d/*.conf
ControlSocket=/tmp/agent.sock
EOF
    curl -fsSL -O  --max-time 30 --retry 3 --retry-delay 6  http://zabbix.c5game.com/agent/zabbix_agentd.d.tar.gz
    # tar指定解压路径,去掉一层目录
    tar zxvf zabbix_agentd.d.tar.gz  -C /etc/zabbix/zabbix_agent2.d/  --strip-components 1 
    systemctl restart zabbix-agent2
}

# Set_hostname
Set_zabbix_conf