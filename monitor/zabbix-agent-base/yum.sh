#!/bin/bash
###
 # @Author: admin@attacker.club
 # @Date: 2022-06-20 18:08:05
 # @LastEditTime: 2022-08-18 17:44:16
 # @Description: 
### 

# curl -s  http://wuyou.run/monitor/zabbix-agent-base/yum.sh|bash  -s   54.241.26.36 cdh-us-11


ZBX_SERVER=$1
HOSTNAME=$2


#### 安装   ####
yum remove zabbix-agent zabbix-release  -y

if [ -f /root/zabbix-agent-3.2.3-1.el6.x86_64.rpm ]; then
yum install -y /root/zabbix-agent-3.2.3-1.el6.x86_64.rpm
else
# rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm
yum --debuglevel=1  install  https://internal.up-gram.com/linux/zabbix-agent-3.2.3-1.el6.x86_64.rpm  -y
fi



Set_hostname() {

  if [ -f /etc/hostname ]; then
    echo "$HOSTNAME" >/etc/hostname
  fi
  sed -i "/HOSTNAME/c HOSTNAME=$HOSTNAME" /etc/sysconfig/network || echo "HOSTNAME=$HOSTNAME" >>/etc/sysconfig/network
  hostname $HOSTNAME
  grep $HOSTNAME /etc/hosts || echo "127.0.0.1 $HOSTNAME" >>/etc/hosts
}

Set_zabbix_conf(){
    
    cat >/etc/zabbix/zabbix_agentd.conf<<EOF
PidFile=/var/run/zabbix/zabbix_agentd.pid
LogFile=/var/log/zabbix/zabbix_agentd.log
LogFileSize=0
StartAgents=0
ServerActive=${ZBX_SERVER}
Hostname=$HOSTNAME
Include=/etc/zabbix/zabbix_agentd.d/*.conf
EOF

    # systemctl restart zabbix-agent
    service   zabbix-agent restart
}

Set_hostname
Set_zabbix_conf





