#!/usr/bin/env python
# coding=utf-8
'''
Author: LJ
Date: 2020-11-20 23:16:57
LastEditTime: 2020-11-22 17:17:39
Description:
'''

import re
import os
import sys
import shlex
import platform
import argparse
# import logging
# from logging.handlers import RotatingFileHandler


def file_io(path, method, content):
    """ 文件写入  """
    with open(path, method) as f:
        f.write(content)


def bash(cmd):
    """
    执行执行bash命令
    """
    logger.info(cmd)
    return shlex.os.system(cmd)


def parse_args():
    """
    获取参数
    """

    content = """

    查看帮助:
           python xxx.py
           python xxx.py --help
    """
    parser = argparse.ArgumentParser(
        usage="py工具 ^_^",
        description=content,
        add_help=False,
        formatter_class=lambda prog: argparse.RawTextHelpFormatter(
            prog, max_help_position=50)
    )
    parser.add_argument("--help",
                        action="help",
                        help="查看帮助信息")
    parser.add_argument("--uninstall",
                        action="store_true",
                        help="卸载")

    args = parser.parse_args()
    return args


def pre_install():
    if platform.system() == 'Linux':
        bash("curl -s http://mirrors.aliyun.com/repo/epel-7.repo >/etc/yum.repos.d/epel-7.repo")
        bash("yum  --debuglevel=1  python-pip -y")
        bash("pip install jinja2")

def main():
    pre_install()

if __name__ == '__main__':
    # logger = LoggerLog("Tools").get_log

    if len(sys.argv) == 1:
        """ 如果没有传参则打印帮助信息 """
        sys.argv.append("--help")
        parse_args()
        sys.exit(1)
    
    params = parse_args()

