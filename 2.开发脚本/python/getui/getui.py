'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-22 16:50:03
LastEditTime: 2022-07-20 10:17:56
Description:  
python getui.py appkey mastersecret wxkey 
'''
import hashlib
import time
import sys
import json
import requests
requests.packages.urllib3.disable_warnings()


## 计算百分百；单位w
current=2
total=200
percent=eval('{:.0f}'.format(int(current)/int(total)*100)) 
if percent >= 2:
    print(f'percent:{percent}%',type(percent))



class GeTui(object) :
    def __init__(self,appid,appkey,mastersecret):
        self.appkey = appkey
        self.mastersecret = mastersecret
        self.url = f"https://restapi.getui.com/v2/{appid}" # App Id
        self.headers = {'Content-Type': 'application/json'}
        self.token = self.get_token()

    def push_count(self):
        """
        接口地址: BaseUrl/report/push/count
        查询应用当日可推送量和推送余量
        """
        pass

    def get_token(self):
        """
        参考文档: https://docs.getui.com/getui/server/rest_v2/token
        """       
        timestamp = str(round(time.time() * 1000)) # 获取13位时间戳
        sign_str  = f"{self.appkey}{timestamp}{self.mastersecret}" # 将 appkey、timestamp、mastersecret 对应的字符串按此固定顺序拼接
        s = hashlib.sha256() # 使用 SHA256 算法加密
        s.update(sign_str.encode()) 
        sign = s.hexdigest()
    
        payload = {
             "sign": sign,
             "timestamp": timestamp,
             "appkey": appkey
        }
        r = requests.post(url=f"{self.url}/auth", headers=self.headers,data=json.dumps(payload), verify=False)
        if r.status_code == 200:
            print(r.json())
            return  r.json()['data']['token']



if __name__ == "__main__":
    appid =  sys.argv[1]
    appkey = sys.argv[2]
    mastersecret = sys.argv[3]
    
    GT = GeTui(appid,appkey,mastersecret)
    #GT.push_count()

