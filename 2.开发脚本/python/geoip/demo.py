'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-20 19:53:53
LastEditTime: 2023-08-24 14:22:26
Description: 
'''
# maxmind-database.mmdb
import geoip2.database
with geoip2.database.Reader('/Users/loganli/Downloads/GeoLite2-City.mmdb') as reader:
    response = reader.city('121.43.57.130');
    if response.country.iso_code != 'CN':
        Region = response.subdivisions.most_specific.names.get('zh-CN', None)
  
    else:
        Region =  response.city.names.get('zh-CN', None)

    print("国家: ", response.country.names['zh-CN'])  # names['zh-CN']转换为中文
    print("地区: ", Region)
    # print(response.city.names) 
    print("纬度: ", response.location.latitude)  # 位置
    print("经度: ", response.location.longitude)
    print("时区：", response.location.time_zone)



