#!/usr/bin/env python
# coding=utf-8
'''
Author: 以谁为师
Website: attacker.club
Date: 2020-09-01 00:47:33
LastEditTime: 2022-07-12 15:58:10
Description: 
'''
import sys
import os

import os.path as op
level5 = op.abspath(op.join(__file__, op.pardir, op.pardir, op.pardir, op.pardir, op.pardir))

level1 = os.path.dirname(__file__) # 获取当前运行脚本的绝对路径  
level2 = os.path.dirname(os.path.dirname(__file__))  # 获取当前运行脚本的绝对路径的上一级路径（去掉最后一个路径）  
level3 = os.path.dirname(os.path.dirname(os.path.dirname(__file__))) # 获取当前运行脚本的绝对路径的上一级路径的上一级路径（去掉最后2个路径）
level4 =  os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


# import xxx，默认情况下python解析器会搜索当前目录、已安装的内置模块和第三方模块，搜索路径存放在sys模块的path中的模块
sys.path.append(level4) 
print(sys.path[-1],level5)
from globalConfig import ALIYUN_CONFIG
print(ALIYUN_CONFIG)

# import json
## json 文件
# configfile  =  '{}/globalConfig.py'.format(path4)
# with open(configfile) as fp: 
#     json_data = json.load(fp)  # 读取file point