'''
Author: admin@attacker.club
Date: 2022-09-10 06:42:19
LastEditTime: 2022-09-10 06:43:35
Description: 
pip install aliyun-python-sdk-eipanycast==1.0.2
'''


import sys
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential
from aliyunsdkeipanycast.request.v20200309.ListAnycastEipAddressesRequest import ListAnycastEipAddressesRequest


request = ListAnycastEipAddressesRequest()
request.set_accept_format('json')

if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]
    credentials = AccessKeyCredential(id, secret)
    client = AcsClient(region_id='cn-shanghai', credential=credentials)
    response = client.do_action_with_exception(request)
    # python2:  print(response) 
    print(str(response, encoding='utf-8'))


