#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-07-23 11:24:26
LastEditTime: 2020-08-17 10:54:28
@Description: 
'''
import time
import sys
import json

from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcms.request.v20190101.DescribeMetricDataRequest import DescribeMetricDataRequest

# client = AcsClient('LTAI4G3b8aqwJiKtiRLaDxu4',
#                    'lzhBwAqbUVsyQtYddwOxqhifJwe5QS', 'me-east-1')
client = AcsClient('LTAI4G3b8aqwJiKtiRLaDxu4',
                   'lzhBwAqbUVsyQtYddwOxqhifJwe5QS', 'me-east-1')


request = DescribeMetricDataRequest()
request.set_accept_format('json')

# 采样周期为60s，Period赋值为60或60的整数倍。
request.set_Period("60")

# 命名空间
request.set_Namespace("acs_kvstore")


# 监控项: 已用容量
request.set_MetricName("SplitrwUsedMemory")
# 实例id
request.set_Dimensions(
    {'instanceId': "r-tc5hesxlqmzk7nvmk8"}
)

# response
response = client.do_action_with_exception(request)
print(str(response, encoding='utf-8'))
