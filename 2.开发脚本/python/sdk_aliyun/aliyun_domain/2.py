'''
Author: admin@attacker.club
Date: 2022-09-21 11:40:46
LastEditTime: 2022-09-21 12:01:25
Description: 
'''
try:
            next = 0
            instances = []
            for i in range(1, 100): 
                req.set_PageNumber(i)
                result = json.loads(client.do_action_with_exception(req))
                curInstances =  result.get('Instances').get('Instance')
                TotalCount = result['TotalCount']
                instances = instances + curInstances
                if next == 1:
                    break
                if not result['NextToken']:  
                    next = 1 
            print("instances len:",len(instances),"TotalCount:",TotalCount)
            return {'code': 200, 'data': instances}
        except Exception as e:
            return {'code': 500, 'msg': e}