#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-05-14 14:17:49
@LastEditTime: 2020-05-14 14:21:54
@Description: 
'''
import sys
import time
import hashlib
import os
import oss2
from itertools import islice
import datetime


today = datetime.date.today()
yesterday = today - datetime.timedelta(days=1)
nowtime = yesterday.strftime('%Y-%m-%d')

# 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
auth = oss2.Auth('LTAI4FteGCWYNVPas61vx5sJ', 'bUqjfENA2RVu1U7U61JMzl278hDP5T')
# Endpoint以杭州为例，其它Region请按实际情况填写。
bucket = oss2.Bucket(
    auth, 'http://oss-cn-hangzhou.aliyuncs.com', 'xdfacebackup')


def print_run_time(func):
    def wrapper(*args, **kw):
        local_time = time.time()
        func(*args, **kw)
        print('[+] [%s] Run time is %.2f' %
              (func.__name__, time.time() - local_time))
    return wrapper


def display():
    # oss2.ObjectIteratorr用于遍历文件。
    print("目录列表:\n")
    for b in islice(oss2.ObjectIterator(bucket), 10):
        print(b.key)

# 上传文件
@print_run_time
def upload(file):
    s = bucket.put_object_from_file(file, file)
    if s.status == 200:
        print("""[+] "%s" 上传成功 """ % file)
    else:
        print("""[+] "%s" 上传失败! %d""" % (file, s.status))


if __name__ == '__main__':
    file = 'run.md5'

    upload(file)  # 上传文件
    display()  # 打印目录文件
