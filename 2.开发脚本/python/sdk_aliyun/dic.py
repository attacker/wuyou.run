#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-07-27 16:24:53
@LastEditTime: 2020-07-27 16:30:41
@Description: 
'''
import json


# [5.03, 'r-bp16du736e8ig50e8a']


item = [{'{#ALIYUN_instanceId}': "r-bp16du736e8ig50e8a", '{#ALIYUN_metric}': 5.03}]
print(json.dumps({'data': item}, sort_keys=True,
                 indent=4, separators=(',', ':')))
