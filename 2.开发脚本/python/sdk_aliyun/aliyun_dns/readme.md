# 动态 ddns 解析脚本

基于阿里云域名 sdk 动态更新解析

## 环境

```bash
pip install -i http://mirrors.aliyun.com/pypi/simple   --trusted-host mirrors.aliyun.com aliyun-python-sdk-core-v3 aliyun-python-sdk-alidns
# 安装模块
```

## 配置文件

ddns.conf

```etc
subdomain=自定义域名     (未解析时程序会自动添加一条A记录)

AccessKeyId=阿里云密钥id
AccessKeySecret=xxxxxx
```

## 执行方法

```bash
python3  ddns.py
```

计划任务:

crontab -e

```bash
*/30 * * * *  cd /opt/aliyun_ddns  && python3  ddns.py >> record.log 2>&1
```

返回结果:

```log
获取ip的接口地址: https://attacker.club/getip
2020-02-19 20:07:07 [183.156.1.1] 解析已存在
```
