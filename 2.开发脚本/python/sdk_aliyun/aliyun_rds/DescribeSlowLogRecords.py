'''
Author: admin@attacker.club
Date: 2022-08-02 14:01:40
LastEditTime: 2022-08-02 15:10:37
Description: 
pip install aliyun-python-sdk-rds==2.7.0
'''
#!/usr/bin/env python
#coding=utf-8

from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential
from aliyunsdkrds.request.v20140815.DescribeSlowLogRecordsRequest import DescribeSlowLogRecordsRequest

def toDO(accesskey,secret,region,InstanceId,StartTime,EndTime):
    credentials = AccessKeyCredential(accesskey,secret)
    # use STS Token
    # credentials = StsTokenCredential('<your-access-key-id>', '<your-access-key-secret>', '<your-sts-token>')
    client = AcsClient(region_id=region, credential=credentials)
    request = DescribeSlowLogRecordsRequest()
    request.set_accept_format('json')
    request.set_DBInstanceId(InstanceId)
    request.set_StartTime(StartTime)
    request.set_EndTime(EndTime)
    # request.set_DBName(DBName)
    request.set_PageNumber(5)
    request.set_PageSize(50)
    
    response = client.do_action_with_exception(request)
    
    # python2:  print(response) 
    print(str(response, encoding='utf-8'))
