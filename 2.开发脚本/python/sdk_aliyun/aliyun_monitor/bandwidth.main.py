#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-04-27 21:54:32
@LastEditTime: 2020-05-06 11:49:24
@Description: 

# 流入bit/s: python bandwidth.main.py  net_rx.rate
# 流出bit/s: python bandwidth.main.py  net_tx.rate

'''


from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcms.request.v20190101.DescribeMetricListRequest import DescribeMetricListRequest
import time
import sys
import json


class aliyun_monitor(object):
    def __init__(self, metric):
        self.metric = metric

    def query(self):
        client = AcsClient('LTAI4G4YqGxxxx',
                           'P61RV8JUb14xxxxx', 'cn-hangzhou')

        request = DescribeMetricListRequest()
        request.set_accept_format('json')
        request.set_Period("60")

        # 命名空间
        request.set_Namespace("acs_bandwidth_package")
        # 监控指标
        request.set_MetricName(self.metric)
        request.set_Dimensions(
            {'instanceId': "cbwp-bp1xxxxx"})

        # response
        response = client.do_action_with_exception(request)

        # json结果处理
        datadict = json.loads(response)

        res_str = datadict['Datapoints']
        data = eval(res_str)
        # print(data)
        print(data[-1]['Value'])


if __name__ == '__main__':
    if len(sys.argv) > 1:
        metric = sys.argv[1]
        Monitor = aliyun_monitor(metric)
        Monitor.query()
