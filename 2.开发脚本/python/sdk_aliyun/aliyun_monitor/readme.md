# 阿里云云监控

基于阿里云云监控 sdk 使用

### 依赖包

```bash
pip install aliyun-python-sdk-core
# SDK核心库
pip install aliyun-python-sdk-cms
# 云监控
```

### 例子

```例子
python slb_main.py  TrafficRXNew
# SLB流入带宽

python bandwidth.main.py  net_rx.rate
# 共享带宽流入bit/s
```

### zabbix 监控项

```text
类型: agent主动模式
键值: slb_http["TrafficTXNew"]
更新间隔: 60s
单位：B
```

### slb

```bash
cat /usr/local/zabbix-agent/etc/zabbix_agentd.conf.d/aliyunsdk.key
# zabbix 配置
```

```bash
UserParameter=slb_http[*],/usr/local/bin/python3.7 /usr/local/zabbix-agent/scripts/slb_80_main.py $1
# 调用外部脚本提取key
```
