#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-04-27 21:54:32
@LastEditTime: 2020-05-06 11:49:32
@Description: 

# 流入bit/s: python slb_main.py  TrafficRXNew
# 流出bit/s: python slb_main.py  TrafficTXNew

'''

from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcms.request.v20190101.DescribeMetricListRequest import DescribeMetricListRequest
import time
import sys
import json


class aliyun_monitor(object):
    def __init__(self, metric):
        self.metric = metric

    def query(self):
        client = AcsClient('LTAI4G4Yxxxxxx',
                           'P61RV8JUb1xxxxxx', 'cn-hangzhou')

        request = DescribeMetricListRequest()
        request.set_accept_format('json')
        # # 只获取1小时内的数据
        # start_time = time.strftime(
        #     '%Y-%m-%d %H:%M:%S', time.localtime(time.time()-3600))

        # timestamp_start = int(time.mktime(time.strptime(
        #     start_time, "%Y-%m-%d %H:%M:%S"))) * 1000

        # request.set_StartTime(timestamp_start)
        request.set_Period("60")

        # 命名空间
        request.set_Namespace("acs_slb_dashboard")
        # 监控指标(端口每秒流入数据量)
        request.set_MetricName(self.metric)
        request.set_Dimensions(
            {'instanceId': "lb-bp12h0dmdxxx", 'port': '443', 'vip': "47.99.2.xx"})

        # response
        response = client.do_action_with_exception(request)

        # json结果处理
        datadict = json.loads(response)

        res_str = datadict['Datapoints']
        data = eval(res_str)
        # print(data)
        print(data[-1]['Maximum'])


if __name__ == '__main__':
    if len(sys.argv) > 1:
        metric = sys.argv[1]
        Monitor = aliyun_monitor(metric)
        Monitor.query()
