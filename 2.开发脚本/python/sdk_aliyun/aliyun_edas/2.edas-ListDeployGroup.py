'''
Author: admin@attacker.club
Date: 2022-07-19 15:14:12
LastEditTime: 2022-07-19 15:14:32
Description: 
'''
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential

credentials = AccessKeyCredential('<your-access-key-id>', '<your-access-key-secret>')
# use STS Token
# credentials = StsTokenCredential('<your-access-key-id>', '<your-access-key-secret>', '<your-sts-token>')
client = AcsClient(region_id='cn-shanghai', credential=credentials)

request = CommonRequest()
request.set_accept_format('json')
request.set_method('POST')
request.set_protocol_type('https') # https | http
request.set_domain('edas.cn-shanghai.aliyuncs.com')
request.set_version('2017-08-01')

request.add_query_param('AppId', "1739937b-9228-490c-9d55-0791cbf07b0b")
request.add_header('Content-Type', 'application/json')
request.set_uri_pattern('/pop/v5/app/deploy_group_list')


response = client.do_action_with_exception(request)

# python2:  print(response) 
print(str(response, encoding = 'utf-8'))