'''
Author: admin@attacker.club
Date: 2022-07-19 15:07:32
LastEditTime: 2022-08-10 15:37:11
Description: 
'''

# aliyun-python-sdk-edas

import sys
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential


request = CommonRequest()
request.set_accept_format('json')
request.set_method('PUT')
request.set_protocol_type('https') # https | http
request.set_domain('edas.cn-shanghai.aliyuncs.com')
request.set_version('2017-08-01')

request.add_query_param('ScalingRuleEnable', "true")
request.add_query_param('ScalingRuleName', "edas_template_cpu")
request.add_query_param('AppId', "78438d35-505b-4b62-9b05-2b157041ae21")
# request.add_query_param('GroupId',"mita-frontend-h5-pre-group-1-3-84c68668fb-mcc7t")
request.add_query_param('InInstanceNum', "2")
request.add_query_param('OutInstanceNum', "4")

# request.add_query_param('ScalingRuleEnable', "true")
request.add_header('Content-Type', 'application/json')
request.set_uri_pattern('/pop/v1/eam/scale/application_scaling_rule')


if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]
    credentials = AccessKeyCredential(id, secret)
    client = AcsClient(region_id='cn-shanghai', credential=credentials)
    response = client.do_action_with_exception(request)
    
    # python2:  print(response) 
    print(str(response, encoding = 'utf-8'))