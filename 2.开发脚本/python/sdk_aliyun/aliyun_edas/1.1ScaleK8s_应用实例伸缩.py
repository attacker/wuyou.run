'''
Author: admin@attacker.club
Date: 2022-08-10 14:50:11
LastEditTime: 2022-08-10 15:32:49
Description: 
'''
#!/usr/bin/env python
#coding=utf-8
import sys

from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential

# credentials = AccessKeyCredential('<your-access-key-id>', '<your-access-key-secret>')
# # use STS Token
# # credentials = StsTokenCredential('<your-access-key-id>', '<your-access-key-secret>', '<your-sts-token>')
# client = AcsClient(region_id='cn-shanghai', credential=credentials)

request = CommonRequest()
request.set_accept_format('json')
request.set_method('PUT')
request.set_protocol_type('https') # https | http
request.set_domain('edas.cn-shanghai.aliyuncs.com')
request.set_version('2017-08-01')


request.add_query_param('AppId', "78438d35-505b-4b62-9b05-2b157041ae21")
request.add_query_param('Replicas', "1")
request.add_header('Content-Type', 'application/json')
request.set_uri_pattern('/pop/v5/k8s/acs/k8s_apps')



if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]
    credentials = AccessKeyCredential(id, secret)
    client = AcsClient(region_id='cn-shanghai', credential=credentials)
    response = client.do_action_with_exception(request)
    # python2:  print(response) 
    print(str(response, encoding = 'utf-8'))