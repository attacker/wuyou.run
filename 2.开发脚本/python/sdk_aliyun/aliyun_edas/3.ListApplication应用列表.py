'''
Author: admin@attacker.club
Date: 2022-07-19 15:07:32
LastEditTime: 2022-08-12 11:46:15
Description: 
'''

# aliyun-python-sdk-edas


#!/usr/bin/env python
#coding=utf-8

import sys
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential



request = CommonRequest()
request.set_accept_format('json')
request.set_method('POST')
request.set_protocol_type('https') # https | http
request.set_domain('edas.cn-shanghai.aliyuncs.com')
request.set_version('2017-08-01')

request.add_query_param('LogicalRegionId', "cn-shanghai:pre")
request.set_uri_pattern('/pop/v5/app/app_list')
request.add_header('Content-Type', 'application/json')


if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]
    credentials = AccessKeyCredential(id, secret)
    client = AcsClient(region_id='cn-shanghai', credential=credentials)
    response = client.do_action_with_exception(request)
    
    # python2:  print(response) 
    print(str(response, encoding = 'utf-8'))