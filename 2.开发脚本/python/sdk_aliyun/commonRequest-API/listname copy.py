'''
Author: admin@attacker.club
Date: 2022-09-01 21:22:28
LastEditTime: 2022-09-02 23:39:42
Description: 
python xx.py id secret clusterId
'''
import sys
import json
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential



request = CommonRequest()
request.set_accept_format('json')
request.set_method('GET')
request.set_protocol_type('https') # https | http
request.set_domain('cs.cn-shanghai.aliyuncs.com')
request.set_version('2015-12-15')

request.add_query_param('Namespace', "redwar")
request.add_query_param('Name', "battle-websocket")
request.add_header('Content-Type', 'application/json')
# request.set_uri_pattern('/triggers/c5b053186d5f44aeaa7f53bdc62bf2375')
request.set_uri_pattern('/clusters/c5b053186d5f44aeaa7f53bdc62bf2375/triggers')

if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]

    credentials = AccessKeyCredential(id, secret)
    client = AcsClient(region_id='cn-shanghai', credential=credentials)

    response = client.do_action_with_exception(request)
    s = response
    print(s,type(s))
    # jsonObj = json.loads(response.decode("UTF-8"))
    # print(type(jsonObj))
    # print(jsonObj)


