'''
Author: admin@attacker.club
Date: 2022-09-01 21:22:28
LastEditTime: 2022-09-02 23:45:59
Description: 
python xx.py id secret clusterId
'''
import sys
import json
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest


request = CommonRequest()
request.set_domain('cs.aliyuncs.com')
request.set_version('2015-12-15')
# request.set_uri_pattern('/clusters')


if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]
    ClusterId = sys.argv[3]
    request.add_query_param('Namespace', "redwar")
    request.add_query_param('Name', "battle-websocket")
    request.add_header('Content-Type', 'application/json')
    request.set_uri_pattern(f'/clusters/{ClusterId}/triggers')
    client = AcsClient(id , secret , region_id='cn-shanghai')
    response = client.do_action_with_exception(request)
    s = response.decode("UTF-8")
    print(s,type(s))
    # jsonObj = json.loads(response.decode("UTF-8"))
    # print(type(jsonObj))
    # print(jsonObj)


