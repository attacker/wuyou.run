'''
Author: admin@attacker.club
Date: 2022-09-01 21:04:51
LastEditTime: 2022-09-01 21:12:47
Description: 
'''
#!/usr/bin/env python
#coding=utf-8

import sys
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential


request = CommonRequest()
request.set_accept_format('json')
request.set_method('GET')
request.set_protocol_type('https') # https | http
request.set_domain('cs.cn-shanghai.aliyuncs.com')
request.set_version('2015-12-15')

request.add_header('Content-Type', 'application/json')




if __name__ == "__main__":
    id =  sys.argv[1]
    secret = sys.argv[2]
    ClusterId = sys.argv[3]

    request.set_uri_pattern(f'/clusters/{ClusterId}')
    credentials = AccessKeyCredential(id, secret)
    client = AcsClient(region_id='cn-shanghai', credential=credentials)
    response = client.do_action_with_exception(request)

    # python2:  print(response) 
    print(str(response, encoding = 'utf-8'))

