'''
Author: admin@attacker.club
Date: 2022-07-25 13:34:23
LastEditTime: 2022-07-25 13:37:36
Description: 
'''
import requests
import shutil

def download_file(url):
    local_filename = url.split('/')[-1]
    with requests.get(url, stream=True) as r:
        with open(local_filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

    return local_filename
    

download_file('https://www.python.org/ftp/python/3.6.15/Python-3.6.15.tar.xz')