'''
Author: admin@attacker.club
Date: 2023-02-20 09:34:29
LastEditTime: 2023-02-20 09:44:06
Description: 
'''
import requests

# http://test:123456@proxy.smartproxycn.com:1000
proxyip = "http://10.10.10.54:1088"
url = "http://myip.ipip.net"
proxies={
    'http':proxyip,
    'https':proxyip,
}
data = requests.get(url=url,proxies=proxies)
print(data.text)