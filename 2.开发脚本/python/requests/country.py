'''
Author: admin@attacker.club
Date: 2023-02-17 10:51:30
LastEditTime: 2023-02-17 11:14:15
Description: 
'''
import requests
import json


url = "https://ipinfo.io"
status = 1
try:
    res = requests.get(url)
except Exception as e:
    status = 2
    print(f"请求异常: {e}")

else:
    if res.status_code == 200 and  res.json()['country'] == "HK":
        print(f"线路正常")
    else:
        status = 2
finally: 
    if status> 1:
        print("异常通知")
        sms = WXWork_SMS(key,title, data)
        sms.send_msg()
