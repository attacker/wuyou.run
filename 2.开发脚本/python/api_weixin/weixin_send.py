'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-20 18:08:05
LastEditTime: 2022-07-15 00:14:19
Description: 
'''
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests, sys, json
requests.packages.urllib3.disable_warnings()

class VxSend():
    def __init__(self, ):
        self.token_url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"

    def Gettoken(self, Corpid, Secret):
        Data = {
            "corpid": Corpid,
            "corpsecret": Secret
        }
        r = requests.get(url=self.token_url, params=Data, verify=False)
        Token = r.json()['access_token']
        self.token = Token

    def SendMessage(self, User, Agentid, Subject, Content):
        Url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s" % self.token
        Data = {
            "touser": User,  # 企业号中的用户帐号，在zabbix用户Media中配置，如果配置不正常，将按部门发送。
            "msgtype": "text",  # 消息类型。
            "agentid": Agentid,  # 企业号中的应用id。
            "text": {
                "content": Subject + '\n' + Content
            },
            "safe": "0"
        }
        r = requests.post(url=Url, data=json.dumps(Data), verify=False)
        return r.text

if __name__ == '__main__':
    User = sys.argv[1]  # 外部传来的第一个参数
    Subject = sys.argv[2]  # 外部传来的第二个参数
    Content = sys.argv[3]  # 外部传来的第三个参数

    Corpid = "wwe53bb44xxx"  # CorpID是企业号的标识
    Secret = "ljB556jd8p_F8kZMM58uzxxx"  # Secret是管理组凭证密钥
    # Tagid = "1"                         # 通讯录标签ID
    Agentid = "1000002"

    vx = VxSend()
    vx.Gettoken(Corpid, Secret)  # 认证获取token信息
    Status = vx.SendMessage(User, Agentid, Subject, Content)
    print(Status)
