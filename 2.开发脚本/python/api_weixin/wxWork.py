'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-22 16:14:11
LastEditTime: 2022-07-15 00:11:54
Description: 
python wxWork.py  6cbef92exxxx
'''
'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-22 16:14:11
LastEditTime: 2022-07-15 00:09:09
Description: 
python wxWork.py  6cbef92exxxx
'''

import sys
import json
import requests
requests.packages.urllib3.disable_warnings()


class WXWork_SMS(object) :
    def __init__(self,data,key):
        self.data = data
        self.key = key
        self.headers = {"Content-Type" : "text/plain"}
        self.url = f"https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={self.key}"

    def send_upload(self,file) :
        tries = 5
        while tries > 0:
            try:
                id_url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/upload_media?key={}&type=file'.format(self.key)  # 上传文件接口地址
                data = {'file': open(file, 'rb')}  # post jason
                response = requests.post(url=id_url, files=data,verify=False)  # post 请求上传文件
                json_res = response.json()  # 返回转为json
                media_id = json_res['media_id']  # 提取返回ID
                data = {
                    "msgtype": "file",
                    "file": {
                        "media_id": media_id
                        }
                }
                req = requests.post(url=self.url, headers = self.headers,json=data,verify=False)
                print('msg_upload:',req.text)
 
                break
            except Exception as e:
                tries -= 1
                print(e)


    ## Markdown类型消息
    def send_msg_markdown(self) :
        send_data = {
            "msgtype": "markdown",  # 消息类型，此时固定为markdown
            "markdown": {
                "content": self.data
            }
        }
 
        req = requests.post(url = self.url, headers = self.headers, json = send_data)
        print('msg_markdown:',req.text)
 
if __name__ == '__main__' :
    key = sys.argv[1]  #  传入秘钥地址
    data = '''
    ## **提醒! 触发预警规则**
    #### **请相关同事注意,及时跟进 !**
    >类型：<font color="info">个推容量预警</font>
    >触发阈值：<font color="warning">10%</font>
    '''
    sms = WXWork_SMS(data,key)
    sms.send_msg_markdown()
    sms.send_upload('readme.md')