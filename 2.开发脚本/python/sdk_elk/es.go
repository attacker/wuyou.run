package main

import (
	"flag"
	"fmt"
	elastic "github.com/olivere/elastic/v7"
	"time"
)

func main() {
	// 定义命令行参数
	timeArg := flag.String("time", "1h", "Relative time (default: 1h)")
	indexArg := flag.String("index", "", "Index name")
	queryArg := flag.String("query", "", "Query string")
	// 解析命令行参数
	flag.Parse()
	// 计算时间范围（相对时间）
	endTime := time.Now()
	startTime, _ := time.ParseDuration("-" + *timeArg)
	startTime = endTime.Add(startTime)
	// 创建 Elasticsearch 实例，并执行查询操作，返回结果集合（默认最多返回 10 条记录）
	esClient, _ := elastic.NewSimpleClient(elastic.SetURL("http://pro-lb-es-1.astralsec.com:9200"))
	resultSet, _ := esClient.Search().
		Index(*indexArg).
		Query(elastic.NewMatchQuery("message", *queryArg)).
		Query(elastic.NewRangeQuery("@timestamp").Gte(startTime).Lte(endTime)).
		Size(10).
		Pretty(true).
		Result(esClient.Context())
	if resultSet != nil {
		// 输出结果集合到控制台中显示（每条记录使用换行符分隔）
		for _, hit := range resultSet.Hits.Hits {
			fmt.Println(hit.Source["message"])
			fmt.Println()
		}
	}
}
