#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# !/usr/bin/env python
# coding:utf-8
# zabbix钉钉报警
import requests, json, sys, datetime


class SendMessage():
    def __init__(self, user, text):
        self.user = user
        self.text = text
        self.headers = {'Content-Type': 'application/json'}
        self.webhook = "https://oapi.dingtalk.com/robot/send?access_token=d0692a0f383fc9ebdb93597565e47a5b"

    def post(self):
        post_data = {
            "msgtype": "text",
            "text": {
                "content": self.text
            },
            "at": {
                "atMobiles": [
                    self.user
                ],
                "isAtAll": False
            }
        }
        results = requests.post(url=self.webhook, data=json.dumps(post_data), headers=self.headers)
        return results


if __name__ == "__main__":
    user = sys.argv[1]
    text = sys.argv[2]
    sender = SendMessage(user, text)

    s = sender.post()
    if s.json()["errcode"] == 0:
        print("\n" + str(datetime.datetime.now()) + "\t" + str(user) + "\t" + "发送成功" + "\n" + str(text))
    else:
        print("\n" + str(datetime.datetime.now()) + "\t" + str(user) + "\t" + "发送失败" + "\n" + str(text))
