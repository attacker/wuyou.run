# -*- coding: utf-8 -*-
# !/usr/bin/env python
# coding:utf-8
# 钉钉报警:https://developers.dingtalk.com/document/app/custom-robot-access
# python3 dingtalk_test.py    "[dn0] job: DDN0.PublicPlatform.vue.uptech-home-page" a8d00522eb6a2266f



import requests, json, sys, datetime


class SendMessage():
    def __init__(self,  text,token):
        self.text = text
        self.headers = {'Content-Type': 'application/json'}
        self.webhook = "https://oapi.dingtalk.com/robot/send?access_token={}".format(token)

    def post(self):
        post_data = {
            "msgtype": "markdown",
            "markdown": {
                "title":"Jenkins发布任务",
                "text": "#### Jenkins发布任务\n {}".format(self.text)
             },
            "at": {
                # "atMobiles": [
                #     self.user
                # ],
                "isAtAll": True # 是否@所有人。
            }
        }
        results = requests.post(url=self.webhook, data=json.dumps(post_data), headers=self.headers)
        return results


if __name__ == "__main__":
    text = sys.argv[1]
    token = sys.argv[2] 
    sender = SendMessage(text,token)
     
    s = sender.post()
    if s.json()["errcode"] == 0:
        print("\n" + str(datetime.datetime.now()) + "\t" + "\t" + "发送成功" + "\n" + str(text))
    else:
        print("\n" + str(datetime.datetime.now()) + "\t"  + "\t" + "发送失败" + "\n" + str(text))
