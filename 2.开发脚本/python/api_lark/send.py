'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-22 16:14:11
LastEditTime: 2023-02-17 11:18:08
Description: 
python send.py  6cbef92exxxx  "自动预警"  "数据xxx"
'''


import sys
import json
import requests
requests.packages.urllib3.disable_warnings()


class Webhook_SMS(object) :
    def __init__(self,key,title,data):
        self.title = title
        self.data = data
        self.key = key
        self.headers = {'Content-Type': 'application/json'}
        self.url = f"https://open.larksuite.com/open-apis/bot/v2/hook/{self.key}"


    def send_msg(self) :
        # alert_headers: 告警消息标题
        alert_headers=self.title
        # alert_content: 告警消息内容，用户可根据自身业务内容，定义告警内容
        alert_content="告警内容,请尽快查阅 🚀\n"
        # message_body: 请求信息主体
        data={
            "msg_type": "interactive",
            "card": {
            "config": {
                "wide_screen_mode": True
            },
            "elements": [
                {
                "tag": "markdown",
                "content":f"{alert_content}{self.data}",
                }
            ],
            "header": {
                "template": "red",
                "title": {
                "content":alert_headers,
                "tag": "plain_text"
                }
            },

            
        }}
        req = requests.post(url = self.url, headers = self.headers,  data=json.dumps(data))
        print('msg_markdown:',req.text)
 
if __name__ == '__main__' :
    
    key = sys.argv[1]  #  秘钥
    title = sys.argv[2]  #  标题
    args = sys.argv[3]  #  标题

    data = f'''
    \n**提醒! 触发预警规则**: <font color='red'> {args}</font>
    '''
    sms = Webhook_SMS(key,title, data)
    sms.send_msg()