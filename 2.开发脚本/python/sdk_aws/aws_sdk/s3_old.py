#!/usr/bin/env python
# coding=utf-8
'''
Author: 以谁为师
Website: attacker.club
Date: 2020-08-11 11:39:49
LastEditTime: 2023-01-11 12:29:40
Description: 
'''
import boto3
from botocore.client import Config

# Initialize a session using DigitalOcean Spaces.
session = boto3.session.Session()
client = session.client('s3',
                        region_name='sfo2',
                        endpoint_url='https://sfo2.digitaloceanspaces.com',
                        aws_access_key_id='XWNPFAZ4HBDS6GHIDSE3',
                        aws_secret_access_key='xxxxxxxxxxxxxxxxxxxxxx'
                        )


# List all buckets on your account.
response = client.list_buckets()
spaces = [space['Name'] for space in response['Buckets']]
BUCKETNAME = spaces[0]
print(BUCKETNAME)

# 上传
client.upload_file('file.txt', BUCKETNAME, "test/1.txt")
client.put_object_acl(
    ACL='public-read', Bucket=BUCKETNAME, Key="test/1.txt")
