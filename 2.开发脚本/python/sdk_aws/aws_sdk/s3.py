'''
Author: Logan.Li
Gitee: https://gitee.com/attacker
email: admin@attacker.club
Date: 2023-07-19 20:41:04
LastEditTime: 2023-07-19 20:41:09
Description: 
'''
import boto3
import argparse

def upload_file_to_s3(file_path, bucket_name, access_key, secret_key):
    # 创建 S3 客户端
    s3_client = boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_key)

    # 上传文件到 S3
    try:
        file_name = file_path.split("/")[-1]  # 获取文件名
        s3_client.upload_file(file_path, bucket_name, file_name)
        print(f"文件上传成功到桶 {bucket_name}，文件名：{file_name}")
    except Exception as e:
        print(f"文件上传失败：{e}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="上传文件到 S3")
    parser.add_argument("--key", required=True, help="AWS Access Key ID")
    parser.add_argument("--secret", required=True, help="AWS Secret Access Key")
    parser.add_argument("--bucket", required=True, help="S3 Bucket 名称")
    parser.add_argument("--file", required=True, help="要上传的文件路径")
    args = parser.parse_args()

    # 获取命令行参数
    access_key = args.key
    secret_key = args.secret
    bucket_name = args.bucket
    file_path = args.file

    # 调用上传函数
    upload_file_to_s3(file_path, bucket_name, access_key, secret_key)
