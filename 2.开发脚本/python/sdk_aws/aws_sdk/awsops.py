'''
Author: admin@attacker.club
Date: 2022-11-02 16:23:17
LastEditTime: 2023-03-15 17:31:45
Description: 
'''
#!/usr/bin/env python3
#coding=utf-8
'''
功能： 操作aws 云
'''
import boto3
from configs import AWS_CONFIG


class AwsOps:
    '''
    功能: aws 云操作基类
    '''

    def __init__(self):
        '''
        功能：利用sdk 操作 aws 资源,需要根据api开通权限
        :param
        :param
        '''
        self._user = None
        self._key = None
        self._region = None
        self._service_name = None
        self._max_query_num = 1000
        self._type = "aws"
        self._account = None
        self._maker = None

    @property
    def account(self):
        return self._account

    @account.setter
    def account(self, value):
        self._account = value
        if value in AWS_CONFIG:
            self._user = AWS_CONFIG[value].get("id")
            self._key = AWS_CONFIG[value].get("key")
        else:
            raise ValueError("aws 账号不存在 configs文件中，请查看配置")

    @property
    def service_name(self):
        return self._service_name

    @service_name.setter
    def service_name(self, value):
        self._service_name = value

    @property
    def region(self):
        return self._region

    @region.setter
    def region(self, value):
        self._region = value

    def get_client(self):
        '''
        功能： 通过boto3 来获取client 连接实例
        :return:  boto3 client
        '''
        client = None
        if self._region:
            client = boto3.client(service_name=self._service_name, aws_access_key_id=self._user,
                                  aws_secret_access_key=self._key,
                                  region_name=self._region)
        else:
            client = boto3.client(service_name=self._service_name, aws_access_key_id=self._user,
                                  aws_secret_access_key=self._key)
        return client
