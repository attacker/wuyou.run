'''
Author: admin@attacker.club
Date: 2023-03-15 17:30:45
LastEditTime: 2023-03-16 17:17:00
Description: 
'''


import boto3
import sys


class CloudApi:
    def __init__(self, secret_id, secret_key):
        self.secret_id = secret_id
        self.secret_key = secret_key
        self.cli = aws.get_client()

    def get_client(self):
        '''
        功能： 通过boto3 来获取client 连接实例
        :return:  boto3 client
        '''
        client = None
        if self._region:
            client = boto3.client(service_name=self._service_name, aws_access_key_id=self._user,
                                  aws_secret_access_key=self._key,
                                  region_name=self._region)
        else:
            client = boto3.client(service_name=self._service_name, aws_access_key_id=self._user,
                                  aws_secret_access_key=self._key)
        return client


if __name__ == '__main__':
    secret_id = sys.argv[1]
    secret_key = sys.argv[2]
    region = sys.argv[3]  # me-east-1

    cloud = CloudApi(secret_id, secret_key)