

grep "\<abc\>" file
grep –w "abc" file
#精确匹配内容

grep PS /etc/profile || echo '''PS1="\[\e[37;1m\][\[\e[32;1m\]\u\[\e[37;40m\]@\[\e[34;1m\]\h \[\e[0m\]\t \[\e[35;1m\]\W\[\e[37;1m\]]\[\e[m\]/\\$" ''' >>/etc/profile
#如果grep没有过滤到含'PS'的行,追加新内容到profile文件；这里使用||逻辑或判断


grep 多条件匹配

1. 同时满足多个条件：

fdisk -l |grep D|grep dev
套用两次grep过滤，查看物理硬盘

2. 匹配任意条件

ethtool eno16777736 |egrep 'Speed|Duplex'
#egrep增强命令,查看eno16777736网卡(物理机) 速度和双工模式
#索内容

1. 字符串内容

grep -r @copyright|grep  index
# r参数归档目录下所有文件，查找包含copyright并且是index文件名的文件
2. 数字内容

cat /proc/meminfo |awk  'NR==1'|grep  -o '[0-9]\{1,\}'
#o参数显示匹配的内容，数字0-9范围，如果{1,99\} 1行99位；查看内存大小
3. 只列出文件

grep -rl  localhost
#搜索网站连接数据库的文件并只列出文件名

2.统计
grep -c   Failed  /var/log/secure
#匹配数目