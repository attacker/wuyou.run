

## -c 参数取指定位字符

history |uniq|cut -c 8-999
#截取8到999位的内容

cat /etc/passwd|awk 'NR==1' |cut -c 1,8,10,15,22-
#过滤passwd密码第一行，截取指定位置，22- 为22位以后所有字符，-10为10位之前

效果：
r00t:/bin/bash

## -d 参数分隔符 与 - f 同时使用

history |uniq|cut -d " " -f 5,7-99
# 通过空格分隔，-f 指定要显示的列;awk 隔符为F注意不要混淆