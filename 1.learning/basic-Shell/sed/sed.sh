## 行内操作

### 删除
fdisk -l |cut -d " " -f 2-4 |grep dev|sed s/,//
#查看磁盘大小;将','逗号删除
替换TOC

sed 's/root/admin/g' file
#将root替换为admin;类似vim参数s替换g全局匹配


### 前后插入字符
sed -ne 's/aaa/HELLO&/p' test 
#在aaa字符前面插入内容;输出结果：HELLOaaa
sed -ne 's/aaa/&HELLO/p' test
#在aaa字符后面插入内容;输出结果：aaaHELLO

sed 's/^/HEAD&/g' file
#在每行的头添加字符，比如"HEAD"
sed 's/$/&TAIL/g' file
#在每行的行尾添加字符，比如“TAIL”


###换行、空格TOC
nl /etc/passwd |sed '10a 1第十行后面开始插入三行\n2\n3 斜杠n是换行\tt大空格'
#换行\n 空格\t,空格键小空格

## 整行操作

### 搜索显示TOC
nl /etc/passwd | sed -n '2p'
#打印第二行,类似于awk NR==2
nl /etc/passwd | sed -n '5,7p'
#打印5-7行
sed -n  '/root/p' /etc/passwd
#只显示包含root的行;参数-n只打印处理的行 
sed   '/nologin/d' /etc/passwd
#删除包含nologin的行，其他输出;d 参数删除

###删除行
sed -i '8d' file
#删除第8行
nl /etc/passwd | sed '2d' 
#只要删除第2行
nl /etc/passwd | sed '2,5d'
#删除2-5行
nl /etc/passwd | sed '3,$d' 
#要删除第3行到最后一行

sed /PATTERN/d file
sed -i  '/ulimit/d' /etc/rc.local
#删除包含关键字的行 

### 插入行：通过行号插入
nl /etc/passwd | sed '2,5c 2-5行被吃了'
#2-5行替换成指定的内容
nl /etc/passwd | sed "2i it's second line"
#第二行前面插入内容；参数i
nl /etc/passwd | sed '2a  The third line'
#第二行下面插入内容；参数a

### 插入行：匹配关键字前后插入
sed  -i  "/rm/i\alias vi='vim'"  ~/.bashrc
#在匹配的rm内容上面插入一条vim配置别名的行
grep vi ~/.bashrc || sed  -i  "/mv/a\alias vi='vim'"  ~/.bashrc
#先判断vi内容是否存在，如果不存在则匹配到mv内容在下面插入一行；

### 插入行： 行首、行某插入
sed '1istart'   /root/.bashrc
#首行添加start字符串
sed '$a end.'   /root/.bashrc
#结尾添加end.内容

### 其他高级用法
sed 's/#.*//;/^$/ d' /etc/ssh/ssh_config
#去掉空行和注释；替换#.*用空并将^$空格打头的内容删除;类似用法：egrep -v '^#|^$' /etc/ssh/ssh_config

sed -i "/HOSTNAME/c HOSTNAME=OS" /etc/sysconfig/network
#搜索关键字，取代该行
sed -i  '/HOSTNAME/d;a HOSTNAME=TemplateOS' /etc/sysconfig/network
#删除包含hostname的行并重新新建 