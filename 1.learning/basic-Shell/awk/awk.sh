
### 显示指定行
cat /proc/meminfo |awk  'NR==1'
#显示第一行

### 分隔显示
cat /proc/meminfo |awk  'NR==1'|awk '{print $2}'
#显示第二列

curl -s --head "blog.attacker.club"|awk  '/HTTP/ {print $2}'
#过滤关键字‘HTTP’的行并将第二列内容打印出来

### 正则
awk  -F= '/^DEV/ {print "网卡"$2}' /etc/sysconfig/network-scripts/ifcfg-eth0 
#正则搜索DEV大头的行，打印第二列网卡名
route  -n | awk '$3~/252.0$/{print $1}'|uniq
#正则匹配第三列掩码是252.0则打印第一列网络地址:网卡 "eth0"

### 高级玩法
awk -F: '$3>=1000 {print $1}' /etc/passwd
#第三列值大于等于1000则打印passwd第一列的用户名
awk -F:  'length($3)==2 {print $1}' /etc/passwd
#第三列字符串是2位长度的，打印第一列用户名信息
#;查看是否存在空口令帐户
awk -F\: '{system("passwd -S "$1)}' /etc/passwd|awk '{print $1,$3}'
#用system和bash执行命令;查看账户创建日期
awk -F\: '{system("passwd -S "$1)}' /etc/passwd|awk '{print $1,$3}'
#同上
#过滤登录失败的ip地址，awk if如果第一列数字有8次以上则打印第二列ip信息
awk '$1> 8 {print $2}'
#同上，效果

grep Failed /var/log/secure |egrep -o '[0-9]{1,3}(\.[0-9]{1,3}){3}' |sort |uniq -c|sort -nr | awk '{if($1 > 8) print $2}' 
#[0-9]{1,3}(.[0-9]{1,3}){3}
[0-9]{1,3}：1-3 位数字
.[0-9]{1,3}：小数点. 后跟 1-3 位数字
(...){3}：前面括号中的组合重复 3 次

### 正则表达式中：
. 表示 “单个任意字符”
. 表示 “小数点”

### 关于 IP 地址, 再提供一种更精确的写法：
\d 表示 “单个任意数字”
((\d{1,3}).){3}(\d{1,3})：与你的式子基本等价
![.\d])：后面不能有. 或数字