#!/bin/bash
# --------------------------------------------------
#Author:  LJ
#Email:   admin@attacker.club

#Last Modified: 2018-03-18 12:30:59
#Description: 
# --------------------------------------------------






## -d 删除字符
echo "a12HJ13fdaADff" | tr -d "[a-z][A-Z]"
1213
echo "a1213fdasf" | tr -d [adfs]
1213

##-t 字符替换
echo "a1213fdasf" | tr -t [afd] [AFO]
A1213FOAsF
echo "Hello World I Love You" |tr -t [a-z] [A-Z]
HELLO WORLD I LOVE YOU

