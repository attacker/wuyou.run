#!/usr/bin/env python
# -*- coding: utf-8-*-
# --------------------------------------------------
#Author:		LJ
#Email:			admin@attacker.club
#Site:		    blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-09-13 23:28:48 Last:2017-09-13 23:28:48
#Description:   
# --------------------------------------------------






# # readlines()会将文本以列表形式装入内存；资源开销大
# with open('test.txt','r') as f:
#     for line in f.readlines():
#         print (line)
#     f.close()
#         # 打印所有行




# high loop  一次取一行；每次内存只保存一行(迭代器)
with open('test.txt','r') as f:

    for  line in f:
        print(line.strip())







""" low loop 先加载到内存在处理
for x,y in enumerate(data.readlines()):
    if x == 9:
        print '---------分割线第10行不打印---------'
        continue #跳过本次循环
    print y.strip()#去空行换行
"""


