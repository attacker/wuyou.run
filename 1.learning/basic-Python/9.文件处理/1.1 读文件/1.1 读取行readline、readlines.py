#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-03 18:10:26
Description:
'''

"""
blog.attacker.club 
"""

# reader.read()  读取整个文件，有时需要切片split('\n')
# reader.readline() 读取下一行
# reader.readlines() 读取整个文件，以list数据类型


##文件读取例子
reader = open("test.txt","r")
print  ("读取下一行",reader.readline())
print  ("读取下一行",reader.readline())
print  ("读取下一行",reader.readline())

print  ("读取下一行长度2",reader.readline(2))

print  ("读取全文第3行",reader.readlines(3))

reader.close() #关闭文件




"""
for line in reader.readlines():
    print line
    #读取单行
f.readline()  #读一行
f.readlines() #以行单位读取全文
"""
