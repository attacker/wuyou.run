# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description



f = open(file='test3.txt',mode='r',encoding='utf-8')
data = f.read()
f.close()

# 缩写
with open('test3.txt','r',encoding='') as f:
    data = f.read()
    f.close()
# encoding必须和文件在保存时设置的编码一致，不然“断句”会不准确从而造成乱码。
"""