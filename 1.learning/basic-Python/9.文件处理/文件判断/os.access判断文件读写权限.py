
import  os

## 判断文件是否可做读写操作
# os.access(path, mode)

"""
os.F_OK: 检查文件是否存在;
os.R_OK: 检查文件是否可读;
os.W_OK: 检查文件是否可以写入;
os.X_OK: 检查文件是否可以执行
"""

if os.access("/file/path/foo.txt", os.F_OK):
    print
    "Given file path is exist."

if os.access("/file/path/foo.txt", os.R_OK):
    print
    "File is accessible to read"

if os.access("/file/path/foo.txt", os.W_OK):
    print
    "File is accessible to write"

if os.access("/file/path/foo.txt", os.X_OK):
    print
    "File is accessible to execute"

