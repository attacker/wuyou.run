

"""
1.os模块;判断文件是否可做读写操作
2.使用Try语句
3.使用pathlib模块

"""

import os


## 判断文件、文件夹是否存在
# 判断文件是否
os.path.exists(test_file.txt)
#True

os.path.exists(no_exist_file.txt)
#False

# 判断文件夹
os.path.exists(test_dir)
#True

os.path.exists(no_exist_dir)
#False

# 只判断文件是否存在 (推荐)
os.path.isfile("test-data")



