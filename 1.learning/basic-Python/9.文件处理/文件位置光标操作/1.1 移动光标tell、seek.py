#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email: admin@attacker.club
#Date:  2017/9/14
#Description:
# --------------------------------------------------

with open('test2','w+',encoding='utf-8') as f:
    for num in range (5):
        f.write('%d\n'% num)
        print('当前光标位置',f.tell())


    f.seek(6) #返回第6字节位置
    print('返回位置',f.tell())
    print('当前行内容',f.readline())


    f.seek(1)  #返回光标开始
    print( [i for i in f.buffer]) # buffer数据


# read(1)取中文字符时gbk编码字符长度有问题，推荐utf-8