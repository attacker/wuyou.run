
with open('test.txt','w') as f:
    f.write('写入文件例子1')
    f.close()


with open('test2.txt','w',encoding='utf-8') as f:
    f.write('写入文件并指定编码') #此行python3是Unicode格式,可以encode
    f.close()


with open('test3.txt','wb') as f:
    f.write('二进制写入文件'.encode('gbk')) # 二进制写入指定编码
    f.close()