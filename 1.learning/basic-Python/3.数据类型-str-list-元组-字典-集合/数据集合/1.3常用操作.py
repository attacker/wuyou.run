#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email: admin@attacker.club
#Site:  blog.attacker.club
#Mail:  admin@attacker.club
#Date:  2017/9/12
#Description:   
# --------------------------------------------------

s = set([3,5,9,10])      #创建一个数值集合  

t = set("Hello")         #创建一个唯一字符的集合  



t.add('x')            # 添加一项  
  
s.update([10,37,42])  # 在s中添加多项  
  
   
  

  

  
"""
t.remove('H')  
#使用remove()可以删除一项
  
len(s)  
#set 的长度
  
x in s  
#测试 x 是否是 s 的成员
  
x not in s  
#测试 x 是否不是 s 的成员


s.issubset(t)  
s <= t  
测试是否 s 中的每一个元素都在 t 中  
  
s.issuperset(t)  
s >= t  
测试是否 t 中的每一个元素都在 s 中  
  
s.union(t)  
s | t  
返回一个新的 set 包含 s 和 t 中的每一个元素  
  
s.intersection(t)  
s & t  
返回一个新的 set 包含 s 和 t 中的公共元素  
  
s.difference(t)  
s - t  
返回一个新的 set 包含 s 中有但是 t 中没有的元素  
  
s.symmetric_difference(t)  
s ^ t  
返回一个新的 set 包含 s 和 t 中不重复的元素  
  
s.copy()  
返回 set “s”的一个浅复制
"""

