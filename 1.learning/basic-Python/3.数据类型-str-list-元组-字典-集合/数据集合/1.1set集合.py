#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email:			admin@attacker.club
#Site:		    blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-09-12 00:04:57 Last:2017-09-12 00:04:57
#Description:   
# --------------------------------------------------


list_1 = [1,3,4,6,3,5,7,9]
print '列表:',list_1,type(list_1)

list_1 = set(list_1)
print '集合:',list_1,type(list_1)


"""
集合是一个无序的，不重复的数据组合，它的主要作用如下：

1. 去重，把一个列表变成集合，就自动去重了
2. 关系测试，测试两组数据之前的交集、差集、并集等关系
"""