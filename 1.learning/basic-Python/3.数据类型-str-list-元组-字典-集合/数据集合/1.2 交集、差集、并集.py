#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email:			admin@attacker.club
#Site:		    blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-09-12 00:08:55 Last:2017-09-12 00:10:24
#Description:   
# --------------------------------------------------




list_1 = [1,3,4,6,3,5,7]
#列表
list_1 = set(list_1)
#转集合
list_2 = set([2,4,6,8,9])
print  ('交集',list_1.intersection(list_2))
#list1与list2都有的
print ('运算符&',list_1 & list_2)


print  ('对称差集',list_1.symmetric_difference(list_2))
#取两边不重复的
print ('运算符^',list_1 ^ list_2)


print  ('差集',list_1.difference(list_2))
#list1 有 list2没的
print ('运算符-',list_1 - list_2)


print  ('并集',list_1.union(list_2))
#list1和list2合集
print ('运算符|',list_1 | list_2)


##############################################

list_3 = set([3,4,5,6,7])
print '子集',list_3.issubset(list_1)
print '父集合',list_1.issuperset(list_3)
#父子集需要有包含，父要包含子

##############################################

print '判断是否是相交集',list_1.isdisjoint(list_2)
#两个有交集就False，没有交集就True



  