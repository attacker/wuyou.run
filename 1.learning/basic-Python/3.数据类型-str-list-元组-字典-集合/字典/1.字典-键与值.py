# -*- coding:utf-8 -*-
#Author Logos
#Mail:admin@attacker.club
#blog:blog.attacker.club
#description
dictionary = {}
dictionary1={}
print ("先建立空字典，后面再想放什么")

dictionary["lj"] = "18069" #或者用下面
dictionary1= {"lj":"18099"}

print (dictionary)
print (dictionary1)

print ("整个字典")
dictionary["gd"] = "hehe"
dictionary["gg"] = "119"
print  (dictionary)

print ("通过key获取值",dictionary["gd"])
print ("获取所有key",dictionary.keys())
print ("获取所有值",dictionary.values())
print ("获取所有项",dictionary.items())
"""
其他语言也有关联数组（associative array），将所有键和值关联在一起叫哈希表（hash tables）

键值要求严格：只能是使用不可变类型（布尔、整数，浮点数，字符串和元组）
不能使用一个列表或者字典作为键，因为他们是可变的类型
"""

dictionary = {"lj":"18099","gd":"hehehe","gg":"1199","aaa":"11"}
for key in sorted(dictionary.keys()):
    print (key,dictionary[key])
# sorted返回新副本排序keys，原数据不变 
