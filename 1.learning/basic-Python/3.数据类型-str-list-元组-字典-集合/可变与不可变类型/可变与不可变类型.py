#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-12-05 09:52:35
Description:
'''
"""

可变类型（mutable）：列表，字典，集合

不可变类型（unmutable）：数字，字符串，元组
"""

# list
l = [1,2,3,4]
id(l)
#4392665160
l[1] = 1.5
id(l)
#4392665160

a = 1
id(a)
#4297537952 
a+=1
id(a)
#4297537984