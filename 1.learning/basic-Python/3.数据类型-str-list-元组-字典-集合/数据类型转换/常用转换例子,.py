print('------------------1----------------')
a=100#整形变量
b=100.0#浮点型变量
c='hangzhou'#字符串
print(a,b,c)
print('---------------------2------------------')
a=b=c=1
print(a,b,c)
a,b,c=1,2,3
print(a,b,c)
print('--------------------3-------------------')
print('Number 数字')
a,b,c=20,5.5,True
#type可以查询变量所指的数据类型
print(type(a),type(b),type(c))
#也可以用isinstance来判断
# type()不会认为子类是一种父类类型
#isinstance()会认为子类是一种父类类型
print('String 字符串')
str1='zifuchuan'
print(str1[0:-1])#输出第一个到倒数第二个
print(str1[0])#输出第一个字符串
print(str1[2:5])#输出第三个到第五个字符串
print(str1[2:])#输出第三个后面所有的字符串
print(str1*2)#输出字符串2次
print(str1+'Test')#链接字符串
print('列表')
listp=['abc',768,2.33,'liebiao',70.2]
tinylist=[123,'liebiao']
print(listp)#输出完整列表
print(listp[0])#输出列表的第一个元素
print(listp[1:3])#输出第二个元素到第三个元素
print(listp[2:])#输出第三个元素开始的所有元素
print(tinylist*2)#输出两次列表
print(listp+tinylist)#链接两个列表
#该变列表中的元素
a=[1,2,3,4,5,6]
a[0]=9
a[2:5]=[13,14,5]
a[2:5]=[]#可以删除所指定的元素
print('Tuple 元组,用法跟上面的一样但修改不了元素')
print('set 集合')
student={'Tom','Jim','Mary','Tom','Jack','Rose'}
print(student)#输出集合，重复的元素被自动去掉
if 'Rose' in student:
    print('Rose 在集合中')
else:
    print('Rose不在集合中')
#set可以进行集合运算
a=set('abra')
b=set('alac')
print(a)#set可以去重复所以输出啊a,b,r
print(a-b)#a和b的差
print(a|b)#a和b,的并集
print(a&b)#a和b的交集
print(a^b)#a和b不同时存在的元素
print('Dictionary 字典')
tinydict={'name':'runoob','code':'1','site':'www.runoob.com'}
print(tinydict)#输出完整的字典
print(tinydict.keys())#输出所有的建
print(tinydict.values())#输出所有的值
print('----数据类型转换--------')
print(int(3.6))#浮点数转换为整数
print(float(1))#整数转换为浮点数
print(float('123'))#字符串转为浮点数
print(complex(1,2))#整数为复数
print(complex('1'))#字符串为负数
dict={'runoob':'runoob.com','google':'goole.com'}
print(str(dict))
i=int(10)
print(str(i))
print(repr(dict))
x=7
print(eval('3*x'))#可以操作字符串
listi=['Google','Taobao','Runoob','Baidu']
print(tuple(listi))
tpo=tuple(listi)
t=('1','2','3')
print(list(t))
print(tpo)
x=set('runoob')
y=set('google')
print(x,y)