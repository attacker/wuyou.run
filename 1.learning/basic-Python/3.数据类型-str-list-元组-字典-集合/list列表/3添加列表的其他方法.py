# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description

print "append() 列表末尾追加一个元素"
print "extend() 向列表末尾追加多个元素"
print "insert() 向指定位置插入元素"

letters = ['a','b','c','d']
print  "原始列表",letters
letters.append("e")
print  "1.向列表追加一个元素",letters
letters.extend(["f","g"])#圆括号里面是一个中括号的列表内容
print  "2.向末尾追加多个元素",letters
letters.insert(0,'z')
print  "3.向指定位置插入元素",letters

"""
三种方法 append(),extend([]),insert()
"""