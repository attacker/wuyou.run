# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description
letters = ['a','b','z','d']
if 'z' in letters: #搜索列表是否包含这个元素
    print "z"
else:
    print "not found z"

if "a" in letters:
    letters.remove('d') #匹配到后删除指定元素
else:
    print "not found a"
print "搜索是否批量到的条件并删除某个元素",letters
print "查找源元素索引",letters.index('z')
"""
for循环匹配元素
变量对象的remove方法删除元素
对象index查看元素指针
"""