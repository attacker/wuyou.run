# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description

letters = ['a','b','c','d']
print  ("原始列表",letters)
letters.append("e")
print  ("1.append向列表追加一个元素",letters)
letters.extend(["f","g"])#圆括号里面是一个中括号的列表内容
print  ("2.extend向末尾追加多个元素",letters)
letters.insert(0,'z')
print  ("3.insert向指定位置插入元素",letters)



print ("""append() 列表末尾追加一个元素
extend() 向列表末尾追加多个元素
insert() 向指定位置插入元素""")


poker = [x for x in range(2,11)] #列表生成式
poker.extend(["J","Q","K","A"]) #

print("扑克:",poker)