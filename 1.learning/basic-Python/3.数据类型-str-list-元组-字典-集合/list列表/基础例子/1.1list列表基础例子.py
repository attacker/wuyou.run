#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-03 21:55:05
Description:
'''


#{}大括号:字典 ;[]中括号:列表 ;小括号():元组


list = ['hehe'] # 定义一个列表数据
list.append(1)  # 添加数据到list

print ('列表', list, '类型', type(list))
print ("------------------------------------")

family = ['Mon', 'Dad', 'bady']
LuckyNumber = [1, 7, 9]
print ("幸运数", LuckyNumber, "家庭成员", family)
print (type(LuckyNumber))
print ("------------------------------------")


valuelist = []
valuelist.append(1)
print ("append一次只能传一个参数：", valuelist)
valuelist.append("hello")
print ("第二次传的参数", valuelist)





"""
1.
newlist[] #创建空列表，用于程序后期向里面添加元素
newlist.append()#append向列表添加元素，一次只能传一个参数进去，"."就是对象；格式： 变量.对象的操作
2.
list列表可以包含任何内容，
"""
