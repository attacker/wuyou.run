# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description

letters = ['a','b','c','d','e','f','g']

print  "原始list元素",letters

letters.remove('a')
print "letters.remove('a')元素",letters

del letters[2]
print "del letters[2]根据元素索引号",letters

letters.pop()
print "letters.pop()默认删最后一个元素",letters
print  "pop()根据索引删指定元素并可以打印出来:",letters.pop(3)

"""
三种方法 remove()、 del 、pop()
"""