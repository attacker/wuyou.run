# -*- coding:utf-8 -*-
#Author Logos
#Mail:admin@attacker.club
#blog:blog.attacker.club
#description

#元组
my_tuple = ("red","green","blue")
print  ("打印元组",my_tuple)

"""
1. 
元组使用圆括号()，列表是中括号了[]
元组是不可改变的，列表是可改变的
Tuples are immutable, lists are mutable.
2.
元组通常有不通数据，而列表是相同数据类型的数据队列。
元组表示的是结构，列表表示顺序
"""
point = (1,2)
#表示一个点
points = [(1,2),(1,3),(3,5)]
print  ("记录棋盘一个字的坐标用元组：",point)
print  ("记录棋盘所有字的坐标用列表：",points)


a=(1,2)
b=[3,4]
c = {a: 'start point'}
#c = {b:'end point'}  ##TypeError 列表不能作为字典key，元组可以
print (c,type(c))