#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2019-01-18 11:35:03
@LastEditTime: 2020-04-20 21:37:21
@Description: 
'''


# bytes object
b = b"example"

# str object
s = "example"

# str to bytes
bytes(s, encoding = "utf8")

# bytes to str
str(b, 1)

# an alternative method
# str to bytes
str.encode(s)

# bytes to str
bytes.decode(b)