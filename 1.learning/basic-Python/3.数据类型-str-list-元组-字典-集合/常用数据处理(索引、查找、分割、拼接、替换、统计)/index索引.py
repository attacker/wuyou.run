#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-29 17:46:47
Description:
'''

s = 'hello'

print(s[1])
# 第二位
print (s[-1])
# 最后一位
print(s.index('e'))
# 字符串索引序号
print(type(s))