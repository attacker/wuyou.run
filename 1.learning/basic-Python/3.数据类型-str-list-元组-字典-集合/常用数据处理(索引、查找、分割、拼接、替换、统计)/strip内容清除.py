#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-03 21:33:53
Description:
'''
s = '  hello,world!  '
s.strip()   # 清除所有空白
s.lstrip()  # 清理左空白
s.rstrip()  # 清理右空白
s2 = '***hello,world!***'
s2.strip('*') #清除指定内容