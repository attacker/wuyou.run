#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-05 16:29:51
Description:
'''


# 三元运算又称三目运算,即条件运算符


"""
简单条件语句：

if 条件成立:
    val = 1
else:
    val = 2
"""

val = 1 if 条件成立 else 2