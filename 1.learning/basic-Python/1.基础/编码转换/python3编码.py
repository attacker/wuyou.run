#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-05 17:36:24
Description:
'''


"""
PY3 除了把字符串的编码改成了unicode, 还把str 和bytes 做了明确区分， 
str 就是unicode格式的字符， bytes就是单纯二进制啦。
"""

