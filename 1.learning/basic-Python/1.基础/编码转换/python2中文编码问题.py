#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-12-12 13:12:03
Description: py2
'''

s为字符串判断
s.isalnum() 所有字符都是数字或者字母
s.isalpha() 所有字符都是字母
s.isdigit() 所有字符都是数字
s.islower() 所有字符都是小写
s.isupper() 所有字符都是大写
s.istitle() 所有单词都是首字母大写，像标题
s.isspace() 所有字符都是空白字符、\t、\n、\r
# True or False