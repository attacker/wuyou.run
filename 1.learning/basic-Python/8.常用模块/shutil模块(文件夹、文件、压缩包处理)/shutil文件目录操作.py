#shutil.copyfileobj(fsrc, fdst[, length])
#将文件内容拷贝到另一个文件中
import  shutil
shutil.copyfileobj(open('old.xml','r'), open('new.xml', 'w'))


#shutil.copyfile(src, dst)
#拷贝文件
shutil.copyfile('f1.log', 'f2.log') #目标文件无需存在

#shutil.copymode(src, dst)
#仅拷贝权限。内容、组、用户均不变
shutil.copymode('f1.log', 'f2.log') #目标文件必须存在

#shutil.copystat(src, dst)
#仅拷贝状态的信息，包括：mode bits, atime, mtime, flags
shutil.copystat('f1.log', 'f2.log') #目标文件必须存在


#shutil.copy(src, dst)
#拷贝文件和权限
shutil.copy('f1.log', 'f2.log')

#shutil.copy2(src, dst)
#拷贝文件和状态信息
shutil.copy2('f1.log', 'f2.log')
shutil.ignore_patterns(*patterns)


#shutil.copytree(src, dst, symlinks=False, ignore=None)
#递归的去拷贝文件夹
shutil.copytree('folder1', 'folder2', ignore=shutil.ignore_patterns('*.pyc', 'tmp*')) #目标目录不能存在，注意对folder2目录父级目录要有可写权限，ignore的意思是排除

#shutil.rmtree(path[, ignore_errors[, onerror]])
#递归的去删除文件
shutil.rmtree('folder1')


#shutil.move(src, dst)
# 递归的去移动文件，它类似mv命令，其实就是重命名。
shutil.move('folder1', 'folder3')