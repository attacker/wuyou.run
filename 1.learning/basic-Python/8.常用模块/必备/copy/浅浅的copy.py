# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description
import copy

person = ['name',['a',100]]
print person

p1=copy.copy(person)
#浅copy
print  p1,type(p1)
p2=person[:]
#完全切片
print   p2,type(p2)
p3=list(person)
#工厂函数 看上去有点像函数，实质上他们是类；
# 当你调用它们时，实际上是生成了该类型的一个实例，就像工厂生产货物一样.
print  p3,type(p3)
"""
三种方式实现结果一样
"""


firmly=['name',['saving',100]]
p1=firmly[:]
p2=firmly[:]

#夫妻共同财产100
p1[0]='jj'
p2[0]='ly'

p1[1][1]=50
#修改jj的元素

print p1
print p2