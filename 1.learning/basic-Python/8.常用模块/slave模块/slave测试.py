
# 序列化
import shelve

f = shelve.open('shelve_test')  # 打开一个文件



names = ["alex", "rain", "test"]
info = {'name':'alex','age':22}


f["names"] = names  # 持久化列表
f['info_dic'] = info
f.close()

# 反序列化
with shelve.open('shelve_test') as f: # 打开一个文件
    print(f['names'])
    print(f['info_dic'])

    # del f['test'] #还可以删除

"""
shelve模块是一个简单的k,v将内存数据通过文件持久化的模块，可以持久化任何pickle可支持的python数据
"""


