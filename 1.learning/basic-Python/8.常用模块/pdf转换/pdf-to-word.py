'''
Author: Logan.Li
Gitee: https://gitee.com/attacker
email: admin@attacker.club
Date: 2023-11-05 14:37:35
LastEditTime: 2023-11-05 14:38:04
Description: 
'''
import sys
from pdf2docx import Converter

def pdf_to_word(pdf_file, docx_file):
    cv = Converter(pdf_file)
    cv.convert(docx_file)
    cv.close()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 pdf-to-word.py source.pdf source.docx")
        sys.exit(1)

    pdf_file = sys.argv[1]
    docx_file = sys.argv[2]

    pdf_to_word(pdf_file, docx_file)
