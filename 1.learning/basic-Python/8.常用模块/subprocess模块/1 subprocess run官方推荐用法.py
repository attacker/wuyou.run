
"""
三种执行命令的方法

subprocess.run(*popenargs, input=None, timeout=None, check=False, **kwargs) #官方推荐

subprocess.call(*popenargs, timeout=None, **kwargs) #跟上面实现的内容差不多，另一种写法

subprocess.Popen() #上面各种方法的底层封装
"""

import  subprocess

subprocess.run(['df','-h'],stderr=subprocess.PIPE,stdout=subprocess.PIPE,check=True)
# 标准写法

subprocess.run('df -h|grep disk',shell=True) #shell=True的意思是这条命令直接交给系统去执行，不需要python负责解析
# 涉及到管道|的命令需要这样写；不能和PIPE的系统管道重复



"""
每次执行返回CompletedProcess 执行对象的结果
"""

res = subprocess.run(['df','-h'],stderr=subprocess.PIPE,stdout=subprocess.PIPE)
#通过操作系统管道"|" subprocess.PIPE 输出 标准输出和错误输出
res.stderr,a.stdout