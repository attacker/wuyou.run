
import platform
import  subprocess


# 获取操作系统平台
def getOpeningSystem():
    return platform.system()


# 判断是否联网
def isOnline():
    userOs = getOpeningSystem()
    try:
        if userOs == "Windows":
            subprocess.check_call(["ping", "-n", "2", "www.baidu.com"], stdout=subprocess.PIPE)
        else:
            subprocess.check_call(["ping", "-c", "2", "www.baidu.com"], stdout=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError:
        print("网络未连通！请检查网络")
        return False


if __name__ == "__main__":
    print(getOpeningSystem())
    print( isOnline())