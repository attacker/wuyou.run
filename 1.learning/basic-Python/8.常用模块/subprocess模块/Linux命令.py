#!/usr/bin/env python
# -*- coding: utf-8 -*-
import subprocess
import sys,os


def base(cmd):
    if subprocess.call(cmd, shell=True):
        raise Exception("{} 执行失败".format(cmd))

def run_if(args):
    base(args)

if __name__ == '__main__':
    # if(os.geteuid() != 0):
    #     raise("请以root权限运行")
    run_if("ifconfig |awk '/netmask/ && !/127/  {print $2}'")