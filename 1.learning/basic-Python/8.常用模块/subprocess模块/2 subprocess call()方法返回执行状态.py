import  subprocess

retcode = subprocess.call(["ls", "-l"]) #0 or 非0
# 执行命令，返回命令执行状态 ， 0 or 非0
print('状态:',retcode)


subprocess.check_call(["ls", "-l"])
# 执行命令，如果命令结果为0，就正常返回，否则抛异常 0

s = subprocess.getstatusoutput('ls /bin/ls')
print(s)
# 接收字符串格式命令，返回元组形式，第1个元素是执行状态，第2个是命令结果



#执行命令，并返回结果，注意是返回结果，不是打印，下例结果返回给res
res=subprocess.check_output(['ls','-l'])
print(res)