import  subprocess


a=subprocess.run('sleep 10',shell=True,stdout=subprocess.PIPE)
a=subprocess.Popen('sleep 10',shell=True,stdout=subprocess.PIPE)
# 区别是Popen会在发起命令后立刻返回，而不等命令执行结果

"""
如果你调用的命令或脚本 需要执行10分钟，你的主程序不需卡在这里等10分钟，可以继续往下走，干别的事情，每过一会，通过一个什么方法来检测一下命令是否执行完成就好了。

Popen调用后会返回一个对象，可以通过这个对象拿到命令执行结果或状态等，该对象有以下方法

poll() , wait() , kill()
"""



"""

"""

a.poll() # 询问进程是否完成
a.pid    # 查看pid进程
a.terminate() # 中止进程信号
a.send_signal() # 给程序发