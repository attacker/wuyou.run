

import time


now = time.time()
print("时间戳:",now)

d1 = time.localtime(now)
# print('解析时间戳',s2)
print("时间戳的格式时间" ,time.strftime("%Y-%m-%d %H:%M",d1))

print("-"*9)

t1 = time.strptime('2017-10-3 17:54',"%Y-%m-%d %H:%M")
print("格式化数据转元组:",t1)
# 时间字符串解析为时间为元组

print("获取年月日等信息:",t1.tm_mon,t1.tm_mday)


# # 2019-05-15T20:00:00.000+0800
