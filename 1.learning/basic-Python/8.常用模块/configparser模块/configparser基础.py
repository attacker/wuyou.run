
import configparser # 导入模块
config = configparser.ConfigParser()  #实例化(生成对象)
#config.sections()  #调用sections方法
conf = config.read('test.conf')  # 读配置文件(注意文件路径)

print(conf)


print(config.sections() )

a = 'bitbucket.org' in config #判断元素是否在sections列表内
print(a)
b = 'bytebong.com' in config
print(b)


print( config['bitbucket.org']['User'] ) # 通过字典的形式取值

"""
[DEFAULT]
ServerAliveInterval = 45
Compression = yes
CompressionLevel = 9
ForwardX11 = yes

[bitbucket.org]
User = hg

[topsecret.server.com]
Port = 50022
ForwardX11 = no
"""