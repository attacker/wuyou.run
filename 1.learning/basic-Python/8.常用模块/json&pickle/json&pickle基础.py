

"""
json，用于字符串 和 python数据类型间进行转换
pickle，用于python特有的类型 和 python的数据类型间进行转换

都提供了四个功能：dumps、dump、loads、load
"""


import pickle
data = {'k1':123,'k2':'Hello'}
print("原数据:",data)

# pickle.dumps 将数据通过特殊的形式转换位只有python语言认识的字符串 (bytes)

p_str = pickle.dumps(data)
print(type(p_str),p_str,pickle.loads(p_str))

#pickle.dump 将数据通过特殊的形式转换位只有python语言认识的字符串，并写入文件
with open('result.pk','wb') as f:
    pickle.dump(data,f)

# with open('result.pk','rb') as f:
#     for i in f:
#         print(i)


import json
# json.dumps 将数据通过特殊的形式转换位所有程序语言都认识的字符串
j_str = json.dumps(data)
print("json字符串类型:",j_str,"数据类型:",type(j_str))



#pickle.dump 将数据通过特殊的形式转换位只有python语言认识的字符串，并写入文件
with open('result.json','w') as f:
    #json.dump(json.loads(j_str), f)
    json.dump(data,f)


# dumps是将dict转化成str格式，loads是将str转化成dict格式。
# dump和load也是类似的功能，只是与文件操作结合起来了。

"""
JSON:
优点：跨语言、体积小
缺点：只能支持int\str\list\tuple\dict

Pickle:
优点：专为python设计，支持python所有的数据类型
缺点：只能在python中使用，存储数据占空间大
"""