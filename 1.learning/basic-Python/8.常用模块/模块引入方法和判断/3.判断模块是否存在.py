#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-04-20 10:00:10
@LastEditTime: 2020-04-20 16:48:55
@Description: 
'''
try:
    import requests
    from requests.auth import HTTPBasicAuth
except ImportError:
    print("[!]Error: You have to install requests module.")
    exit()
