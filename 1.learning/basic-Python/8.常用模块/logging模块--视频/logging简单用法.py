

import logging

# logging.warning("user [alex] attempted wrong password more than 3 times")
# logging.critical("server is down")
# # 简单例子



logging.basicConfig(filename='example.log',level=logging.INFO)
logging.debug('This message should go to the log file')
logging.info('So should this')
logging.warning('And this, too')
# 日志写到文件里