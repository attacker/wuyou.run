# -*- coding:utf-8 -*-
# -------------------------------------------
# Python简单密码加密程序
# 随机生成4位salt，与原始密码组合，通过md5加密
# Author : Lrg
# -------------------------------------------
# encoding = utf-8
from random import Random
from hashlib import md5

# 获取由4位随机大小写字母、数字组成的salt值
def create_salt(length=4):
    salt = ''
    chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
    len_chars = len(chars) - 1
    random = Random()
    for i in xrange(length):
        # 每次从chars中随机取一位
        salt += chars[random.randint(0, len_chars)]
    return salt


# 获取原始密码+salt的md5值
def create_md5(input_pwd,salt):
    md5_obj = md5()
    md5_obj.update(input_pwd + salt)
    return md5_obj.hexdigest()


input_pwd = str(raw_input("请输入密码:")) # 原始密码

salt = create_salt() # 随机生成4位salt
md5 = create_md5(input_pwd,salt)# 加密后的密码

print  "[原始密码]\n\n",input_pwd
print '[加点盐]\n\n', salt
print '[md5密文]\n\n', md5


