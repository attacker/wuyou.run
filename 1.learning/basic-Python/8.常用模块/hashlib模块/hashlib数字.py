import  hashlib


md5 = hashlib.md5()
md5.update(b"123456")
#参数必须是byte类型，否则报Unicode-objects must be encoded before hashing错误


print(md5.digest()) #2进制格式hash
print('长度:',len(md5.hexdigest())) #16进制格式hash
print('加密字符串:',md5.hexdigest())


strs = md5.digest()
strs = str(strs)

st= strs.encode('utf-8').decode('utf-8') # 解码一下
print("解码一下 %st" %  st)


