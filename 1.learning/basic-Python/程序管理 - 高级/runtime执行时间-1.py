#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-04-22 14:44:15
@LastEditTime: 2020-04-22 14:54:48
@Description:
'''

import datetime
import time
starttime = datetime.datetime.now()

time.sleep(1.5)
endtime = datetime.datetime.now()

print((endtime - starttime).seconds)

start = time.time()
time.sleep(2.5)
end = time.time()
print("Run time is %.3f" % (end-start))
