#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-04-22 14:43:02
@LastEditTime: 2020-04-22 14:43:55
@Description:  装饰器查看每个函数执行时间
'''


import time


def print_run_time(func):
    def wrapper(*args, **kw):
        local_time = time.time()
        func(*args, **kw)
        print('[+] [%s] Run time is %.2f' %
              (func.__name__, time.time() - local_time))
    return wrapper
