#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-12-12 19:46:01
Description:
'''


name="Logos"
age=12
print(
    '''
    name    : %s  #姓名
    age     : %d  #年龄
    '''
    %(name,age)
    )
#%s就是代表字符串占位符，除此之外，还有%d,是数字占位符


s = '\tname:{0} \n\tage:{1}'.format(name,age)
print(s)