#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email: admin@attacker.club
#Site:  blog.attacker.club
#Mail:  admin@attacker.club
#Date:  2017/9/7
#Description:   
# --------------------------------------------------

import re



## "."  匹配除\n换行符以外的任意单字符' 0到多次
print re.findall('a.c','abcdefg')


## + 前一个字符一直匹配 1到多次
print "+",re.findall('a+1','a123456789aaaaabcdefg')
print "+ 取相同值为数字\d",re.findall('\d+7','a123456789aaaaabcdefg')
print "+ 原生字符（）取匹配的值",re.findall(r'(\d+)7','a123456789aaaaabcdefg')

## ? 0到1次
print  "?",re.findall('a.?','a123456789abcdefg')

## "*" 批量前面字符0-多次任意字符
print re.findall('a.*a','a123456789abcdefg')


## "{}" 大括号匹配次数和范围
print "{3}",re.findall('a.{3}','a123456789abcdefga987a887aahss') #匹配3次
print "{1,}",re.findall('a.{1,}','a123456789abcdefg')
print "{2,2}",re.findall('a.{2,2}3','a123456789abcdefg3abssab3')
#1次无线
print re.findall('a.{,6}','a123456789abcdefg')
print re.findall('a.{1,2}','a123456789abcdefg')
#1次无线
print re.findall('a.{,6}7','a123456789abcdefg')
#


"""
次数:
　　* 重复零次或更多次
　　+	重复一次或更多次
　　?	重复零次或一次
　　{n}	重复n次
　　{n,}	重复n次或更多次
　　{n,m}	重复n到m次
"""