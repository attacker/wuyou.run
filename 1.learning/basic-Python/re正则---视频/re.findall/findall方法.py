#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email:			admin@attacker.club
#Site:		    blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-09-05 23:54:22 Last:2017-09-06 23:41:16
#Description:   findall
# --------------------------------------------------

import re

def Info (Text):
    print (">>>>>\t\033[1;31m%s\033[0m\t<<<<<" % Text)



print ("""'re.findall(pattern, string, flags=0)""")
print ('普通字符匹配',re.findall('hehe','zhinenghehe'),
    type(re.findall('hehe','zhinenghehe')))

Info('re模块中最常用和简单的一个方法,以列表的形式展示返回值')


a = """
re.match 从头开始匹配
re.search 匹配包含
re.findall 把所有匹配到的字符放到以列表中的元素返回
re.split 以匹配到的字符当做列表分隔符
re.sub 匹配字符并替换
re.fullmatch 全部匹配
"""
print(a)