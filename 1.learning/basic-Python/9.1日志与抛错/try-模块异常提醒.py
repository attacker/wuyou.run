#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2020-02-12 11:44:40
Description:
'''
try:
    import requests
    from requests.auth import HTTPBasicAuth
except ImportError:
    print("[!]Error: You have to install requests module.")
    exit()

try:
    from bs4 import BeautifulSoup
except ImportError:
    print("[!]Error: You have to install BeautifulSoup module.")
    exit()

    

try:
    from config.Config import *
except ImportError:
    print("[!]Error: Can't find Config file for searching.")
    exit()

try:
    from include.ColorPrint import *
except ImportError:
    print("[!]Error: Can't find ColorPrint file for printing.")
    exit()



