

try:
    #正常的操作...
    pass
except Exception as e:
    print(e) #捕捉发生的所有异常
else:
    pass
    #如果没有异常执行这块代码...

#以上方式try-except语句捕获所有发生的异常。但这不是一个很好的方式，我们不能通过该程序识别出具体的异常信息。因为它捕获所有的异常。


"""

except(Exception1[, Exception2[,...ExceptionN]]]):
   发生以上多个异常中的一个，执行这块代码
   ......................
else:
    如果没有异常执行这块代码

"""