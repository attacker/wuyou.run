#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-12-17 23:19:47
Description:
'''

import logging


def log(message):
    logger = logging.getLogger('testlog')

    streamhandler = logging.StreamHandler()
    streamhandler.setLevel(logging.ERROR)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
    streamhandler.setFormatter(formatter)

    logger.addHandler(streamhandler)
    logger.error(message)

if __name__ == '__main__':
    log('hi')
    log('hi too')
    log('hi three')


"""
深度解析：
Google之后，大概搞明白了，就是你第二次调用log的时候，根据getLogger(name)里的name获取同一个logger，而这个logger里已经有了第一次你添加的handler，第二次调用又添加了一个handler，所以，这个logger里有了两个同样的handler，以此类推，调用几次就会有几个handler。。

所以这里有以下几个解决办法：

每次创建不同name的logger，每次都是新logger，不会有添加多个handler的问题。（ps:这个办法太笨，不过我之前就是这么干的。。）
像上面一样每次记录完日志之后，调用removeHandler()把这个logger里的handler移除掉。
在log方法里做判断，如果这个logger已有handler，则不再添加handler。
与方法2一样，不过把用pop把logger的handler列表中的handler移除。
--------------------- 
作者：huilan_same 
来源：CSDN 
原文：https://blog.csdn.net/huilan_same/article/details/51858817 
版权声明：本文为博主原创文章，转载请附上博文链接！
"""
