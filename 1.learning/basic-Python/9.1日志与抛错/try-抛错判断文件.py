


## 判断文件是否存在
try:
    f = open("test.txt","r")
    f.close()
except FileNotFoundError:
    print ("File is not found.")
except PersmissionError:
    print ("You don't have permission to access this file.")

# 如果你open的文件不存在，将抛出一个FileNotFoundError的异常;
# 文件存在，但是没有权限访问，会抛出一个PersmissionError的异常。


try:
    f =open()
    f.close()
except IOError:
    print ("File is not accessible.")
# 简化IOError子类写法；   使用try语句进行判断，处理所有异常非常简单和优雅的。且不需要引入外部模块。