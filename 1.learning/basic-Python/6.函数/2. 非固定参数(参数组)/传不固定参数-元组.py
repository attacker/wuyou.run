

def send_alert(msg,user):
    print(msg, user)

# 1个运维
send_alert('服务器挂了!!!','晶晶哥')

def send_alerts(msg,*user): # 专业用法 *args
    print(msg,*user) # 里面不加*,就是元组参数需要要for循环迭代;

# 10个运维
send_alerts('服务器挂了!!!','晶晶哥','小明','二狗子','...')

# 传个列表试试
send_alerts('服务器挂了!!!',*['晶晶哥','小明','二狗子','...']) #要加*解包成字符