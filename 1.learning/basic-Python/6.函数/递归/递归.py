
import sys

def recursion (n):
    print(n)
    recursion(n+1)

print(sys.getrecursionlimit())
sys.setrecursionlimit(1500)
recursion(1)
# 默认最大递归层次1为000次
