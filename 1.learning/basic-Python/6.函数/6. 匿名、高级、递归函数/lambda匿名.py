#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Author:  LJ
Email:   admin@attacker.club
Time:    2020/3/24 22:16
Description: 
"""


print(lambda x:x*2, 9)
for i in (lambda x:x*2, 9):
    print(i)

# 闭包
lambda x: (lambda: x**2)

def lambda1(x):
    def lambda2():
        return x**2
    return lambda2

print(lambda1(2))