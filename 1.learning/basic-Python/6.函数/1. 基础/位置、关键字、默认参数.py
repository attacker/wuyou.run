


def printsomething (name,args):
    print (name)
    print (args)

printsomething("位置参数","xxxx")


def printsomething (name,args=1):
    print (name)
    print (args)

printsomething("默认参数",2)




def stu_register(name,age,*args,**kwargs): # *kwargs 会把多传入的参数变成一个dict形式
    print(name,age,args,kwargs)

stu_register("Alex",22)
#输出
#Alex 22 () {}#后面这个{}就是kwargs,只是因为没传值,所以为空

stu_register("Jack",32,"CN","Python",sex="Male",province="ShanDong")
#输出
#Jack 32 ('CN', 'Python') {'sex': 'Male', 'province': 'ShanDong'}