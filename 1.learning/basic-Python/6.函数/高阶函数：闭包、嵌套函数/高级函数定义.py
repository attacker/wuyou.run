
def add(x,y,f):
    return f(x) + f(y)


res = add(3,-6,abs)
print(res)



'''
满足以下任意一个条件，即是高阶函数:
1. 接受一个或多个函数作为输入
2. return 返回另外一个函数
'''