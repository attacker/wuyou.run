

def func():

    n = 10

    def func2():

        print("func",n)

    return  func2


f =  func()
print(f)
f()


"""
过程：在函数内部再调用函数，并返回这个函数结果就是闭包；

"""