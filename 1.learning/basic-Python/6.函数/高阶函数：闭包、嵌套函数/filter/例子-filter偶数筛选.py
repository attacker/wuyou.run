


"""
用filter函数处理数字列表，将列表中所有的偶数筛选出来
"""

num = [1,3,5,6,7,8]

f = list(filter(lambda x:x%2 == 0,num))
print(f)