#_*_coding:utf-8_*_

user_status = False #用户登录了就把这个改成True

def login(func): #把要执行的模块从这里传进来;参数 hennan

    def inner():
        _username = "alex"  # 假装这是DB里存的用户信息
        _password = "123456"  # 假装这是DB里存的用户信息
        global user_status

        if user_status == False:
            username = input("user:")
            password = input("pasword:")

            if username == _username and password == _password:
                print("welcome login....")
                user_status = True
            else:
                print("wrong username or password!")

        if user_status == True:
            func()  # 看这里看这里，只要验证通过了，就调用相应功能 ；参数执行 henan()

    return inner # 返回函数内存地址,不执行 闭包


def home():
    print("---首页----")

def america():
    print("----欧美专区----")

def japan():
    print("----日韩专区----")


@login
def henan():
    print("----河南专区----")


japan()

# henan = login(henan)
henan()