
#_*_coding:utf-8_*_

user_status = False

def login(func):

    def inner(*args,**kwargs):  # 传参
        _username = "alex"
        _password = "123456"
        global user_status

        if user_status == False:
            username = input("user:")
            password = input("pasword:")

            if username == _username and password == _password:
                print("welcome login....")
                user_status = True
            else:
                print("wrong username or password!")

        if user_status == True:
            func(*args,**kwargs) #传入函数可以传参

    return inner # 返回函数内存地址,不执行 闭包


def home():
    print("---首页----")

def america():
    print("----欧美专区----")
@login
def japan():
    print("----日韩专区----")


@login
def henan(style):
    print("----河南专区----", style)




# henan = login(henan)
henan("动漫")
japan() # 非固定参数传参不报错

