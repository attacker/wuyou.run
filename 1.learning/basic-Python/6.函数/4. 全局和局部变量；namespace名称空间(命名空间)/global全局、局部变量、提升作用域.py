# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description

name = "Alex Li" # 全局变量

def change_name():
    global name # global先声明变量，将声明的变量提升为全局变量
    name = "Alex 又名 金角大王,路飞学城讲师" # 局部变量
    print("after change", name)


change_name()

# name = "Alex Li" # 再更新一次全局变量

print("在外面看看name改了么?", name)