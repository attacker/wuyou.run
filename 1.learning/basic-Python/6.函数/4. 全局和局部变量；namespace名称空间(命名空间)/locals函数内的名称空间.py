# -*- coding:utf-8 -*-
#Author LJ
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description

'''
locals: 是函数内的名称空间，包括局部变量和形参
'''


#作用域查找顺序
level = 'L0'
n = 22


def func():
    level = 'L1'
    n = 33
    print(type(locals()),locals())

    def outer():
        n = 44
        level = 'L2'
        print(locals(),n)

        def inner():
            level = 'L3'
            print(locals(),n) #此外打印的n是多少？
        inner()
    outer()


func()