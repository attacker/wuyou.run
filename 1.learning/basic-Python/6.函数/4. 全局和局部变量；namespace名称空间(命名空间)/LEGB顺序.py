
作用域查找顺序:

locals
enclosing #相邻的
globals
builtins

LEGB 代表名字查找顺序: locals -> enclosing function -> globals -> __builtins__

locals 是函数内的名字空间，包括局部变量和形参
enclosing 外部嵌套函数的名字空间
globals 全局变量，函数定义所在模块的名字空间
builtins 内置模块的名字空间