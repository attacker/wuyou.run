#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-12-09 13:09:41
Description: python3 只支持items方法
'''


d = {'x': 'A', 'y': 'B', 'z': 'C' }
print ('所有项目',d.items(),type(d.items()))
l = []
for k, v in d.items():
    print (k, '=', v)
    l.append("%s=%s" % (k,v))
print (l)



results = [k + '=' + v for k, v in d.items()]
print(results)
