def range2(n):

    count = 0
    while count < n:
        print("count",count)
        count += 1

        yield count #return

a = range2(10)
print(a)

next(a)
next(a)
next(a)

# for i in range2:
#     print(i)

"""
:return 返回并中止function

:yield 返回数据，并冻结当前的执行过程；
next唤醒冻结的函数执行过程，直到遇到下一个yield
"""