
L = [x * x for x in range(10)]
print('列表生成式',L)


L = (x * x for x in range(10))
print('列表生成式[]改成()成为生成器',L)

print(L.__next__())
print(L.__next__())
print(next(L))

for n in L:
    print('剩下的', n)
    if  n > 30:
        break


# next 没有更多的元素时，抛出StopIteration的错误。
# 平常使用for循环迭代

"""

for n in L:
    pass
# Python3 for循环本质上就是通过不断调用next()函数实现的
"""


#for迭代实际上完全等价于：
while True:
    try:
        print(next(L))

    except StopIteration:
        # 遇到StopIteration就退出循环
        break
