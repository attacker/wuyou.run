
# 生成器节省内存（）

# 生成方法 1.列表生成式  2.函数方式

a = ( x for x in range(5))

next(a)
print(a)


for i in a:
    print(i)


"""
range 底层就是生成器

for 循环对生成器有错误捕捉

next(a) 迭代完会报错
"""




