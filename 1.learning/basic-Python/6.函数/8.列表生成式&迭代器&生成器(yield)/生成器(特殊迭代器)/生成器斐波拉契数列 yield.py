#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2019-01-15 22:23:38
@LastEditTime: 2020-06-11 17:45:27
@Description: 
'''


def fib(max):
    n, a, b = 0, 0, 1

    while n < max:
        # print(b)
        yield b  # 同return，有冻结等待next函数
        a, b = b, a+b
        n += 1


# return 'done'
d = fib(10)
print("10位的fib", d)

for i in d:
    print(i)

print("1+1  2+1   3+2  5+3  8+5")
