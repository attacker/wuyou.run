
L = list(map(lambda x:x+2,range(10)))
print(L,type(L))

li = [x for x in range(20,25)]
print(li,type(li))
#列表生成式


li = iter([x for x in range(20,25)])
print(li,type(li))
#列表迭代器

li = (x for x in range(20,25))
print(li,type(li))
#生成器

"""
什么是迭代器：

可以被next()函数调用并不断返回下一个值的对象称为迭代器：Iterator
"""


