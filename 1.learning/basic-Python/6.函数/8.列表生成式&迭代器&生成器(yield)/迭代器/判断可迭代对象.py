from collections import  Iterator

a=isinstance((x for x in range(10)), Iterator)
print(a)

isinstance([xxx], Iterator)

isinstance({xxx}, Iterator)
isinstance(100, Iterable)
isinstance('abc', Iterator)

"""
from collections import Iterable
>isinstance([], Iterable)
True
>isinstance({}, Iterable)
True
>isinstance(100, Iterable)
False

# 列表，字典等集合数据类型都是可以迭代的对象：Iterable

"""

"""
from collections import Iterator
>isinstance((x for x in range(10)), Iterator)
True

可以被next()函数调用并不断返回下一个值的对象称为迭代器：Iterator
"""