#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2018-11-05 14:40:24
Description:
'''

L = []
for x in range(1, 11):
    L.append(x * x)

print(L)



X = [x * x for x in range(1, 11)]
print ("\n列表生成式用一行语句代替循环生成:")
print("给每个值相乘",X)

p = [i + 1 for i in range(1,11)]
print("给每个值加1",
      p
      )


"""
py3 range 替换py2 xrange 生成器非列表
"""