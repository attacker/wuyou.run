def range2(n):

    count = 0
    while count < n:
        print("count",count)
        count += 1


        yield count #类似return
        print()

"""
:return 返回并中止function

:yield 返回数据，并冻结当前的执行过程；
next唤醒冻结的函数执行过程，直到遇到下一个yield

send可以参数值
第一次调用时，请使用next()语句或是send(None)，不能使用send发送一个非None的值，否则会出错的
"""

n = range2(100)

n.send(None)
next(n)
n.send('stop')
next(n)
next(n)
next(n)
