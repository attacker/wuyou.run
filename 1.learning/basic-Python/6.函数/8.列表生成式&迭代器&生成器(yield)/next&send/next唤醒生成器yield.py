def range2(n):

    count = 0
    while count < n:
        print("count",count)
        count += 1

        yield count #类似return

"""
:return 返回并中止function

:yield 返回数据，并冻结当前的执行过程；
next唤醒冻结的函数执行过程，直到遇到下一个yield

"""

n = range2(9)

next(n)
next(n)
# next() = n.send(None)