#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-7
#Description:
# --------------------------------------------------

# -*- coding: utf-8 -*-
"""图标"""
import sys
from PyQt5 import QtWidgets,QtGui


class Icon(QtWidgets.QWidget):
    def __init__(self, parent = None):
        QtWidgets.QWidget.__init__(self, parent)

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle("图标")
        self.setWindowIcon(QtGui.QIcon(r'sample.ico'))


app = QtWidgets.QApplication(sys.argv)
icon = Icon()
icon.show()
sys.exit(app.exec_())