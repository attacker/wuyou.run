#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""


#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-27
#Description:


"""



import re
import os
import  sys
import time

#提示
banner=">>>>>\t\033[1;33m%s\033[0m\t<<<<<"
def warn (Text):
    print (banner % Text)
def info (Text):
    print (banner % Text)

#shell
def shell(cmd):
    os.system(cmd+'&>/dev/null')
    #不返还
def shellinfo(cmd):
    r=os.popen(cmd)
    text=r.read()
    r.close()
    return text
    #有返回


# file
def file(path,method,content):
    f=open(path,method)
    f.write(content)
    f.close()


#examples
""" 
warn('安装失败') #打印警告
info('完成') #打印信息
shell('dir') #执行命令
file('hehe','w','www.baidu.com\nwww.attacker.club')#写入文件

"""

if __name__ == "__main__":
    shell('touch log.txt')
    shell('ping 192.168.1.1')
