# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club
#description 计时器
import time,sys,os

#判断系统
if ("nt" == os.name  ):
    run_cmd = "cls"
elif "posix" == os.name :
    run_cmd = "clear"
else:
    print "系统不能识别，执行失败"
    sys.exit(1)


#os.system(run_cmd)
while True:
    print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    # 格式化成2016-03-20 11:45:39形式
    time.sleep(1)  # 等待10秒
    os.system(run_cmd)
