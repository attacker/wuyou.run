# -*- coding:utf-8 -*-
#Author LJ 
#Mail:admin@attacker.club
#Site:blog.attacker.club


for i in [1,2,3,4,5]: #指定数值与次数1,2,3,4,5
    print i,"hello" #缩进的代码块，叫做循环体 body of the loop

for i in range (10): #(0,10)的简写9次，循环范围0,1,2.....
    print i,"hello"

for i in range (11,21): #10次 范围11-20
    print i
for i in range (100,200,25): # 给range()函数增加第三个参数,则循环按步长25计算
    print i

for i in range (10,4,-1): #负数时向下计数
    print i