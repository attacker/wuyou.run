#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-10
#Description:


"""



class Human:
    """
    此类主要是构建人类
    """
    mind = '有思想'  # 第一部分：静态属性 属性 静态变量 静态字段
    dic = {}
    l1 = []


    def work(self): # 第二部分：方法 函数 动态属性
        print('人类会工作')

"""
类的结构从大方向来说就分为两部分：
静态变量。
动态方法。
"""
p = Human()

p.work() # 实例化方法

print(p.mind) # 实例属性