class Human:
    """
    此类主要是构建人类
    """
    mind = '有思想'  # 第一部分：静态属性 属性 静态变量 静态字段
    dic = {}
    l1 = []
    def work(self): # 第二部分：方法 函数 动态属性
        print('人类会工作')
print(Human.__dict__)
print(Human.__dict__['mind'])


# Human.__dict__['mind'] = '无脑'
print(Human.__dict__)  # 错误
#通过这种方式只能查询，不能增删改.
# 第一种方式只用户查询全部内容（一般不用单独属性查询）

 # 万能的点
class Human:
    """
    此类主要是构建人类
    """
    mind = '有思想'  # 第一部分：静态属性 属性 静态变量 静态字段
    dic = {}
    l1 = []
    def work(self): # 第二部分：方法 函数 动态属性
        # print(self)
        print('人类会工作')
print(Human.mind)  # 查
Human.mind = '无脑'  # 改
print(Human.mind)
del Human.mind  # 删
Human.walk = '直立行走'
print(Human.walk)
# 通过万能的点 可以增删改查类中的单个属性



# 两种做一个总结：如果想查询类中的所有内容，通过 第一种__dict__方法，如果只是操作单个属性则用万能的点的方式