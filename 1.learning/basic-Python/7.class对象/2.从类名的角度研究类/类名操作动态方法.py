class Human:
    """
    此类主要是构建人类
    """
    mind = '有思想'  # 第一部分：静态属性 属性 静态变量 静态字段
    dic = {}
    l1 = []
    def work(self): # 第二部分：方法 函数 动态属性
        # print(self)
        print('人类会工作')
    def tools(self):
        print('人类会使用工具')
Human.work(111)
Human.tools(111)

#下面可以做，但不用。
Human.__dict__['work'](111)