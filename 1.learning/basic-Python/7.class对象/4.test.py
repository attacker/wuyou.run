#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------

'''
class Student(object):
    def __init__(self, name, gender):
        self.name = name
        self.gender = gender


A=Student('lj','man')
print(A.name, A.gender)
'''


class Student(object):
    def __init__(self, name, gender):
        self.__name = name
        self.__gender = gender

    def get_name(self):
    	print(self.__name)
    def get_gender(self):
    	print(self.__gender)


A=Student('lj','man')
A.get_name()
A.get_gender()
#内部调用
