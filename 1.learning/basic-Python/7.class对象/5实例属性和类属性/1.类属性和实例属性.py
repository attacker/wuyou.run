#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-12
#Description:
# --------------------------------------------------


class Student(object):
    count = 0


    def __init__(self, name):
        self.name = name
        Student.count += 1
        #self.count = self.count + 1


'''
为了统计学生人数，可以给Student类增加一个类属性，
每创建一个实例，该属性自动增加
'''



if Student.count != 0:
    print('测试失败!')
else:
    bart = Student('Bart')
    if Student.count != 1:
        print('测试失败!')
    else:
        lisa = Student('Bart')
        if Student.count != 2:
            print('测试失败!')
        else:
            print('Students:', Student.count)
            print('测试通过!')

HAHA=Student("hehe")
print(Student.count)

"""
结果：list 是可变类型，int 是不可变类型；
self.count是实例属性作用范围在本实例，Student.count这个属性类的所有实例都可以访问到。
"""