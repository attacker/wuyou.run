

from enum import Enum,unique


@unique  # @unique装饰器可以帮助我们检查保证没有重复值。
class Color(Enum):
    red = 1
    green = 2
    blue = 3


Month = Enum('Month', ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))

for name, member in Month.__members__.items():
    print(name, '=>', member, ',', member.value)

@unique
class Weekday(Enum):
    Sun = 0 # Sun的value被设定为0
    Mon = 1
    Tue = 2
    Wed = 3
    Thu = 4
    Fri = 5
    Sat = 6




print(Color.red.value)
print(Month.Aug.value)
print(Weekday(5),Weekday.Wed.value)