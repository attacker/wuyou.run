

class Student(object):
    pass


s = Student()
s.name = 'Michael' # 动态给实例绑定一个属性
print(s.name)



def set_age(self, age):
    self.age = age
# 定义一个函数作为实例方法


from types import MethodType
s.set_age = MethodType(set_age, s)# 给实例绑定一个方法
s.set_age(25) # 调用实例方法
s.age # 测试结果
print(s.age)

s2 = Student() # 创建新的实例
#s2.set_age(25) # 尝试调用方法  报错'Student' object has no attribute 'set_age'


def set_score(self, score):
    self.score = score

Student.set_score = set_score  # 为了给所有实例都绑定方法，可以给class绑定方法

s.set_score(100)
s2.set_score(99)
print(s.score,s2.score)
# 通常情况下，上面的set_score方法可以直接定义在class中，但动态绑定允许我们在程序运行的过程中动态给class加上功能，这在静态语言中很难实现。