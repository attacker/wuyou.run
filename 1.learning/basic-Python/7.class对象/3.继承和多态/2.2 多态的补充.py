#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------


'''
当我们定义一个class的时候，我们实际上就定义了一种数据类型。
我们定义的数据类型和Python自带的数据类型，比如str、list、dict没什么两样
'''

class Animal(object):
    def run(self):
        print('Animal is running...')

class Cat(Animal):
    def run(self):
        print("cat eat flash")


a = list() # a是list类型
b = Animal() # b是Animal类型
c = Cat() # c是Dog类型

print (isinstance(a, list))
print (isinstance(b, Animal))
#判断一个变量是否是某个类型(class)可以用isinstance()判断
print(type(b),type(c))

print(isinstance(c,Animal)) #c不仅仅是Cat，c还是Animal
print(isinstance(b,Cat)) # Cat可以看成Animal，但Animal不可以看成Cat

'''
著名的“开闭”原则:
对扩展开放：允许新增Animal子类；
对修改封闭：不需要修改依赖Animal类型的run_twice()等函数
'''
"""
“鸭子类型”，它并不要求严格的继承体系，
一个对象只要“看起来像鸭子，走起路来像鸭子”，那它就可以被看做是鸭子。

--- 重要点
继承可以把父类的所有功能都直接拿过来，这样就不必重零做起，
子类只需要新增自己特有的方法，也可以把父类不适合的方法覆盖重写
"""



