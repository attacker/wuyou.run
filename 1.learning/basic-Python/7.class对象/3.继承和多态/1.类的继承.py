#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------


class Animal(object):
    def run(self):
        print('Animal is running...')
#这是父类;基类、父类或超类(Base class、Super class)


class Dog(Animal):
    pass
#子类(Subclass)


dog = Dog()
dog.run()
#继承父类的方法


