#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------


class Animal(object):
    def run(self):
        print('Animal is running...')

class Cat(Animal):
    def run(self):
        print("cat eat flash")


cat = Cat()
cat.run()
#子类与父类方法相同时会覆盖父类的方法 (继承中的多态)