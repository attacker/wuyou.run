#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-12
#Description:
# --------------------------------------------------


import types

def fn():
    pass
...

type(fn)==types.FunctionType
#True

type(abs)==types.BuiltinFunctionType
#True
type(lambda x: x)==types.LambdaType
True
type((x for x in range(10)))==types.GeneratorType
#True