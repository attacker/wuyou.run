#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-12
#Description:
# --------------------------------------------------


class MyObject(object):
    def __init__(self):
        self.x = 9
    def power(self):
        return self.x * self.x
obj = MyObject()





hasattr(obj, 'x') # 有属性'x'吗？
#True
print(obj.x)
#9


hasattr(obj, 'y') # 有属性'y'吗？
#False
setattr(obj, 'y', 19) # 设置一个属性'y'
hasattr(obj, 'y') # 有属性'y'吗？
#True

print (getattr(obj, 'y')) # 获取属性'y'
#19
print(obj.y) # 获取属性'y'
#19

"""
hasattr判断属性是否存在，getattr获取属性，setattr添加属性
如果试图获取不存在的属性，会抛出AttributeError的错误
"""
