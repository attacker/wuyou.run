#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-12
#Description:
# --------------------------------------------------


"""
class的继承关系来说，使用type()就很不方便。我们要判断class的类型，可以使用isinstance()函数

"""
>>> a = Animal()
>>> d = Dog()
>>> h = Husky()
然后，判断：

>>> isinstance(h, Husky)
True
>>> isinstance(h, Dog) and isinstance(h, Animal)
True

>>>isinstance(a, Husky)
False

'''
还可以判断一个变量是否是某些类型中的一种,如判断是否是list或者tuple:
'''
>>> isinstance([1, 2, 3], (list, tuple))
True
