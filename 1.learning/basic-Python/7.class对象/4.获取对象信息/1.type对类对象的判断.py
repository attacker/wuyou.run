#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------


type(123)
#<class 'int'>

type('hehe')
#<class 'str'>

type(None)
#<class 'NoneType'>

type(abs)
#<class 'builtin_function_or_method'>

'''
type()函数返回的是什么类型呢？它返回对应的Class类型。
如果我们要在if语句中判断，就需要比较两个变量的type类型是否相同：
'''

type(123)==type(456)
#True
type(123)==int
#True
type('abc')==type('123')
#True
type('abc')==str
#True
type('abc')==type(123)
False
# 字符串和数字类型不一样