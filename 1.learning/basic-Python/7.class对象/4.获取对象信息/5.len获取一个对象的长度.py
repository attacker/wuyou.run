#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-12
#Description:
# --------------------------------------------------


class MyDog(object):
    def __len__(self):
        return 100
...
dog = MyDog()
print (len(dog))
#100


'''
>>> len('ABC')
3
>>> 'ABC'.__len__()
3
'''