#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------


class preson(object):
    def __init__(self, name, age):
        self.__name = name
        self.age = age
        #类属性


    def print_all(self):
        print("%s:%s" % (self.__name,self.age))
        # 类方法;直接访问内部定义数据的函数

A=preson("LJ","28")

A.print_all()
#1.调用内部访问

print(A.age)
#2.在外部不能调用到"__name"的私有属性。


print(A._preson__name)
#3.强行访问私有类（不要这样做！）