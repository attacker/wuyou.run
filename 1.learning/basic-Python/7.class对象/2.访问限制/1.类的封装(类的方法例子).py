#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------


class preson(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age
        #类属性


    def print_all(self):
        print("%s:%s" % (self.name,self.age))
        # 类方法;直接访问内部定义数据的函数


A=preson("LJ","28")
A.print_all()

#只在类的内部相互关联实现数据访问
#数据和逻辑被“封装”起来，调用很容易，但却不用知道内部实现的细节
