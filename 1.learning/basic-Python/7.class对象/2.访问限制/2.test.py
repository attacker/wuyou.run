#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-3-11
#Description:
# --------------------------------------------------

'''
class Student(object):
    def __init__(self, name, gender):
        self.name = name
        self.gender = gender


A=Student('lj','man')
print(A.name, A.gender)
'''


class Student(object):
    def __init__(self, name, gender):
        self.__name = name
        self.__gender = gender

    def get_name(self):
    	print(self.__name)


    def get_gender(self):
    	return self.__gender

    def set_gender(self,gender):
        self.__gender = gender




bart = Student('Bart', 'male')
if bart.get_gender() != 'male':
    print('测试失败!')
else:
    bart.set_gender('female')
    if bart.get_gender() != 'female':
        print('测试失败!')
    else:
        print('测试成功!')