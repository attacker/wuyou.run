


class Animal(object):
    def run(self):
        print('Animal is running...')



class Dog(Animal):
    print('dog')

    def eat(self):
        print('Eating meat...')

class Cat(Animal):
    pass

# dog = Animal()
# dog.run()

dog = Dog()
dog.run()
dog.eat()  # 对子类增加一些方法

print(isinstance(dog,Animal)) # 定义的数据类型和Python自带的数据类型，比如str、list、dict没什么两样




# 当我们定义一个class的时候，可以从某个现有的class继承，
# 新的class称为子类（Subclass），而被继承的class称为基类、父类或超类（Base class、Super class）