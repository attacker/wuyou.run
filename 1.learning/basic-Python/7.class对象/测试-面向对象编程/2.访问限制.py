class Student(object):

    def __init__(self, name, score):
        self.__name = name
        self.__score = score

    def print_score(self):
        print('%s: %s' % (self.__name, self.__score))


# 外部获取
    def get_name(self):
        return self.__name
    def get_score(self):
        return self.__score
# 外部设置属性值
    def set_score(self, score):
        self.__score = score

#  外部设置属性值；在方法中可以对参数做检查，避免传入无效的参数
    def set_score2(self, score):
        if 0 <= score <= 100:
            self.__score = score
        else:
            raise ValueError('bad score')


bart = Student('Bart Simpson', 59) # 实例化




# print(bart.score)  __私有属性无法外部调用
print(bart.get_score())  # 外部获取私有属性


bart.set_score2(69) # 修改私有属性
print(bart.get_score()) # 查询是否修改



bart.__name = 'new boy'
print(bart.__name,bart.get_name())
# 内部的__name变量已经被Python解释器自动改成了_Student__name，而外部代码给bart新增了一个__name变量