


class Animal(object):
    def run(self):
        print('Animal is running...')

class Dog(Animal):
    pass

dog=Animal()

print (type(123))
print (type(None))
print (type(abs))
print (type(dog))




import types
def fn():
    pass

print (type(fn)==types.FunctionType )

type(lambda x: x)==types.LambdaType
#True

a = Animal()
d = Dog()


result = isinstance(a, Dog)
print(result)
result = isinstance(a, Animal)
print(result)

isinstance('a', str)
isinstance(123, int)
isinstance(b'a', bytes)

isinstance([1, 2, 3], (list, tuple))
isinstance((1, 2, 3), (list, tuple)) # 某些类型中的一种
# True


## dir查看类信息 和 len内部方法
result = dir(Animal)
print(result)
# 如果你调用len()函数试图获取一个对象的长度，实际上，在len()函数内部，它自动去调用该对象的__len__()方法
print (len('ABC')== 'ABC'.__len__()) # 相等
