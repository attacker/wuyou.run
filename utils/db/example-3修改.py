#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------
#Author:  LJ
#Email:   admin@attacker.club

#Last Modified: 2018-06-03 22:52:26
#Description: 
# --------------------------------------


import pymysql


conn = pymysql.connect(host = '127.0.0.1',port=3306,user = 'root',passwd = '',db = 'mysql')
#打开数据库连接
cursor = conn.cursor()
# 使用cursor()方法获取操作游标


effect_row = cursor.execute("insert into student VALUES (2,'jerry','male');")
#effect_row　影响到多少行

conn.commit()  #修改操作要提交
cursor.close()#关闭连接