#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------
#Author:  LJ
#Email:   admin@attacker.club

#Last Modified: 2018-06-03 22:56:45
#Description: 
# --------------------------------------

## 查询操作
import pymysql


conn = pymysql.connect(host = '127.0.0.1',port=3306,user = 'root',passwd = '',db = 'mysql')
#打开数据库连接
cursor = conn.cursor()
# 使用cursor()方法获取操作游标



req_1 = cursor.fetchone ()　#第一条
req_many  = cursor.fetchmany  (3)　#3条　;二维元组
req_all = cursor.fetchall  #获取所有结果


#！前面fetch过，后面只能查剩下的

### 字典键值对的形式(默认元组)
cursor = conn.cursor(cursor=pymysql.cursor.DictCursor)