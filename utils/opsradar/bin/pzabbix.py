#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Author:  LJ
Email:   admin@attacker.club
Time:    2020/4/1 18:03
Description:
"""
import json
import requests


class pzabbix():
    def __init__(self):
        self.url = 'http://zabbix.xxxx.com/api_jsonrpc.php'

    def token_key(self):
        post_headers = {'Content-Type': 'application/json'}
        post_data = {
            "jsonrpc": "2.0",
            "method": "user.login",
            "params": {
                "user": "jingjing",
                "password": "xxxx"
            },
            "id": 1
        }

        ret = requests.post(url=self.url, data=json.dumps(
            post_data), headers=post_headers)
        return ret.json()["result"]


s = pzabbix()
res = s.token_key()
print(res)
# print(type(res))
# print(res["result"])
