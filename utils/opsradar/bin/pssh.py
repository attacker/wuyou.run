#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Author:  LJ
Email:   admin@attacker.club
Time:    2020/3/17 09:47
Description:
"""

import paramiko
import time
import socket
import os


# print(os.getcwd().strip('/bin$'))
def run(host, user, passwd, port, cmd):
    date = time.strftime("%b %d %H:%M:%S", time.localtime())
   # print("INFO:\t%s\t\033[33m%s@%s,%s\033[0m Trying to connect　..." % (date, user, host, port))
    # paramiko.util.log_to_file('paramiko.log')

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(
            hostname=host,
            username=user,
            password=passwd,
            port=port,
            pkey=None, look_for_keys=False,
            timeout=3, allow_agent=False)
        #print("INFO:\t%s\t%s@%s:%s \033[1;32mConnection status successful\033[0m." % (date, user, host, port))

        stdin, stdout, stderr = ssh.exec_command(cmd)
        command = ("Command: %s\t Date: %s\n\n" %
                   (cmd, date))
        results = stdout.read().decode() + stderr.read().decode()
        #  print(command)
        #print(results)


    except Exception as e:
        results = str(e)
        print(e, type(e))
    finally:
        ssh.close()
       # print("INFO:\t%s\t%s@%s:%s \033[31mExit connection status\033[0m." % (date, user, host, port))
    return results


if __name__ == '__main__':
    cmd = 'uptime'
    run('10.0.1.231', "root", "123456", 22, cmd)
