#!/usr/bin/env python3
# coding: utf-8


import smtplib  # 加载smtplib模块
from email.mime.text import MIMEText
from email.utils import formataddr
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


class SendMail(object):
    def __init__(self, target, email, password, smtp, alias, title, content):
        self.target = target  # 发送地址
        self.sys_sender = email  # 邮件地址
        self.sys_pwd = password  # 邮箱账号
        self.smtp = smtp  # smtp服务器地址
        self.alias = alias
        self.title = title  # 标题
        self.content = content  # 发送内容

    def send(self, file_list):
        """
        发送邮件
        :param file_list: 附件文件列表
        :return: bool
        """
        try:
            # 创建一个带附件的实例
            msg = MIMEMultipart()
            # 发件人格式
            msg['From'] = formataddr([self.alias, self.sys_sender])
            # 收件人格式
            msg['To'] = formataddr(["", self.target])
            # 邮件主题
            msg['Subject'] = self.title
            # 括号里的对应收件人邮箱昵称、收件人邮箱账号

            # 邮件正文内容
            msg.attach(MIMEText(self.content, 'plain', 'utf-8'))

            # 多个附件
            for file_name in file_list:
                print("file_name", file_name)
                # 构造附件
                xlsxpart = MIMEApplication(open(file_name, 'rb').read())
                # filename表示邮件中显示的附件名
                xlsxpart.add_header('Content-Disposition',
                                    'attachment', filename='%s' % file_name)
                msg.attach(xlsxpart)

            # SMTP服务器
            server = smtplib.SMTP_SSL(self.smtp, 465, timeout=10)
            # 登录账户
            server.login(self.sys_sender, self.sys_pwd)
            # 发送邮件
            server.sendmail(self.sys_sender, [self.target, ], msg.as_string())
            # 退出账户
            server.quit()
            return True
        except Exception as e:
            print(e)
            return False


if __name__ == '__main__':

    host = "imap.exmail.qq.com"
    email = "ops@seedien.com"
    password = "kmzEiF9TpRXv4iYP"
    smtp = "smtp.exmail.qq.com"
    alias = 'robot'
    target = "admin@attacker.club"  # 发送地址
    title = "统计"  # 标题

    content = "2019-11-01 ~ 2019-11-30 统计，见附件!"    # 发送内容
    file_list = ["杭州05-31.xlsx"]   # 附件列表

    ret = SendMail(target, email, password, smtp,
                   alias, title, content).send(file_list)
    print(ret, type(ret))
