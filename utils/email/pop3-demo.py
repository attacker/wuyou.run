#!/usr/bin/env python
# coding=utf-8
'''
@Author: 以谁为师
@Website: attacker.club
@Date: 2020-04-23 16:09:42
@LastEditTime: 2020-04-24 11:44:42
@Description: 
'''
import poplib
import getpass
# from email.header import decode_header
from email.parser import Parser
from bs4 import BeautifulSoup


# 连接到POP3服务器:
server = poplib.POP3("pop.exmail.qq.com")
# 可以打开或关闭调试信息:
server.set_debuglevel(1)
# 可选:打印POP3服务器的欢迎文字:
print(server.getwelcome().decode('utf-8'))

# 身份认证:
server.user("a@xxxx.com")
server.pass_(getpass.getpass())

# stat()返回邮件数量和占用空间:
print('Messages: %s. Size: %s' % server.stat())
# list()返回所有邮件的编号:
resp, mails, octets = server.list()
# 可以查看返回的列表类似[b'1 82923', b'2 2184', ...]
print(mails)

# 获取最新一封邮件, 注意索引号从1开始:
index = len(mails)

if index > 0:
    resp, email_content, octets = server.retr(index)
    # print(email_content)
    # email_content存储了邮件的原始文本的每一行,
    # 可以获得整个邮件的原始文本:

    msg_content = b'\r\n'.join(email_content).decode('utf-8')
    # print(msg_content)
    # 稍后解析出邮件:
    msg = Parser().parsestr(msg_content)
    print("邮件类型: %s" % msg.get_content_type())
    print("发件人: %s" % msg["From"])
    html = msg.get_payload(decode=True)

    # BeautifulSoup过滤body 内容
    soup = BeautifulSoup(html, 'lxml')
    body = soup.find('div').text
    body = body.split()[0]
    print("邮件内容: %s" % body)

else:
    print('没有邮件处理')

# 可以根据邮件索引号直接从服务器删除邮件:
# server.dele(index) 无法删除 ->使用imaplib

# 关闭连接:
server.quit()
