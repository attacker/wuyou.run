

## 查看命令
fab list

## 测试主机
fab ping
 
## 执行命令
fab cmd:name="uptime"
fab  cmd:name="netstat -pntl |grep redis" 
fab cmd:name="""salt -E "webpd" test.ping """


## 批量cmd列表
fab do 


## upload 上传
    #上传文件：本地目录,远程目录;默认将当前目录内文件上传到/tmp
    # 例子 fab upload   本地:/data/software/java/jdk*8*   目标: /usr/local/src
## download 下载
    # 例子 fab download  目标：远程路径文件或目录 本地：./当前路径;留空会生成主机ip目录;指定路径文件或目录


