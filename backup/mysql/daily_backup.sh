#!/bin/bash

# crontab: * * * * * /opt/daily_backup.sh xwiki

BackupHome="/home/xwikibak/mysqlbak"      # 本地原始文件
RemoteHome="/Backup/dt_nas246/mysqldata"  # 远程备份路径

starttime=$(date +'%Y-%m-%d %H:%M:%S')

# 判断变量参数
if [ -z "$1" ]; then
    echo "please input parameter \$1"
    exit 1
else
    SERVERNAME=$1
    echo "current parameter ${SERVERNAME}"
fi

# 本地备份路径
if [ ! -d $BackupHome ]; then
        mkdir -p $BackupHome
fi  


#### 变量 ####
program_dir=$(dirname $0)              # 获取当前路径
bak_log="$program_dir/script.log"      # Log
exec 2>&1 >  $bak_log # 记录脚本内的输出



lastdayfile=${BackupHome}/${SERVERNAME}_$(date +%Y%m%d --date="-1 days").sql
bakcup_sql=${SERVERNAME}_$(date +%Y%m%d).sql.gz
full_backup_sql=${BackupHome}/${bakcup_sql}
encrypt_file=${BackupHome}/${SERVERNAME}_$(date +%Y%m%d).tar.gz

# echo "
# 昨日备份:$lastdayfile
# 今日备份:$full_backup_sql
# 加密备份:$encrypt_file
# "


mails="l@qq.me,x@163.com"
# 发送邮件


function sqlbackup(){
    host=$1
    local port=$2
    local user=$3
    local userPWD=$4

    # 判断mysql是否在线
    mysqladmin -h$host -P$port -u$user -p$userPWD ping | grep alive
    if [ $? -ne 0 ]; then
        echo "conn the server err...Please GoTo server check log:${bak_log}"
        mail -s "$full_backup_sql bakcup"  $mails < $bak_log
        exit -1
    fi

    # 开始备份数据库
    echo "Begin Backup $SERVERNAME $host in ${starttime}"
	# gzip -d解压
    mysqldump -h$host -P$port -u$user -p$userPWD --all-databases --all-tablespaces --routines --lock-all-tables | gzip >   $full_backup_sql
	# gunzip -d eolinker_os_20200924.sql.gz 解压
    if [ $? -eq 0 ]; then
        echo "Mysqldump Success! End in $(date +'%Y-%m-%d %H:%M:%S')"
     
        
        # 昨天文件没有找到则文件大小设置为0
        if [ ! -f $lastdayfile ]; then
            lastdayfize=0
        else
            lastdayfize=$(du -b $lastdayfile | awk '{print $1}')
        fi
        toadyfsize=$(du -b $full_backup_sql | awk '{print $1}')


        # 备份文件比昨天还小提醒
        if [ $toadyfsize -lt $lastdayfize ]; then
            echo "Bakcup:$full_backup_sql fail...Size is Lower...Please GoTo server check log:${bak_log}"
        fi

        # 清理7天内的文件
        find $BackupHome   -name   "*.sql.gz" -mtime +15 -type f -print -exec rm -f {} \;
		find $BackupHome   -name   "*.tar.gz" -mtime +15 -type f -print -exec rm -f {} \;
    fi
}


function remote_backup(){

    if [ -d $RemoteHome ]; then
        cd  $BackupHome
        tar -zcf - $bakcup_sql |openssl des3 -salt -k "xxxxxxxx" | dd of=$encrypt_file
        # rsync -v $encrypt_file  $RemoteHome
    else

        echo "The remote directory does not exist !!! "

    fi

    # 加密方法: tar -zcf - 要加密的文件或目录 |openssl des3 -salt -k 加密的密码 | dd of=加密后的文件
    # 解密方法: dd if=加密的文件 |openssl des3 -d -k 加密密码 | tar zxf -
}



function the_end(){

    count1=`du -sh $full_backup_sql`
    count2=`du -sh $encrypt_file`
    count3=`du -sh $RemoteHome/${encrypt_file##*/}`
    echo
    echo "$count1"
    echo "$count2"
    echo "$count3"

    endtime=`date +'%Y-%m-%d %H:%M:%S'`
    start_seconds=$(date --date="$starttime" +%s);
    end_seconds=$(date --date="$endtime" +%s);
    echo "Task time： "$((end_seconds-start_seconds))"s"
    mail -s "End of backup task"  $mails < $bak_log
}


# 根据不同入参执行函数
case $1 in
    xwiki)
        sqlbackup 192.168.1.11 3307 root 123456
        remote_backup
        the_end
        ;;
    eolinker_os)
        sqlbackup 192.168.1.12 3361 root 11111
        remote_backup
        the_end
        ;;
    *)
        echo "Use available parameters  \$1: $0 xwiki | eolinker_os"
        ;;
esac

