#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:        LJ
#Email:         admin@attacker.club
#Site:          blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-06-29 12:30:10 Last:2017-09-09 13:49:08
#Description:   Zabbix_agent安装
# --------------------------------------------------

import re
import os
import sys
import time


cmd_list = ["pkill zabbix_agentd ",
"rm /usr/local/zabbix-agent/  /etc/init.d/zabbix-agent -rf",
"rpm -ivh http://repo.zabbix.com/zabbix/3.2/rhel/6/x86_64/zabbix-release-3.2-1.el6.noarch.rpm",
"yum install zabbix-agent -y"
""
]


def warn (Text):
    print ">>>>>\t\033[1;31m%s\033[0m\t<<<<<" % Text
def info (Text):
    print ">>>>>\t\033[1;33m%s\033[0m\t<<<<<" % Text
#提示

def shell(cmd):
    os.system(cmd)
def shellinfo(cmd):
    r=os.popen(cmd)
    text=r.read()
    r.close()
    return text
#shell
def file(path,method,content):
    f=open(path,method)
    f.write(content)
    f.close()


{{ forloop.counter }}:





if __name__ == '__main__':

    zbxhost = raw_input('输入zabbix服务器ip:\n')
    #zbxhost = '192.168.1.2'

    for each_cmd in cmd_list:
        info ('执行命令:%s' % each_cmd)
        shell(each_cmd)

    info ('写入配置')    
    file('/etc/zabbix/zabbix_agentd.conf','w',
'''#version 1
LogFile=/tmp/zabbix_agentd.log
Server=%s
ServerActive=%s
HostMetadataItem=system.uname''' % (zbxhost,zbxhost))

    info ('启动zabbix agent')
    shell ('/usr/sbin/zabbix_agentd')