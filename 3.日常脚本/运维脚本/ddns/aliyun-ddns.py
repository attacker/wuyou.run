'''
Author: Logan  Mail: admin@attacker.club
Date: 2022-06-20 18:08:05
LastEditTime: 2022-06-22 13:37:11
Description: 
'''
import re
import requests

from  aliyunsdkcore.client import  AcsClient
import configparser # 导入模块



def getip(url):
    req = requests.get(url)
    result = req.text
    ipaddr = re.findall(r"\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b", result)
    if ipaddr:
        return ipaddr[0]




def ddns_config():
    config = configparser.ConfigParser()  # 实例化(生成对象)
    config.read('ddns.cfg')  # 读配置文件(注意文件路径)
    config.sections()   # 调用sections方法
    return config



if __name__ == "__main__":

    url = "http://attacker.club/getip"  # 域名
    # url = "http://ipinfo.io"

    try:
        IP = getip(url)
    except Exception as e:
        print(e)
    else:
        print("获取到ip地址:%s" % IP)
        cf = ddns_config()
        print(cf["aliyunak"]["AccessKeyId"])
        print(cf["aliyunak"]["AccessKeySecret"])














