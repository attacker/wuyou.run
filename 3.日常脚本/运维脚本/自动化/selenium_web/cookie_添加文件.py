from selenium import webdriver
import time
import  json

driver = webdriver.Chrome()
driver.get("http://jp.ops.net")  #打开浏览器
time.sleep(3)

## 导入cookie
f1 = open('cookie.txt')
cookie = f1.read()
cookie =json.loads(cookie)
for c in cookie:
    driver.add_cookie(c)

## 刷新页面
driver.refresh()
#关闭浏览器
driver.quit()