from selenium import webdriver
import time
import json

driver = webdriver.Chrome()
driver.get("http://jp.ops.net")  # 打开浏览器
time.sleep(3)

## 导入cookie
#driver.add_cookie({'name':'csrftoken','value':'EpTN67jINCnrlEX6DJ0hkV0AiBHoCPBjO5tD5ahlQi79IEdq60g90nEm9SgZrCSg'})
driver.add_cookie({'name':'sessionid','value':'sffndhh483dqkdey2xbpvjtlvaa05q5v'})


## 刷新页面
driver.refresh()

driver.find_element_by_link_text("资产管理").click()
driver.find_element_by_link_text("资产列表").click()

time.sleep(30)

#关闭浏览器
driver.quit()