#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-4-9
#Description: http://ops.attacker.club/Automatic/fabric.html
# --------------------------------------------------
import os,sys
from fabric.api import env,run,local,put,get,prompt,runs_once,task,abort
from fabric.contrib.console import confirm
from fabric.colors import green,yellow
from fabric.context_managers import settings,hide
from env.command

confirm('是否执行脚本,Continue?')
print (green('\n\n正在执行中... \n'))

#主机
if  os.path.exists("host.txt") and os.path.getsize("host.txt")>0:#存在文件并且文件不为空
    hosts = []
    file = open('host.txt', 'r')
    for line in file.readlines():
        line = line.strip()
        if line:
            hosts.append(line)
    file.close()
    env.hosts = hosts
    # 主机列表信息

else:
    env.hosts = str(input("输入主机:\n")) #没有host.txt列表


env.user = 'root'
#远程帐号
#远程连接超时时间
env.timeout = 5
#命令超时时间
env.command_timeout = 10

#env.key_filename = ['~/.ssh/id_rsa'] #证书使用


@runs_once #只匹配一次，避免每个主机处理都输入一次
def input_raw():
    return prompt("请输入密码",default="123456")
    #远程密码


@runs_once      #只执行一次，避免每个主机处理都输入一次
def local_path():
    return prompt("输入本地保存路径：")
@runs_once
def local_files(): #建议绝对路径,可用通配符
    return prompt("输入本地文件或目录名(绝对路径)：")

@runs_once
def remote_path():
    return prompt("输入远程保存路径：",default="/tmp")
@runs_once
def remote_files():
    return prompt("输入远程文件或目录名：")



@task
def do ():
    env.password = input_raw()
    with settings(hide('running','stderr'),warn_only=True):
        run ("uptime")
        #执行指定的远程命令

@task
def cmdlist ():
    env.password = input_raw()
    with settings(hide('running', 'stderr'), warn_only=True):
        with open ("cmdlist.txt") as f:
            count=0
            for cmdline in f:
                run(cmdline)
                count += 1

                print((yellow('[INFO]')+green('\t[%s]') +'\t%s') % (count,cmdline))

            print (yellow("[%s]\t\t执行完毕...") % env.host)




@task
def ping ():
    env.password = input_raw()
    with settings(hide('running','stderr','everything'),warn_only=True):
        run("ping -c 1 114.114.114.114")
        print (yellow("[%s]\t\t执行完毕...") % env.host)
    #fab ping

@task
def cmd (name):
    env.password = input_raw()
    with settings(hide('running','stderr'), warn_only=True):
        run(name)
    #fab cmd:name="uptime"

@task
def upload():
    env.password = input_raw()
    with settings(hide('running', 'stderr'), warn_only=True):
        put(local_files(),remote_path())
    #上传文件：本地目录,远程目录;默认将当前目录内文件上传到/tmp
    # 例子 fab upload   本地:/data/software/java/jdk*8*   目标: /usr/local/src

@task
def download():
    env.password = input_raw()
    with(hide('running', 'stderr')):
        get(remote_files(), local_path())
    # 例子 fab download  目标：远程路径文件或目录 本地：./当前路径;留空会生成主机ip目录;指定路径文件或目录

@task
def debug():
    file = files_path()
    print(file)



