#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:    LJ
#Email:     admin@attacker.club

#Date:      18-4-16
#Description:
# --------------------------------------------------


import os,sys
from fabric.api import env,run,local,put,get,prompt,runs_once,task,abort
from fabric.contrib.console import confirm
from fabric.colors import green,yellow
from fabric.context_managers import settings,hide


filelist = [
    "/data/software/java/*",
#    "/data/software/maven/*",
#    "/data/software/python/*",
    "/data/software/ELK/*",
]



env.user = 'root'
#env.hosts = ['10.4.230.201','192.168.0.10']
#env.hosts = ['10.4.230.201']
env.hosts = ['192.168.0.10']

env.timeout = 5
#命令超时时间
env.command_timeout = 10




@runs_once #只匹配一次，避免每个主机处理都输入一次
def input_raw():
    return prompt("请输入密码",default="123456")
    #远程密码




@task
def upload():
    env.password = input_raw()
    for line in filelist:
        with settings(hide('running', 'stderr'), warn_only=True):
            result = put(line, "/srv/salt/prod/files/")
            if result.failed and not confirm("put file failed,Continue[Y/N]?"):
                abort("Aborting file put task!")
