# -*- coding: utf-8 -*-
#python2


import ssl
from pysphere import VIServer


ssl._create_default_https_context = ssl._create_unverified_context

server = VIServer()
server.connect('vcenter.config.net','administrator@vcenter.config.net','xxxxxx')

FILE='vmlist.txt'
vmlist = server.get_registered_vms(datacenter="Datacenter")
fobj = open(FILE,'a')
for vm_path in vmlist:
    vm = server.get_vm_by_path(vm_path)
    status = vm.get_status()
    print ("vm_path=[%s], status=[%s]" % (vm_path, status))
    fobj.write("%s\n" % vm_path)
fobj.close()

# vm_name = 'nginx'

# def lineiter(fobj):
#     for each in fobj:
#         if vm_name in each:
#             yield  each

# # 98-1.livemweb

# with open(FILE,'r') as fobj:
#     line = lineiter(fobj)
#     for e in line:
#         u=e.strip()
#         n = server.get_vm_by_path(u)
