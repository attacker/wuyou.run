#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------
# Author:  LJ
# Email:   admin@attacker.club

# Last Modified: 2018-06-19 21:33:43
# Description:
# --------------------------------------
import requests
import sys
import json
import datetime
import pprint


from requests.packages.urllib3.exceptions import InsecureRequestWarning
# 禁用安全请求警告
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def GetToken(Userid, Secret):
    Url = "https://cloud.gttxidc.com/xddos/public/authorize"
    Data = {
        "appId": Userid,
        "secretKey": Secret
    }
    r = requests.get(url=Url, params=Data, verify=False)
    return r.json()


def GetAttack(ip, begin, end):
    Url = "https://cloud.gttxidc.com/xddos/public/attacks"
    headers = {
        "Authorization": Token,
    }
    Data = {
        "ipList": ip,
        "beginDate": begin,
        "endDate": end
    }

    r = requests.get(url=Url, params=Data, headers=headers, verify=False)

    Attack_data = r.json()['result']['data']
    attack_dict = {}
    if len(Attack_data) > 0:
        for each_ip in Attack_data:
            attacks = each_ip['attacks']
            for attack in attacks:
                attack['startTime'] = datetime.datetime.fromtimestamp(
                    attack['startTime']).strftime("%Y/%m/%d %H:%M:%S")
                attack['endTime'] = datetime.datetime.fromtimestamp(
                    attack['endTime']).strftime("%Y/%m/%d %H:%M:%S")
                print(each_ip['ip'], "===============")
                pprint.pprint(attack, indent=3)
    else:
        print ("no attack")


def GetBlackHole(ip):
    Url = "https://cloud.gttxidc.com/xddos/public/blackHoleStatus"
    headers = {
        "Authorization": Token,
    }
    Data = {
        "ipList": ip,
    }

    r = requests.get(url=Url, params=Data, headers=headers, verify=False)

    BlackHole_data = r.json()['result']['data']
    if len(BlackHole_data) > 0:
        print("BlackHole IP:===============\n")
        print(BlackHole_data)

    else:
        print ("no BlackHole")


if __name__ == '__main__':
    now = datetime.datetime.now()  # 当前时间
    min = now - datetime.timedelta(minutes=300)  # 30分钟前

    beginDate = min.strftime('%Y%m%d%H%M%S')
    endDate = now.strftime('%Y%m%d%H%M%S')  # 3分钟一次查询

    #beginDate = "20180619221500"
    #endDate =   "20180621152000"

    appId = "13958178591"  # 账户
    secretKey = "674V52"  # Secret凭证密钥
    ipList = "36.27.209.66,36.27.209.67,36.27.209.68,36.27.209.69,36.27.209.70,36.27.209.71,36.27.209.72,36.27.209.73"

    Token_json = GetToken(appId, secretKey)
    Token = Token_json['result']['Authorization']
    print(Token)

    #Attack_json = GetAttack(ipList, beginDate, endDate)
    #Attack_data = Attack_json['result']['data']

    GetAttack(ipList, beginDate, endDate)
    

    GetBlackHole(ipList)
