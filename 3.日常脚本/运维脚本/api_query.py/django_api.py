# -*- coding: utf-8 -*-
# @Author: Logos
# @Date:   2018-07-20 21:36:10
# @Last Modified by:   Logos
# @Last Modified time: 2018-07-20 22:45:34
import requests
import sys
import json
import pprint



#from requests.packages.urllib3.exceptions import InsecureRequestWarning
# 禁用安全请求警告
#requests.packages.urllib3.disable_warnings(InsecureRequestWarning)




def Getapi():
    #Url = "http://127.0.0.1:8000"
    Url = "http://127.0.0.1:8000/users/?format=json"
    r = requests.get(url=Url, verify=False)
    return r.json()


if __name__ == '__main__':
	#pprint.pprint(GetToken())
	pprint.pprint(Getapi(),indent=5)


