#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author:  LJ
Email:   admin@attacker.club
Last Modified: 2019-07-05 11:37:03
Description:
'''


from elasticsearch import Elasticsearch
import datetime
import sys
import time
import json

"""
now = datetime.datetime.now()  # 当前时间
before = now - datetime.timedelta(minutes=5)  # 10分钟前
today = now.strftime('%Y.%m.%d') # 当天格式化:2019.07.04

begin_time = before.strftime('%Y-%m-%dT%H:%M:%S.000+0800')
end_time = now.strftime('%Y-%m-%dT%H:%M:%S.000+0800')
print(before_time,'-',now_time)
"""

day = '2019-06-27'
day_format=('.'.join(day.split('-')))



begin_time = "{0}T00:00:00.000+0800".format(day)
end_time = "{0}T23:59:59.999+0800".format(day)



sn_list = [

"XDI080B17L02169",
"XDI080BL18D00458",
"XDI080BL18F02310",
"XDI080BL18F02321",
"XDI080BL18F02550",
"XDI080BL18G01785",
"XDI080BL18G02444",
"XDI080BL18G03127",
"XDI080BL18G03249",
"XDI080BL18G03450",
"XDI080BL18G03489",
"XDI080BL18G03538",
"XDI080BL18G03543",
"XDI080BL18G03544",
"XDI080BL18G03574",
"XDI080BL18G03581",
"XDI080BL18G03582",
"XDI080BL18G03823",
"XDI080BL18G03931",
"XDI080BL18H02006",
"XDI080BL18H02116",
"XDI080BL18H02128",
"XDI080BL18H02593",
"XDI080BL18H05092",
"XDI080BL18K00286",
"XDI080BL18K00374",
"XDI080BL18K00393",
"XDI080BL18L01279",

]



# domain = sys.argv[1] # 自定义域名入参
domain = 'devicecore.xxxx.com'
deviceSN = ''
# deviceSN = "XDI080BL18K00374"
doc = {

    "query":
        {
            "bool":
                {
                    "filter":
                        [
                            {"match": {
                                # "fields.log_source": 'prod_nginx'
                                "server_name" : domain
                            }},

                            {"match": {
                               "deviceSN": deviceSN 
                            }},

                            {"range": {
                                "@timestamp": {
                                    "gte": begin_time,
                                    "lt": end_time
                                },
                            }},

                        ],
                }
        }
}

text = json.dumps(doc)
e = eval(text)
print (text,e,type(e))



if __name__ == "__main__":

    es = Elasticsearch([{'host': '172.16.140.120', 'port': 9200}])  # es地址
    # index = "logstash-nginx-access-{0}".format(today)  # index索引名
    index ="logstash-nginx-access-%s" %day_format
    print (index)

    # with open
    # n = 0
    # for i in sn_list:
    #     n = n+1
    #     print (i,n)
    #     deviceSN = n

    #     res = es.search(index, body=doc)  # 实例化
    #     res_docs = res["hits"]["hits"]
    #     total = res['hits']['total']
    #     print(deviceSN,total)
    #     time.sleep (10)


    # res = es.search(index, body=doc)  # 实例化
    # res_docs = res["hits"]["hits"]
    # total = res['hits']['total']

    # # print(res_docs)
    # print(deviceSN,total) 