#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:        LJ
#Email:         admin@attacker.club
#Site:          blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-09-05 23:30:44 Last:2015-05-17 04:12:52
#Description:   
# --------------------------------------------------
import threading
import socket
bind_ip = "0.0.0.0" #全网地址
bind_port = 999 #服务器监听端口

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(5) #最大连接数5

print "[*] Listening on %s:%d" % (bind_ip, bind_port)


#客户端处理线程
def handle_client(client_socket):
    # 发送一些数据
    client_socket.send("连接成功\r\n")
    # 打印客户端发的数据
    request = client_socket.recv(1024)
    print "[*] Reveived: %s" % request
    # 返回一个数据包
    client_socket.send("ACK!\r\n")
    client_socket.close()
    
while True:
    client, addr = server.accept()
    print "[*] Accepted connection from: %s:%d" % (addr[0], addr[1])
    # 挂起客户端线程，处理传入的数据
    client_handler = threading.Thread(target=handle_client,args=(client,))
    client_handler.start()