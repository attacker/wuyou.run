#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------------
#Author:		LJ
#Email:			admin@attacker.club
#Site:		    blog.attacker.club

#Site:blog.attacker.club  Mail:admin@attacker.club
#Date:2017-09-05 23:30:43 Last:2015-05-17 04:12:52
#Description:   
# --------------------------------------------------
import  socket
target_host = "127.0.0.1"
target_port = 999

# 建立一个socket对象
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 连接客户端
client.connect((target_host, target_port))
# 发送一些数据
client.send("GET / HTTP/1.1\r\nHost: attacker.com\r\n\r\n")
# 接收数据
response = client.recv(4096)

print response