'''
Author: admin@attacker.club
Date: 2022-09-20 11:09:09
LastEditTime: 2022-09-20 11:09:20
Description: 
'''
# coding:utf-8
from multiprocessing.pool import ThreadPool
import socket
import time



def scan_port(port):
    try:
        s = socket.socket()
        s.settimeout(2)
        s.connect(('192.168.194.131',int(port)))
        print("+++端口号：", port)
        print(s.recv(1024))
        s.close()
    except Exception as e:
        # print(">>>端口号关闭：",port)
        pass
if __name__ == '__main__':
    print(">>>州的先生zmister.com 端口扫描器 Beta0.2")
    start_port = int(input("请输入起始端口，最小为1："))
    end_port = int(input("请输入结束端口号，最大为65535："))
    start_time = time.time()
    pool = ThreadPool(processes=500)
    pool.map_async(scan_port,range(start_port,end_port))
    pool.close()
    pool.join()
    end_time = time.time()
    print("扫描耗时：",end_time - start_time)